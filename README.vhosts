# This vhost file assumes that it is the *only* bunch
# of vhosts on the system. For the EBN machine, this
# is true. You should edit the file to adjust for the
# paths in the filesystem; the ones used here are for
# the OpenSUSE installation.

# There are three vhosts defined:
#   www.englishbreakfastnetwork.org
#	Code quality checking and presentation; PHP + PostgreSQL
#   solaris.bionicmutton.org
#	Mercurial and some static HTML
#   api.kde.org
#	Static HTML and a search CGI

NameVirtualHost *:80



# The EBN vhost uses PHP, nothing else fancy
<VirtualHost *:80>
    ServerAdmin sysadmin@kde.org
    ServerName www.englishbreakfastnetwork.org

    DocumentRoot /mnt/ebn/apache/www.englishbreakfastnetwork.org

    ErrorLog /mnt/ebn/apache/log/ebn-error
    CustomLog /mnt/ebn/apache/log/ebn-access combined

    HostnameLookups Off
    UseCanonicalName Off
    ServerSignature On


    # EBN uses PHP5 for all the webpages
    Include /etc/apache2/conf.d/php5.conf


    ScriptAlias /cgi-bin/ "/mnt/ebn/apache/cgi-bin/ebn"

    <Directory "/mnt/ebn/apache/cgi-bin/ebn">
	AllowOverride None
	Options +ExecCGI -Includes
	Order allow,deny
	Allow from all
    </Directory>


    <Directory "/mnt/ebn/apache/www.englishbreakfastnetwork.org">
	Options Indexes FollowSymLinks
	AllowOverride None
	Order allow,deny
	Allow from all
    </Directory>

    # Use both the EBN and the PEAR includes in PHP
    php_value include_path "/mnt/ebn/apache/www.englishbreakfastnetwork.org:/usr/share/php5/PEAR"
</VirtualHost>

# api.kde.org has nothing fancy, pure static HTML and some cgi-bin.
<VirtualHost *:80>
    ServerAdmin sysadmin@kde.org
    ServerName api.kde.org

    DocumentRoot /mnt/ebn/apache/api.kde.org

    ErrorLog /mnt/ebn/apache/log/api-error
    CustomLog /mnt/ebn/apache/log/api-access combined

    HostnameLookups Off
    UseCanonicalName Off
    ServerSignature On

    ScriptAlias /cgi-bin/ "/mnt/ebn/apache/cgi-bin/api"

    <Directory "/mnt/ebn/apache/cgi-bin/api">
	AllowOverride None
	Options +ExecCGI -Includes
	Order allow,deny
	Allow from all
    </Directory>


    <Directory "/mnt/ebn/apache/api.kde.org">
	Options Indexes FollowSymLinks
	AllowOverride None
	Order allow,deny
	Allow from all
    </Directory>


    # The old EBN let Doxygen generate into html/ directories,
    # so there are zillions of links to module/subdir/html/file.html;
    # This rewrite will drop the superfluous html.
    RedirectMatch 301 (.*)/html/(.*)$ $1/$2
</VirtualHost>



# The BM hosts serve up Mercurial repositories, so they use python
# scripts copied over from the Mercurial installation.
<VirtualHost *:80>
    ServerAdmin sysadmin@kde.org
    ServerName solaris.bionicmutton.org

    DocumentRoot /mnt/ebn/apache/solaris.bionicmutton.org

    ErrorLog /mnt/ebn/apache/log/bm-error
    CustomLog /mnt/ebn/apache/log/bm-access combined

    HostnameLookups Off
    UseCanonicalName Off
    ServerSignature On

    Include /etc/apache2/conf.d/php5.conf
    php_value include_path "/mnt/ebn/apache/api.kde.org/media/includes:/usr/share/php5/PEAR"

    ScriptAlias /cgi-bin/ "/mnt/ebn/apache/cgi-bin/bm"
    ScriptAliasMatch ^/hg(.*) /mnt/ebn/apache/cgi-bin/bm/hgwebdir.cgi$1

    <Directory "/mnt/ebn/apache/cgi-bin/bm">
	AllowOverride None
	Options +ExecCGI -Includes
	Order allow,deny
	Allow from all
    </Directory>


    <Directory "/mnt/ebn/apache/solaris.bionicmutton.org">
	Options Indexes FollowSymLinks
    
	AllowOverride None
	Order allow,deny
	Allow from all
    </Directory>
</VirtualHost>

<Directory ~ "\.svn">
    Order allow,deny
    Deny from all
</Directory>

