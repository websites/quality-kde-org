#ifndef EXPORTHELPER_H
#define EXPORTHELPER_H

#include <QtCore/QString>

class QSqlDatabase;
class QStringList;

class ExportHelper
{
public:
    ExportHelper( QSqlDatabase *database );
    ~ExportHelper();

    /** Returns a distinct list of components for which component check results where reported */
    QStringList components() const;

    /** Returns a distinct list of languages for which sloc was reported. */
    QStringList languages() const;

    /** Returns a distinct list of modules that where registered. */
    QStringList modules() const;

    /** Returns a distinct list of submodules that where registered for @param module. */
    QStringList submodules( const QString &module ) const;

    /** Returns the tool(s) used for the last component check ran on @param component */
    QStringList toolsForLastComponentCheck( const QString &component ) const;

    /** Returns the plugins available for @param tool */
    QStringList plugins( const QString &tool ) const;

    /**
     * Returns the loc. These functions always return the values for the last
     * component check for which results where reported. The methods that have
     * QString() as value for @param language parameter return the summed sloc
     * for all languages.
     */
    int sloc( const QString &component,
              const QString &language = QString() ) const;
    int slocPerModule( const QString &component,
                       const QString &module,
                       const QString &language = QString() ) const;
    int slocPerSubmodule( const QString &component,
                          const QString &module,
                          const QString &submodule,
                          const QString &language = QString() ) const;

    /**
     * Returns the number of issues. These functions always return the values for the last
     * component check for which results where reported.
     */
    int issues( const QString &component,
                const QString &plugin = QString() ) const;
    int issuesPerModule( const QString &component,
                         const QString &module,
                         const QString &plugin = QString() ) const;
    int issuesPerSubmodule( const QString &component,
                            const QString &module,
                            const QString &submodule,
                            const QString &plugin = QString() ) const;

private:
    class Private;
    Private *d;
};

#endif // EXPORTHELPER_H
