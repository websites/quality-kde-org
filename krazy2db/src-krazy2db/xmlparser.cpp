/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "xmlparser.h"

#include <QtCore/QFile>

XmlParser::XmlParser(QString const &fileName)
  : mXmlFile(new QFile(fileName))
{
  Q_ASSERT(mXmlFile->exists());
  mXmlFile->open(QIODevice::ReadOnly);
  Q_ASSERT(mXmlFile->isOpen());

  mDocument = new XmlDocument(mXmlFile);
  // TODO add validation
}

XmlParser::~XmlParser()
{
  delete mXmlFile;
  delete mDocument;
}

