#ifndef TOOLRUNRESULTPARSER_H
#define TOOLRUNRESULTPARSER_H

#include <libstorage/tool.h>

#include "xmlparser.h"

class ToolRunResultParser : public XmlParser
{
  public:
    ToolRunResultParser(Tool const &tool,
                        qlonglong componentCheckId,
                        QString const &xmlFile);

    virtual void parse(Database *db);

    void setModule(QString const &module);

    void setSubModule(QString const &subModule);

  private: // Methods
    QString strip(QString const &filePath);

  private:
    qlonglong mComponentCheckId;
    QString   mModule;
    QString   mSubModule;
    Tool      mTool;
};

#endif // TOOLRUNRESULTPARSER_H
