/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef RESULTANALYZER_H
#define RESULTANALYZER_H

#include <QtCore/QMap>
#include <QtCore/QString>

class Database;
class Tool;

/**
 * Little helper class that scans the result dir. Each directory found in the
 * result dir is considered a submodule. Everthing below that as files.
 */
class ResultAnalyzer
{
  public:
    ResultAnalyzer();

    /**
     * Analyze the resultsdir and interpret the xml files as output of @p tool.
     * Returns the componentCheckId or -1 on failure.
     */
    qlonglong anylyze(Tool const &tool);

    /**
     * Component info must have the following keys: component, module, revision.
     */
    void setComponentInfo(QMap<QString,QString> const &componentInfo);

    /**
     * The database in which the reported issues will be stored. Does not take
     * over ownership of the pointer.
     */
    void setDatabase(Database * const db);

    /**
     * Results dir containing the results of <em>one</em> tool. Each xml file in
     * this and its subdirs are considered as a result file.
     */
    void setResultDir(QString const &resultDir);

  private: //Methods
    void scanDir(qlonglong componentCheckId, Tool const &tool, QString const &path);

    void parse(qlonglong componentCheckId,
               Tool const &tool,
               QString const &path,
               QString const &xmlFile);

  private:
    QMap<QString,QString> mComponentInfo;
    Database             *mDatabase;
    QString               mResultDir;

};

#endif // RESULTANALYZER_H
