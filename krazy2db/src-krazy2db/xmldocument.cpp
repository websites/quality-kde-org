/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "xmldocument.h"

#include <QtCore/QString>
#include <QtCore/QProcess>
#include <QtCore/QFile>
#include <QtXml/QXmlSimpleReader>
#include <QtXmlPatterns/QXmlResultItems>
#include <QtXmlPatterns/QXmlQuery>

// XmlDocumentPrivate

InvalidXmlFile::InvalidXmlFile( const QString &fileName )
  : mFileName( fileName )
{ }

QString InvalidXmlFile::fileName() const
{
  return mFileName;
}


class XmlDocument::XmlDocumentPrivate
{
  public:
    XmlDocumentPrivate(QFile * const xmlFile)
        : mFile(xmlFile)
        , mIsValid(false)
    { }

    QFile * const mFile;
    bool          mIsValid;
    QString       mXmllint;
    QString       mXmlNamespace;
    QString       mXmlSchema;
    QXmlQuery     mQuery;
};

// XmlDocument

XmlDocument::XmlDocument(QFile * const xmlFile)
    : d(new XmlDocumentPrivate(xmlFile))
{
  Q_ASSERT(d->mFile->isOpen());
  d->mFile->seek(0);
  QXmlSimpleReader reader;
  if ( reader.parse( d->mFile ) ) {
    d->mFile->seek(0);
    d->mQuery.bindVariable("inputDocument", d->mFile);
  } else {
    throw InvalidXmlFile( xmlFile->fileName() );
  }
}

XmlDocument::~XmlDocument()
{
  delete d;
}

bool XmlDocument::has(QString const &path) const
{
  return count(path) > 0;
}

bool XmlDocument::readBool(QString const &path) const
{
  QString boolString = readString(path);
  Q_ASSERT(boolString == "true" || boolString == "false");
  return (boolString == "true") ? true : false;
}

int XmlDocument::count(QString const &path) const
{
  return readInt("count(" + path + ")");
}

int XmlDocument::readInt(QString const &path) const
{
  QString intString = readString(path);
  bool ok = false;
  int number = intString.toInt(&ok);
  Q_ASSERT(ok);
  return number;
}

QString XmlDocument::readString(QString const &path) const
{
  QString query;
  if (!d->mXmlNamespace.isEmpty())
    query += "declare default element namespace \"" + d->mXmlNamespace + "\";\n";
  query += "declare variable $inputDocument external;\n";
  query += "doc($inputDocument)/" + path;

  d->mQuery.setQuery(query);
  Q_ASSERT(d->mQuery.isValid());

  QString result;
  d->mQuery.evaluateTo(&result);

  return result.trimmed();
}

void XmlDocument::setDefaultNamespace(QString const &xmlNamespace)
{
  d->mXmlNamespace = xmlNamespace;
}

void XmlDocument::setXmllintExecutable(QString const &xmllintPath)
{
  d->mXmllint = xmllintPath;
}

void XmlDocument::setXmlSchema(QString const &schema)
{
  d->mXmlSchema = schema;
}

bool XmlDocument::isValid()
{
  if (d->mXmllint.isEmpty() || d->mXmlSchema.isEmpty())
    return false;

  QProcess xmllint;
  xmllint.start(d->mXmllint, QStringList() << "--schema" << d->mXmlSchema << d->mFile->fileName());

  if (!xmllint.waitForFinished())
    return false; // Timed out after 30 seconds.

  QString result = xmllint.readAllStandardError().trimmed();
  return (result == d->mFile->fileName() + " validates" );
}
