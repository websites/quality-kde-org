set(krazy2db_SRCS
  slocparser.cpp
  toolrunresultparser.cpp
  resultanalyzer.cpp
  xmldocument.cpp
  xmlparser.cpp
  toollistparser.cpp
  main.cpp
)

add_executable(krazy2db ${krazy2db_SRCS})
target_link_libraries(krazy2db
  ${QT_QTXML_LIBRARY}
  ${QT_QTXMLPATTERNS_LIBRARY}
  storage
)
