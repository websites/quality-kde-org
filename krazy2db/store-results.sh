#!/bin/bash

if [[ -d build ]]; then
  G_XMLTODB_EXEC=./build/src-krazy2db/krazy2db
elif [[ -d qtcreator-build ]]; then
  G_XMLTODB_EXEC=./qtcreator-build/src-krazy2db/krazy2db
else
  echo "No build dir found";
  return 1;
fi

# First things first, was the tool build?
if [[ ! -x $G_XMLTODB_EXEC ]]; then
  echo "Could not find $G_XMLTODB_EXEC, make sure you build the tool.";
  exit 1;
fi

# Was the obligatory component result dir parameter passed?
if [[ $# != 2 ]]; then
  echo "Usage: $0 [COMPONENT_RESULT_DIR] [DB_CONFIG]";
  exit 1;
fi

# Does the passed parameter actually exist?
if [[ ! -e $1 ]]; then
  echo "The directory $1 does not exists";
  exit 1;
fi

# Is the passed parameter actually a directory?
if [[ ! -d $1 ]]; then
  echo "$1 is not a directory.";
  exit 1;
fi

# Okay everything set, lets get the results into the db.
for module in $1/*; do
  if [[ -d $module ]]; then
    echo "Reading results of $module...";
    $G_XMLTODB_EXEC $module $2
  fi
done

