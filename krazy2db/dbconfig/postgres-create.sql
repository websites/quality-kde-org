CREATE TABLE tools (
  id SERIAL PRIMARY KEY,
  name TEXT,
  version TEXT,
  description TEXT
);

CREATE TABLE plugins (
  id SERIAL PRIMARY KEY,
  tool_id INTEGER REFERENCES tools (id),
  name TEXT,
  version TEXT,
  description TEXT,
  filetype TEXT,
  is_extra BOOLEAN
);

CREATE TABLE component_check (
  id SERIAL PRIMARY KEY,
  component TEXT,
  revision TEXT,
  date TEXT
);

CREATE TABLE module_submodule (
  id SERIAL PRIMARY KEY,
  module TEXT,
  submodule TEXT
);

CREATE TABLE checked_files (
  id SERIAL PRIMARY KEY,
  module_submodule_id INTEGER REFERENCES module_submodule (id),
  file TEXT,
  type TEXT
);

-- Results per file/plugin
CREATE TABLE checked_file_results (
  checked_files_id INTEGER REFERENCES checked_files (id),
  component_check_id INTEGER REFERENCES component_check (id),
  plugin_id INTEGER REFERENCES plugins (id),
  issue_count INTEGER,
  PRIMARY KEY (checked_files_id, component_check_id, plugin_id)
);

-- SLOC per submodule
CREATE TABLE module_submodule_sloc (
  component_check_id INTEGER REFERENCES component_check (id),
  module_submodule_id INTEGER REFERENCES module_submodule (id),
  language TEXT,
  sloc INTEGER
);

-- drop table tools cascade;
-- drop table plugins cascade;
-- drop table component_check cascade;
-- drop table module_submodule cascade;
-- drop table checked_files cascade;
-- drop table checked_file_results;
