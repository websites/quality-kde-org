/*
    Copyright (c) 2010 Tobias Koenig <tokoe@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef DBCONFIG_H
#define DBCONFIG_H

#include <QtCore/QSettings>
#include <QtSql/QSqlDatabase>

/**
 * A base class that provides an unique access layer to configuration
 * and initialization of different database backends.
 */
class DbConfig
{
  public:
    enum Table {
      Tools,
      Plugins,
      ComponentCheck,
      ModuleSubmodule,
      CheckedFiles,
      CheckedFileResults,
      ModuleSubmoduleSloc
    };

  public:

    virtual ~DbConfig();

    /**
     * Returns the DbConfig instance for the database as configured in
     * @param settings.
     */
    static DbConfig* configuredDatabase( QSettings &settings );

    /**
     * This method applies the configured settings to the QtSql @p database
     * instance.
     */
    virtual void apply( QSqlDatabase &database ) const = 0;

    /**
     * Creates all default tables.
     */
    bool createTables( QSqlDatabase &database ) const;

    /**
     * Creates all default tables.
     */
    virtual bool createTable( QSqlDatabase &database, Table table ) const = 0;


  protected:
    DbConfig();
};

#endif
