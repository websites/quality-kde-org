/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef TOOL_H
#define TOOL_H

#include <QtCore/QString>

class Plugin;

class Tool
{
  public:
    Tool(QString const &name = "unknown");

    Tool(Tool const &other);

    ~Tool();

    void addPlugin(Plugin const &plugin);

    QString description() const;

    qlonglong id() const;

    QString name() const;

    QString version() const;

    //PluginList plugins(QString const &fileType = QString()) const;

    Plugin plugin(QString const &name) const;

    void setId(qlonglong id);

    void setDescription(QString const &description);

    void setName(QString const &name);

    void setVersion(QString const &version);

  private:
    class ToolPrivate;
    ToolPrivate *d;
};

#endif // TOOL_H
