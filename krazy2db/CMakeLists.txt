project(krazy2db)
cmake_minimum_required(VERSION 2.6)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules/")

include(CheckCXXCompilerFlag)

set(Sqlite_MIN_VERSION "3.6")
find_package(Sqlite REQUIRED)

set(QT_MIN_VERSION "4.5.0")
set(QT_USE_QTXMLPATTERNS 1)
find_package(Qt4 REQUIRED)
include(${QT_USE_FILE})

set(CMAKE_INSTALL_RPATH ${CMAKE_PREFIX_PATH}/lib)
check_cxx_compiler_flag(-fvisibility=hidden __HAVE_GCC_VISIBILITY)
set(__HAVE_GCC_VISIBILITY ${HAVE_GCC_VISIBILITY} CACHE BOOL "GCC support for hidden visibility")

# configure_file(macros.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/krazymacros.h)

set(INSTALL_TARGETS_DEFAULT_ARGS RUNTIME DESTINATION bin
                                 LIBRARY DESTINATION lib${LIB_SUFFIX}
                                 ARCHIVE DESTINATION lib${LIB_SUFFIX})

include_directories(
  ${QT_INCLUDE_DIR}
  ${CMAKE_SOURCE_DIR}
)

add_subdirectory(libstorage)
add_subdirectory(src-krazy2db)
add_subdirectory(src-manyeyes)

