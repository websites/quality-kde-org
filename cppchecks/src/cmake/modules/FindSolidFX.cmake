# Copyright (c) 2009, Bertjan Broeksema, <b.broeksema@kdemail.net>

# Try to find the SolidFX headers and libraries. Once done this will define:
#
# TODO: Currently it is not clear to me yet which libraries/headers are really
#       required, so for now everything is required. This probably can change in
#       the future. In that case we'll have to define a minimal set which makes
#       SolidFX_FOUND evaluate to TRUE.
#
#  SolidFX_FOUND - SolidFX has been found
#  SolidFX_INCLUDE_DIR - Path to the SolidFX headers.
#  SolidFX_LIBRARIES - All libraries normally needed. In most case you want to
#                      use this.

# SolidFX_COMMON_FOUND
# SolidFX_CPP_FOUND
# SolidFX_FXCXXAPI_FOUND
# SolidFX_DATABASE_FOUND
# SolidFX_FDBE_FOUND
# SolidFX_EXPORTERS_FOUND
# SolidFX_GRAPH_FOUND
# SolidFX_METRICS_FOUND
# SolidFX_QUERIES_FOUND
# SolidFX_IBERTY_FOUND

# SolidFX_COMMON_LIBRARY    - libcommon.a
# SolidFX_CPP_LIBRARY       - libcpp.a
# SolidFX_FXCXXAPI_LIBRARY  - libfxcxxapi.a
# SolidFX_DATABASE_LIBRARY  - libdatabase.a
# SolidFX_EXPORTERS_LIBRARY - libExporters.a
# SolidFX_FDBE_LIBRARY      - libfdbe.a
# SolidFX_GRAPH_LIBRARY     - libGraph.a
# SolidFX_IBERTY_LIBRARY    - libiberty.a
# SolidFX_METRICS_LIBRARY   - libMetrics.a
# SolidFX_QUERIES_LIBRARY   - libQueries.a

set(LIB_PATHS /usr/lib /usr/local/lib ENV LD_LIBRARY_PATH)
set(INCLUDE_PATHS /usr/include /usr/local/include ENV INCLUDE_PATH)

MESSAGE(STATUS "Looking for SolidFX libraries and includes:")

## Libraries

# Common library

FIND_LIBRARY(SolidFX_COMMON_LIBRARY NAME common PATHS ${LIB_PATHS})
FIND_PATH(SolidFX_EFESAPI_INCLUDE_DIR NAME EfesAPI PATHS ${INCLUDE_PATHS})

IF (SolidFX_EFESAPI_INCLUDE_DIR)
  STRING(REGEX REPLACE "/EfesAPI$" "" solidfx_include_dir ${SolidFX_EFESAPI_INCLUDE_DIR})
  SET(SolidFX_INCLUDE_DIR ${solidfx_include_dir} CACHE PATH "")
  UNSET(SolidFX_EFESAPI_INCLUDE_DIR)
  MESSAGE(STATUS "  Found SolidFX headers  : ${SolidFX_INCLUDE_DIR}")
ELSE (SolidFX_EFESAPI_INCLUDE_DIR)
  IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX headers")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF(SolidFX_EFESAPI_INCLUDE_DIR)


IF (SolidFX_COMMON_LIBRARY)
   SET(SolidFX_COMMON_FOUND TRUE)
ENDIF (SolidFX_COMMON_LIBRARY)

IF (SolidFX_COMMON_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/common   : ${SolidFX_COMMON_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_COMMON_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/common")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_COMMON_FOUND)

# cpp library

FIND_LIBRARY(SolidFX_CPP_LIBRARY NAME cpp PATHS ${LIB_PATHS})

IF (SolidFX_CPP_LIBRARY)
   SET(SolidFX_CPP_FOUND TRUE)
ENDIF (SolidFX_CPP_LIBRARY)

IF (SolidFX_CPP_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/cpp      : ${SolidFX_CPP_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_COMMON_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/cpp")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_CPP_FOUND)

# database library

FIND_LIBRARY(SolidFX_DATABASE_LIBRARY NAME database PATHS ${LIB_PATHS})

IF (SolidFX_DATABASE_LIBRARY)
   SET(SolidFX_DATABASE_FOUND TRUE)
ENDIF (SolidFX_DATABASE_LIBRARY)

IF (SolidFX_DATABASE_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/database : ${SolidFX_DATABASE_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_DATABASE_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/database")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_DATABASE_FOUND)

# Exporters library

FIND_LIBRARY(SolidFX_EXPORTERS_LIBRARY NAME Exporters PATHS ${LIB_PATHS})

IF (SolidFX_EXPORTERS_LIBRARY)
   SET(SolidFX_EXPORTERS_FOUND TRUE)
ENDIF (SolidFX_EXPORTERS_LIBRARY)

IF (SolidFX_EXPORTERS_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/Exporters: ${SolidFX_EXPORTERS_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_EXPORTERS_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/Exporters")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_EXPORTERS_FOUND)

# fdbe library

FIND_LIBRARY(SolidFX_FDBE_LIBRARY NAME fdbe PATHS ${LIB_PATHS})

IF (SolidFX_FDBE_LIBRARY)
   SET(SolidFX_FDBE_FOUND TRUE)
ENDIF (SolidFX_FDBE_LIBRARY)

IF (SolidFX_FDBE_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/fdbe     : ${SolidFX_FDBE_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_FDBE_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/fdbe")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_FDBE_FOUND)

# fxcxxapi library

FIND_LIBRARY(SolidFX_FXCXXAPI_LIBRARY NAME fxcxxapi PATHS ${LIB_PATHS})

IF (SolidFX_FXCXXAPI_LIBRARY)
   SET(SolidFX_FXCXXAPI_FOUND TRUE)
ENDIF (SolidFX_FXCXXAPI_LIBRARY)

IF (SolidFX_FXCXXAPI_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/fxcxxapi : ${SolidFX_FXCXXAPI_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_FXCXXAPI_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/fxcxxapi")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_FXCXXAPI_FOUND)

# Graph library

FIND_LIBRARY(SolidFX_GRAPH_LIBRARY NAME Graph PATHS ${LIB_PATHS})

IF (SolidFX_GRAPH_LIBRARY)
   SET(SolidFX_GRAPH_FOUND TRUE)
ENDIF (SolidFX_GRAPH_LIBRARY)

IF (SolidFX_GRAPH_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/Graph    : ${SolidFX_GRAPH_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_GRAPH_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/Graph")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_GRAPH_FOUND)

# iberty library

FIND_LIBRARY(SolidFX_IBERTY_LIBRARY NAME iberty PATHS ${LIB_PATHS})

IF (SolidFX_IBERTY_LIBRARY)
   SET(SolidFX_IBERTY_FOUND TRUE)
ENDIF (SolidFX_IBERTY_LIBRARY)

IF (SolidFX_IBERTY_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/iberty   : ${SolidFX_IBERTY_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_IBERTY_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/iberty")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_IBERTY_FOUND)

# Metrics library

FIND_LIBRARY(SolidFX_METRICS_LIBRARY NAME Metrics PATHS ${LIB_PATHS})

IF (SolidFX_METRICS_LIBRARY)
   SET(SolidFX_METRICS_FOUND TRUE)
ENDIF (SolidFX_METRICS_LIBRARY)

IF (SolidFX_METRICS_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/Metrics  : ${SolidFX_METRICS_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_METRICS_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/Metrics")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_METRICS_FOUND)

# Metrics library

FIND_LIBRARY(SolidFX_QUERIES_LIBRARY NAME Queries PATHS ${LIB_PATHS})

IF (SolidFX_QUERIES_LIBRARY)
   SET(SolidFX_QUERIES_FOUND TRUE)
ENDIF (SolidFX_QUERIES_LIBRARY)

IF (SolidFX_QUERIES_FOUND)
   IF (NOT SolidFX_FIND_QUIETLY)
      MESSAGE(STATUS "  Found SolidFX/Queries  : ${SolidFX_QUERIES_LIBRARY}")
   ENDIF (NOT SolidFX_FIND_QUIETLY)
ELSE (SolidFX_QUERIES_FOUND)
   IF (SolidFX_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find SolidFX/Queries")
   ENDIF (SolidFX_FIND_REQUIRED)
ENDIF (SolidFX_QUERIES_FOUND)

# WARNING: Don't change the order of the SolidFX libs
set(SolidFX_LIBRARIES
  ${SolidFX_FDBE_LIBRARY}
  ${SolidFX_EXPORTERS_LIBRARY}
  ${SolidFX_QUERIES_LIBRARY}
  ${SolidFX_METRICS_LIBRARY}
  ${SolidFX_FXCXXAPI_LIBRARY}
  ${SolidFX_COMMON_LIBRARY}
  ${SolidFX_GRAPH_LIBRARY}
  ${SolidFX_DATABASE_LIBRARY}
  ${SolidFX_CPP_LIBRARY}
)

SET(SolidFX_FOUND TRUE)