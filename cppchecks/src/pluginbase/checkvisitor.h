#ifndef CHECKVISITOR_H
#define CHECKVISITOR_H

#include <added/ASTVisitor.h>

#include "issue.h"
#include "location.h"

namespace EFES {
  class FileTable;
}

class CheckVisitor : public EFES::ASTVisitor
{
  public: // Methods
    CheckVisitor(EFES::ExtractionUnitPtr const &eup);

    /** Return the issues found after visiting the extraction unit. */
    IssueList issues() const;

    /** Virtual overloads of ASTVisitor */
    virtual Visit postVisitASTNode(EFES::ASTNode &obj);
    virtual Visit postVisitASTNode(EFES::ASTNode const &obj);
    virtual Visit visitASTNode(EFES::ASTNode &obj);
    virtual Visit visitASTNode(EFES::ASTNode const &obj);

    /**
     * Returns the location of the given node.
     */
    Location location(EFES::NodeWithLocation const &obj) const;

  protected: // Methods
    /** Entry point to all checking work, must be reimplemented by subclasses. */
    virtual Visit check(EFES::ASTNode const &node) = 0;

    /**
     * Returns wether or not the given node is part of the project or found in
     * one of the headers in the system include paths.
     */
    bool inUserSpace(EFES::ASTNode const &node);

  protected: // Members
    EFES::ExtractionUnitPtr const mExtractionUnitPtr;
    IssueList                     mIssues;
};

#endif // CHECKVISITOR_H
