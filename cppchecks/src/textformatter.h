#ifndef TEXTFORMATTER_H
#define TEXTFORMATTER_H

#include "outputformatter.h"

class TextFormatter : public OutputFormatter
{
  public:
    virtual void printFooter() const;

    virtual void printHeader() const;

    virtual void printListFooter() const;

    virtual void printListHeader() const;

    virtual void printPlugin(QString const &name, QString const &version, QString const &desc) const;

    virtual void printResults(IssueList const &issues)  const;

  private:
    void printDescription() const;
};

#endif // TEXTFORMATTER_H
