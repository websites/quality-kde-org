#ifndef VIRTUALMISMATCHCHECK_H
#define VIRTUALMISMATCHCHECK_H

#include <pluginbase/analyzerplugin.h>

class VirtualMismatchCheck : public QObject, public AnalyzerInterface
{
  Q_OBJECT
  Q_INTERFACES(AnalyzerInterface)

  public:
    VirtualMismatchCheck();

    virtual QString description() const;

    virtual QString name() const;

    virtual void run(EFES::ExtractionUnitPtr const &eup);

    virtual QString shortDescription() const;

    virtual QString version() const;
};

#endif // VIRTUALMISMATCHCHECK_H
