#include "virtualmismatchcheckvisitor.h"

#include <added/FileTable.h>
#include <EfesAPI/FactDBEngine.h>
#include <QtCore/QDebug>
#include <QtCore/QMultiMap>
#include <QtCore/QString>

using namespace EFES;

// Helper methods.

QString typeName(Type const &type)
{
  if (type.isPointerType())
    return type.asPointerType()->atType->toString().c_str();
  else if (type.isReferenceType())
    return type.asReferenceType()->atType->toString().c_str();
  else if (type.isCompoundType())
    return type.asCompoundType()->toString().c_str();
  else if (type.isSimpleType())
    return type.asSimpleType()->toString().c_str();
  else
    return QString("Unknown");
}

typedef QPair<QString, int> NameArgcPair;
typedef QList<Variable const*> QVariableList;
typedef QMultiMap<QString, Variable const*> ReturnTypNameFunctionMap;
typedef QMap<NameArgcPair, ReturnTypNameFunctionMap> SortedFunctions;

/**
 * Split the list of functions into seperate lists containing functions with
 * similar signature and same name.
 */
QList<QVariableList> sort(QVariableList functions)
{
  // Assumes:
  // 1) All functions in the list have the same name
  // 2) All functions in the list have the same number of arguments
  // 3) Al functions have a return type with the same name.

  QMultiMap<QString, Variable const*> sortedFunctions;

  foreach (Variable const *variable, functions) {
    QStringList typeNames;
    QList<bool> refOrVal;

    FunctionType const *functionType = variable->type->asFunctionType();

    QString key = variable->name;
    key += typeName(*functionType->retType);
    key += functionType->retType->isPointerType() ? "(0)" : "(1)";

    bool skippedReceiver = false;
    foreach (Variable* var, functionType->params) {
      if (!skippedReceiver) {
        skippedReceiver = true;
        continue;
      }
      key += ',';
      key += typeName(*var->type).remove(QRegExp("\\s*const"));
      key += functionType->retType->isPointerType() ? "(0)" : "(1)";
    }

    sortedFunctions.insert(key, variable);
  }

  QList<QVariableList> result;
  foreach (QString const &key, sortedFunctions.uniqueKeys()) {
    result << sortedFunctions.values(key);
  }

  return result;
}

SortedFunctions sort/*Virtuals*/(CompoundType const *ct, bool recursive)
{
  SortedFunctions result;

  TS_classSpec const *cs = ct->syntax;  //the AST of the class-declaration
  if (!cs) return result;               //no AST info? nothing to do

  if (recursive) {
    const TS_classSpec::BaseClassSpecs &bcl = cs->bases;
    for(int i=0;i<bcl.size();++i) {     //treat bases of 'ct'
      CompoundType const *ct = bcl[i]->type;
      if (!ct) continue;                //i-th base has no type info, may be due to multiple reasons - anyway must skip it
      result = sort(ct, recursive);        //process base 'ct' recursively
    }
  }

  MemberList const &cml = *cs->members; //bases are done; now get methods and other members of this
  for(int i = 0; i < cml.list.size(); ++i) {
    Member const *m = cml.list[i];

    Variable const *function = 0;


    if (m->asMR_func()) {               //first case: method definition (decl+body)
      MR_func const *f = m->asMR_func();
      Variable const *v = f->f->nameAndParams->var;
      if (!v) continue;                 //no type info, skip

      if(v->hasFlag(DF_VIRTUAL)) {
        function = v;
      }
   } else if (m->asMR_decl()) { //second case: possible method declaration (no body) or other kind of member
     MR_decl const *md = m->asMR_decl();
     Declaration const *decl = md->d;

     //REMARK: I hope this works ok for multiple-declarator declarations
     for(Declaration::Declarators::const_iterator di = decl->decllist.begin();
         di != decl->decllist.end(); ++di) {
       Declarator const *d = *di;
       Type const *t = d->type;
       if (!t || !d->var) continue;           //no type info, skip

       // discriminate between method decl and other member decls
       if (t->isFunctionType() && d->var->hasFlag(DF_VIRTUAL)) {
         function = d->var;
       }
     }
   }

   if (function) {
     NameArgcPair key(function->name, function->type->asFunctionType()->params.size() - 1);
     QString returnType = typeName(*function->type->asFunctionType()->retType);
     ReturnTypNameFunctionMap &map = result[key];

     if (!map.values(returnType).contains(function))
       map.insert(returnType, function);
   }
  }

  return result;
}

// VirtualMismatchCheckVisitor

VirtualMismatchCheckVisitor::VirtualMismatchCheckVisitor(ExtractionUnitPtr const &p)
  : CheckVisitor(p)
{}

ASTVisitor::Visit VirtualMismatchCheckVisitor::check(ASTNode const &n)
{
  if (inUserSpace(n) && n.nodeType() == TYPE_TS_classSpec) {
    TS_classSpec const *cs = n.asTypeSpecifier()->asTS_classSpec();
    if (cs->keyword == TI_CLASS)
      processClass(*cs);
  }

  return VISIT_CHILDREN_AND_POST;
}

void VirtualMismatchCheckVisitor::processClass(TS_classSpec const &classSpec)
{
  SortedFunctions functions = sort(classSpec.ctype, true);
  foreach(NameArgcPair const &key, functions.keys()) {
    ReturnTypNameFunctionMap map = functions.value(key);
    foreach(QString const &returnType, map.uniqueKeys()) {
      QVariableList functionVariables = map.values(returnType);
      if (functionVariables.size() <= 1)
        continue;

      QList<QVariableList> lists = sort(functionVariables);
      foreach (QVariableList const &list, lists) {
        if (list.size() == 1)
          continue;

        int prevConstCount = -1;
        int prevRefCount = -1;
        int constReceiver = 0;
        foreach (Variable const *functionVariable, list) {
          FunctionType const *functionType = functionVariable->type->asFunctionType();
          int currentConstCount = 0;
          int currentRefCount = 0;

          if (functionType->retType->isReference()) {
            if (functionType->retType->asReferenceType()->atType->getCVFlags() & CV_CONST)
              ++currentConstCount;
            else // When not using const reference we assume mixing.
              ++currentRefCount;
          } else if (functionType->retType->isPointer()) {
            if (functionType->retType->asPointerType()->atType->getCVFlags() & CV_CONST)
              ++currentConstCount;
          } else if (functionType->retType->getCVFlags() & CV_CONST) {
            // Value types, doesn't matter.
            ++currentConstCount;
          }

          bool skippedReceiver = false;
          foreach (Variable const *param, functionType->params) {
            if (!skippedReceiver) {
              skippedReceiver = true;
              if (param->type->getCVFlags() & CV_CONST);
                ++constReceiver;
              continue;
            }

            if (param->type->isReference()) {
              if (param->type->asReferenceType()->atType->getCVFlags() & CV_CONST) {
                ++currentConstCount;
              }
              else // When not using const reference we assume mixing.
                ++currentRefCount;
            } else if (param->type->isPointer()) {
              if (param->type->asPointerType()->atType->getCVFlags() & CV_CONST)
                ++currentConstCount;
            } else if (param->type->getCVFlags() & CV_CONST) {
              ++currentConstCount;
            }
          }

          if (prevConstCount == -1)
            prevConstCount = currentConstCount;
          else if (currentConstCount != prevConstCount) {
            Issue issue;
            issue.setDescription("Mixed constness of argument and/or return types.");
            issue.setLocation(location(*functionVariable));
            issue.setScope(classSpec.ctype->toString().c_str());

            foreach (Variable const *variable, list) {
              issue.addIssueLine(QString("class ")
                                 + variable->type->asFunctionType()->getClassOfMember()->name
                                 + QString(": ") + variable->toString().c_str());
            }
            mIssues.append(issue);
          }

          if (prevRefCount == -1)
            prevRefCount = currentRefCount;
          else if (currentRefCount != prevRefCount) {
            Issue issue;
            issue.setDescription("Mixed usage of reference and value for argument and/or return types.");
            issue.setLocation(location(*functionVariable));
            issue.setScope(classSpec.ctype->toString().c_str());

            foreach (Variable const *variable, list) {
              issue.addIssueLine(QString("class ")
                                 + variable->type->asFunctionType()->getClassOfMember()->name
                                 + QString(": ") + variable->toString().c_str());
            }
            mIssues.append(issue);
          }
        }

        if (constReceiver > 0 && constReceiver != list.size()) {
          Issue issue;
          issue.setDescription("Mixed constness of receiver for functions with similar signature.");
          issue.setLocation(location(*list.last()));
          issue.setScope(classSpec.ctype->toString().c_str());

          foreach (Variable const *variable, list) {
            issue.addIssueLine(QString("class ")
                               + variable->type->asFunctionType()->getClassOfMember()->name
                               + QString(": ") + variable->toString().c_str());
          }
          mIssues.append(issue);
        }
      }
    }
  }
}

