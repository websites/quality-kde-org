#ifndef CLASSHIERARCHY_H
#define CLASSHIERARCHY_H

#include <added/OverloadUtils.h>
#include <QtCore/QList>
#include <QtCore/QMap>

#include <pluginbase/issue.h>

namespace EFES {
  class TS_classSpec;
}

class CheckerDataFactory;

/**
 * The ClassHierarchyChecker checks for the following issues:
 *
 * - Consistency of virtual declarations. Functions with the same signature
 *   which are overriden via inheritance should be declared virtual first time
 *   they appear in the hierarchy.
 * - Missing virtual keywords. Virtual function from a baseclass is overriden
 *   without re-specifying the 'virtual' keyword (bad style).
 */
class ClassHierarchyChecker
{
  public:
    ClassHierarchyChecker();

    ~ClassHierarchyChecker();

    void analyze();

    /**
     * Initializes the checker with given class specification. The fileName is
     * expected to be the file in which the class is defined.
     */
    void initialize(Location const &location, EFES::TS_classSpec const *classSpec);

    /**
     * Returns a list of issues found during analysis.
     */
    IssueList issues() const;

  private:
    CheckerDataFactory                    *mCheckerFactory;
    EFES::TS_classSpec const              *mClass;     // The class-decl this object treats
    Location                               mLocation;  // The file in which the class-is defined.
    IssueList                              mIssues;
    EFES::OverloadUtils::CompoundOverloads mOverloads; // Stores the overload-data for this' class hierarchy
};

#endif // CLASSHIERARCHY_H
