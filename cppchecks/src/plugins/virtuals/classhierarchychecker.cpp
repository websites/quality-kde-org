#include "classhierarchychecker.h"

#include <added/ASTNodes.h>
#include <iostream>
#include <QtCore/QString>

#include "checkerdatafactory.h"

using namespace EFES;
using namespace OverloadUtils;
using namespace std;

ClassHierarchyChecker::ClassHierarchyChecker()
  : mClass(0)
  , mOverloads(mCheckerFactory = new CheckerDataFactory)
{ }

ClassHierarchyChecker::~ClassHierarchyChecker()
{
  delete mCheckerFactory;
}

//populates this with info from 'cs' and superclasses
void ClassHierarchyChecker::initialize(Location const &location, TS_classSpec const *cs)
{
  mClass = cs;
  mLocation = location;
  mOverloads.initialize(cs->ctype);
}

void ClassHierarchyChecker::analyze() //Performs the analysis on this' class-hierarchy
{
  if (mClass->keyword != TI_CLASS)
    return; //Whether this is a 'class' or sth else ('struct', 'union', etc)

  bool classPrinted = false;
  for (CompoundOverloads::NameToOVFunctions::const_iterator it = mOverloads.methods.begin();
       it != mOverloads.methods.end(); ++it) {
    string const &nm = (*it).first;
    CompoundOverloads::OVFunctionsName const &mv = *(it->second);

    for(CompoundOverloads::OVFunctionsName::const_iterator it = mv.begin(); it != mv.end(); ++it) {
      OVFunctionsSigData const &os = (OVFunctionsSigData&)**it;

      if (os.virtualDeclarationInconsistent()) {
        QString description("Virtual declaration inconsistency");
        Issue issue;
        issue.setDescription(description);
        issue.setLocation(mLocation);
        issue.setScope(mClass->ctype->toString().c_str());

        foreach (OVFunction* function, os) {
          bool virt = function->var->flags & DF_VIRTUAL;

          QString line(function->var->toQualifiedString().c_str());
          line += " virtual = ";
          line += virt ? '1' : '0';
          issue.addIssueLine(line);
        }

        mIssues.append(issue);
      }

      if (os.virtualKeywordMissing()) {
        QString description("Virtual method missing 'virtual' keyword");
        Issue issue;
        issue.setDescription(description);
        issue.setLocation(mLocation);
        issue.setScope(mClass->ctype->toString().c_str());

        for(OVFunctionsSig::const_iterator jt = os.begin(); jt != os.end(); ++jt) {
          OVFunctionData const *function = static_cast<OVFunctionData*>(*jt);

          if (!function->virtualKeywordMissing()) continue;

          QString line(function->var->toQualifiedString().c_str());
          issue.addIssueLine(line);
        }
        mIssues.append(issue);
      }
    }
  }
}

IssueList ClassHierarchyChecker::issues() const
{
  return mIssues;
}
