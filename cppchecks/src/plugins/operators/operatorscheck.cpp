#include "operatorscheck.h"

#include <EfesAPI/EFES.h>

#include "operatorscheckvisitor.h"

using namespace EFES;

OperatorsCheck::OperatorsCheck() : QObject(0)
{ }

QString OperatorsCheck::description() const
{
  return QString("Does various checks on overloaded operators in classes. "
                 "Ignoring these issues might lead to bevavior not expected by "
                 "other developers.\n"
                 "- Checks if the assingment operator has the canonical form "
                 "(i.e. X& operator=(X const &other)\n"
                 "- Checks if overloaded comparison operators (e.g. operator=="
                 ", operator>, etc.) have const parameters, are declared "
                 "const and return a bool value.");
}

QString OperatorsCheck::name() const
{
  return QString("OperatorsCheck");
}

void OperatorsCheck::run(ExtractionUnitPtr const &eup)
{
  OperatorsCheckVisitor visitor(eup);
  visitor.traverse(*eup->ast());
  mIssues = visitor.issues();
}

QString OperatorsCheck::shortDescription() const
{
  return QString("Does various checks on overloaded operators in classes.");
}

QString OperatorsCheck::version() const
{
  return "0.1";
}

Q_EXPORT_PLUGIN2(operatorscheck, OperatorsCheck)
