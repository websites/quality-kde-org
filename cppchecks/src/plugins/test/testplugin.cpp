#include "testplugin.h"

#include <QtCore/QDebug>

using namespace EFES;

TestPlugin::TestPlugin() : QObject(0)
{ }

QString TestPlugin::description() const
{
  return QString("This plugin is soly for testing purposes and does therefore nothing");
}

QString TestPlugin::name() const
{
  return "TestAnalyzerPlugin";
}

void TestPlugin::run(ExtractionUnitPtr const &eup)
{
  Q_UNUSED(eup);
  qDebug() << "Running the test plugin.";
}

QString TestPlugin::shortDescription() const
{
  return QString("Test plugin, does nothing.");
}

QString TestPlugin::version() const
{
  return "testversion 0";
}

Q_EXPORT_PLUGIN2(testanalyzer, TestPlugin)
