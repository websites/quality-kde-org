#ifndef TESTPLUGIN_H
#define TESTPLUGIN_H

#include <QtCore/QObject>

#include <pluginbase/analyzerplugin.h>
#include <krazymacros.h>

class KRAZY_EXPORT TestPlugin : public QObject, public AnalyzerInterface
{
  Q_OBJECT
  Q_INTERFACES(AnalyzerInterface)

  public:
    TestPlugin();

    virtual QString description() const;

    virtual QString name() const;

    virtual void run(EFES::ExtractionUnitPtr const &eup);

    virtual QString shortDescription() const;

    virtual QString version() const;
};

#endif // TESTPLUGIN_H
