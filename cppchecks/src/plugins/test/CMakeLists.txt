set(testanalyzer_SRCS
  testplugin.cpp
)

set(test_plugin_MOC_HDRS
  testplugin.h
)

add_definitions(${QT_DEFINITIONS})
add_definitions(-DQT_PLUGIN)
add_definitions(-DQT_SHARED)

qt4_wrap_cpp(testanalyzer_MOC_SRCS ${test_plugin_MOC_HDRS})

add_library(testanalyzer SHARED ${testanalyzer_SRCS} ${testanalyzer_MOC_SRCS})
target_link_libraries(testanalyzer ${QT_QTCORE_LIBRARY})
