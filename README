This directory contains the various tools that make up the EBN.
There are a number of tools which take some common configuration
options (and some uncommon ones) and which analyze and report
on KDE source code.  

To install these tools on a machine, see README.setup.
For a sample vhosts file when serving the results with Apache,
  see README.vhosts.

The tools are:

- apidox     KDE API documentation is generated from the source
             code with Doxygen (1.4.7 for KDE trunk). This generates
             lots of error messages, which are then nicely formatted.
             See apidox/INSTALL.txt  for installation information.
             See apidox/README for usage information.

- coverage   Count, collect, graph gcov coverage of KDE
- sanitizer  KDE user documentation (in XML docbook) is subject to
             a long list of style guidelines.
- krazy2     KDE source code itself is subject to style and efficiency
             guidelines. The krazy tool counts and collates defects.


These tools are pulled together into a website for viewing; the website
subdirectory does just that.

It is recommended to use a (Postgres) database to store results in.

