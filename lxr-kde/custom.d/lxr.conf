# -*- mode: perl -*-
# LXR master configuration file
#

(
		################################
		# Global configuration section #
		################################

	{
		#
		# Master mode (where to find the tree name) #
		#===========================================#

		# Possible values are:
		#	- 'single'		single-tree context, no tree name (implicit)
		#	- 'host'		one host per tree
		#	- 'prefix'		generic host prefixed with tree name
		#	- 'section'		one section per tree (to be handled through symbolic
		#	          		                      links or duplication)
		#	- 'embedded'	tree name embedded in section path
		#	- 'argument'	first argument is tree name [PREFERRED]
		# CAUTION!
		#	Changing this parameter after initial configuration may
		#	require adjustements to other parameters!

	  'routing' => 'single'

		# Auxiliary tools subsection #
		#============================#

	, 'tmpdir' => '/tmp'

	, 'glimpsebin'     => '/home/lxr/inst/bin/glimpse'
	, 'glimpseindex'   => '/home/lxr/inst/bin/glimpseindex'
	, 'glimpsedirbase' => '/home/lxr/glimpse-db'
	#, 'swishbin'     => '/usr/bin/swish-e'
	#, 'swishdirbase' => '/home/lxr/swish-db'
	#, 'swishconf' => '/home/lxr/lxr/templates/swish-e.conf'

	, 'ectagsbin' => '/usr/bin/ctags'
	, 'ectagsconf' => '/home/lxr/lxr/templates/ectags.conf'

	, 'cvspath' => '/bin:/usr/local/bin:/usr/bin:/usr/sbin'
	, 'gitpath' => '/bin:/usr/local/bin:/usr/bin:/usr/sbin'
	, 'hgpath'  => '/bin:/usr/local/bin:/usr/bin:/usr/sbin'
	, 'svnpath' => '/bin:/usr/local/bin:/usr/bin:/usr/sbin'

		# Path to "magic" file to guess file content
		# CAUTION!
		#	This is a Fedora path; may be different under
		#	other distributions.
	# # # BUG # # #
	# A bug in File::MMagic version 1.27 prevents from using this
	# system magic file because it starts with a newline.
	# Parameter is commented out. Your alternate choice is:
	# 1) remove the initial offending empty lines,
	# 2) use another file without initial blank lines,
	# 3) default to the old magic file shipping with the release,
	# 4) work without any magic file to revert to internal tables.
#	, 'magicmime' => '/usr/share/misc/magic'

		# Computer DNS names subsection #
		#================================
	,	'host_names' =>	[ 'https://lxr.kde.org'
						, 'https://lxrnew.kde.org:443'
						]

		# HTML subsection #
		#=================#

		# All paths in this subsection are relative to LXR root directory

		# Templates used for headers and footers
	, 'htmlfatal'		=> '/home/lxr/lxr/templates/html/html-fatal.html'
# Customized for KDE adaptation
	, 'htmlhead'		=> '/home/lxr/lxr/custom.d/html-head.html'
# Customized for KDE adaptation
	, 'htmltail'		=> '/home/lxr/lxr/custom.d/html-tail.html'
	, 'htmldir'			=> '/home/lxr/lxr/templates/html/html-dir-indexing.html'
		# If you do not need the "last indexed" column in directory
		# display (e.g. for a stable unchanging tree), erase the
		# '-indexing' suffix above.
	, 'htmlident'		=> '/home/lxr/lxr/templates/html/html-ident.html'
	, 'htmlsearch'		=> '/home/lxr/lxr/templates/html/html-search-glimpse.html'
	, 'htmlconfig'		=> '/home/lxr/lxr/templates/html/html-config.html'

		# htmlhead and htmltail can be customised for the various
		# LXR operations. Just replace html by source (listing file),
		# sourcedir (displaying directory), diff (difference markup),
		# ident (identifier search), search (free-text search)
		# as below (which changes nothing):
#	, 'sourcehead'		=> '/home/lxr/lxr/custom.d/html-head.html'
#	, 'sourcedirhead'	=> '/home/lxr/lxr/custom.d/html-head.html'

		# showconfig script is a bit special since "version" has
		# no meaning for it. Better to wipe out the 'variables' selection
		# links with an adequate header:
	, 'showconfighead'		=> '/home/lxr/lxr/templates/html/config-head-btn-smaller.html'

	, 'htmlperf'            => '/home/lxr/lxr/templates/html/html-perf.html'

		# Default character width of left version (for diff)
	, 'diffleftwidth'	=> 50

		# Default identifier search constraint (for ident)
#	, 'identdefonly'	=> 1

		# CSS style sheet
	, 'stylesheet'		=> '/kde_lxr.css'
	#, 'alternate_stylesheet' => [ 'templates/classic.css' ]

		# Default character encoding
	, 'encoding'	=> 'utf-8'

		# File management subsection #
		#============================#

		# Which files should be excluded from indexing (and display).
	, 'ignorefiles' =>
		'^\\.|~$|\\.(o|a|orig)$|^CVS$|^core$'
		# Which extensions to treat as images when browsing.  If a file is an image,
		# it is displayed.
	, 'graphicfile' =>
		'bitmap|bmp|gif|icon?|jp2|jpe?g|pjpe?g|png|svg|tiff?|xbm|xpm'

		# Which file extensions are associated with which icon
		# Below is an example using 'Oxygen' theme available with KDE
		# small-icons/ is a symbolic link to
		#		 /usr/share/icons/oxygen/22x22/
		# (This location is valid for Fedora distributions,
		# check for others)
	, 'iconfolder' => '/small-icons/'
		# Don't forget the final / above.
	, 'icons' =>
		{	'[gm]?awk'			=> 'mimetypes/application-x-awk.png'
		,	'c|pc'				=> 'mimetypes/text-x-csrc.png'
		,	'h|hh'				=> 'mimetypes/text-x-chdr.png'
		,	'c\+\+|cc|cpp|cxx'	=> 'mimetypes/text-x-c++src.png'
		,	'hpp|hxx|h\+\+'		=> 'mimetypes/text-x-c++hdr.png'
		,	'java'				=> 'mimetypes/text-x-java.png'
		,	'js'				=> 'mimetypes/application-x-javascript.png'
		,	'ma?k'				=> 'mimetypes/text-x-makefile.png'
		,	'plx?|pm|perl'		=> 'mimetypes/application-x-perl.png'
		,	'php3?|phtml'		=> 'mimetypes/application-x-php.png'
		,	'py|python|px'		=> 'mimetypes/text-x-python.png'
		,	'sh|ba?sh|ksh|zsh|m4' => 'mimetypes/text-x-script.png'
		,	's?html?'			=> 'mimetypes/text-html.png'
		,	'css'				=> 'mimetypes/text-css.png'
		,	'p|pas'				=> 'mimetypes/text-x-pascal.png'
		,	'patch'				=> 'mimetypes/text-x-patch.png'
		,	'pdf'				=> 'mimetypes/application-pdf.png'
		,	'sql|pk(s|b)'		=> 'mimetypes/text-x-sql.png'
		,	'rb|ruby'			=> 'mimetypes/application-x-ruby.png'
		,	'txt'				=> 'mimetypes/text-plain.png'
		,	'ui'				=> 'mimetypes/text-xml.png'
		,	'.*\~'				=> 'mimetypes/application-x-trash'
		}
	, 'graphicicon' => 'mimetypes/image-x-generic.png'
	, 'defaulticon' => 'mimetypes/unknown.png'
	, 'diricon'     => 'mimetypes/inode-directory.png'
	, 'parenticon'  => 'actions/go-up.png'

	, 'filetypeconf' => '/home/lxr/lxr/templates/filetype.conf'

	, 'genericconf' => '/home/lxr/lxr/lib/LXR/Lang/generic.conf'

		# "Common factor" subsection #
		#============================#

		# In case your LXR installation controls several trees,
		# put here what you consider common parameters between
		# your source trees.
		# A parameter is "common" if its value must be simultaneously
		# adjusted in every tree.

		# Pattern for extracting tree name from URL, since all
		# trees are usually served from the same physical server.
		# Meaningful only in multiple trees context. Example for
		# built-in method where tree id is last before script name
#	, 'treeextract' => '([^/]*)/[^/]*$'

	, 'dbuser'		=> 'lxr'
	, 'dbpass'		=> 'INSERT_PASSWORD_HERE'
	, 'dbprefix'	=> 'lxr_'
	}

# ------------------------------------------------------------------------

		###############################
		# Tree configuration sections #
		###############################

# ------------------------------------------------------------------------
,	{

		# Server configuration subsection #
		#=================================#


	  'virtroot'     => ''
	,
	  'caption'      => 'KDE Source Cross Reference'
#	, 'shortcaption' => 'Tree'

	#, 'treename'     => ''

		# a link of the form (prefix)($filepath)(postfix) is generated when viewing a file
		#   example for cvsweb:
#	, 'cvswebprefix' => 'http://cvs.myhost.com/cgi-bin/cvsweb.cgi'
#	, 'cvswebpostfix' => '?cvsroot=rootname'
		#   example for viewcvs:
#	, 'cvswebprefix' => 'http://cvs.myhost.com/cgi-bin/viewcvs.cgi/myroot'
#	, 'cvswebpostfix' => ''

		# Tree location subsection #
		#==========================#

		# sourceroot - where to get the source files from

	, 'sourceroot' => '/home/lxr/src'

		# The string to display as the base part of every path in the tree
	, 'sourcerootname' => '$v'

		# Version selection subsection #
		#==============================#

	, 'variables' =>

			# Define typed variable "v".
		{ 'v' =>
			{ 'name' => 'Branch group'
				# This is the list of versions to index.
			, 'range' => [ readfile('/home/lxr/src/versions') ]
				# The default version to display
				# If not specified, first in 'range' used
			}

			# Other variables may be defined for use by rewrite rules
			# in the subdirectory section.
			# These variable definitions are needed for Linux kernel
			# browsing. Read carefully the User's Manual.
			# Alternately, use template lxrkernel.conf dedicated to kernel
			# configuration.
		}

		# Subdirectory subsection #
		#=========================#

		# Tree-specific files to ignore (in every directory of the tree)
		# Extend and uncomment the following copy of the global
		# parameter to override:
#	, 'ignorefiles' =>
#		'^\\.|~$|\\.(o|a|orig)$|^CVS$|^core$'

		# Directories to always ignore. These usually are the SCM's
		# private directories which possibly may contain non-public
		# project history. Note that .xxx directories are hidden
		# by an internal hard-coded rule. Redundant example:
#	, 'ignoredirs' => ['CVSROOT', 'CVS', '.git']
        , 'ignoredirs' =>       [qw(
                                        .git
                                        .svn
                                                )]


		# Where to look for include files inside the sourcetree.
		# This is used to hyperlink to included files. Example:
#	, 'incprefix' => [ '/include', '/include/linux' ]

		# These do funky things to paths in the system - you probably don't need them.
		# They are used to simulate compiler behaviour to various options
		# to rewrite short paths from (#)include statement and get the
		# real "physical" path to the file.
# 	, 'maps' =>		# $a means substitute with current value of variable 'a'
# 			[ '/include/asm[^\/]*/' => '/include/asm-$a/'
# 			, '/arch/[^\/]+/'       => '/arch/$a/'
# 			],

		# Having a fully functional set of 'incprefix'/'maps' directives
		# for the kernel is not a trivial task. It requires defining many
		# auxiliary variables which are dependent on the current development
		# state of the kernel.
		# Use template lxrkernel.conf.

		# Data storage subsection #
		#=========================#

		# The DBI identifier for the database to use
		# For MySQL, the format is dbi:mysql:dbname=<name>
		# for Postgres, it is dbi:Pg:dbname=<name>;host=localhost
		# for Oracle, it is dbi:Oracle:host=localhost;sid=DEVMMS;port=1521
		# for SQLite, it is dbi:SQLite:dbname=<filename>
	, 'dbname' => 'dbi:mysql:dbname=lxr'
		# If you need to specify the username or password for the database connection,
		# uncomment the following two lines
#	, 'dbuser'		=> 'lxr'
#	, 'dbpass'		=> 'lxrpw'

		# If you need multiple lxr configurations in one database, set different table
		# prefixes for them.
#	, 'dbprefix'	=> 'lxr_'

		# The following two parameters are now automatically
		# generated from 'glimpsedirbase' or 'swishdirbase' and
		# 'virtroot'.
		# They may nevertheless be overridden here by uncommenting
		# them and filling-in an appropriate absolute path.
		# For using glimpse, the directory to store the .glimpse files in is required
#	, 'glimpsedir' => '/path/to/glimpse/databases'
		# Location of swish-e index database files if using swish-e
#	, 'swishdir' => '/path/to/swish/databases'
	}

# ------------------------------------------------------------------------
,	

#@here_tree:
)
