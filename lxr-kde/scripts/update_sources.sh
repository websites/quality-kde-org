#!/bin/bash

# Written by David Faure <faure@kde.org>

logfile="/home/lxr/logs/src_update.log"
rm -f $logfile
date > $logfile

cd ~/kdesrc-build || exit 1
git pull -q || exit 1
cmake -DCMAKE_DISABLE_FIND_PACKAGE_ECM=1 >>$logfile 2>&1 || exit 1
make install 2>&1 >>$logfile
cd

flags="--src-only"
kdesrc-build --rc-file=stable-qt4-kdesrc-buildrc $flags 2>&1 >>$logfile
kdesrc-build --rc-file=kf5-qt5-kdesrc-buildrc $flags 2>&1 >>$logfile

grep Error $logfile

