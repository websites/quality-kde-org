#!/bin/bash

# Runs genxref for one or all versions

version="$1"

#if [ -z "$version" ]; then
  #echo "Version argument required"
  #exit 1
#fi

logfile="/home/lxr/logs/genxref_update.log"

if ! lockfile -1 -r 1 ~/genxref.lock; then
  if ! kill -s 0 `cat ~/genxref.pid`  &> /dev/null; then
    echo "Ignoring stale lock file."
    ls -l ~/genxref.lock
    rm -f ~/genxref.lock
    lockfile -r 1 ~/genxref.lock
  else
    exit
  fi
fi
echo $$ > ~/genxref.pid

if [ -z "$version" ]; then
    #for version in `cat ~/src/versions`; do
        #echo "<p>Updating index since `date`..." > ~/public_html/lxr_status_$version
    #done
    VERSIONARG="--allversions"
    REINDEX="--reindexall"
else
    VERSIONARG="--version=$version"
    REINDEX=""
    #echo "<p>Updating index since `date`..." > ~/public_html/lxr_status_$version
fi

echo "Starting genxref $REINDEX $VERSIONARG at `date`" >> $logfile
cd ~/lxr
# To see detailed log: 2> ~/logs/genxref.debug.log
# Otherwise: 2>/dev/null
./genxref $REINDEX --url=https://lxr.kde.org $VERSIONARG > ~/logs/genxref.log 2>/dev/null
echo "genxref done at `date`" >> $logfile

#if [ -z "$version" ]; then
    #for version in `cat ~/src/versions`; do
        #echo "<p>Last updated `date`" > ~/public_html/lxr_status_$version
    #done
#else
    #echo "<p>Last updated `date`" > ~/public_html/lxr_status_$version
#fi

rm -f ~/genxref.pid
rm -f ~/genxref.lock
