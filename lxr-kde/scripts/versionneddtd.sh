#!/bin/sh
if [ $# -ne 3 ]; then
  echo "Usage: $0 <source-path> <filename> <website-path>"
  exit 1
fi
#exec 2>/dev/null >/dev/null

LOCKFILE=$HOME/versionneddtd.sh.lock
if test -f $LOCKFILE; then
  echo "$0: $LOCKFILE already present on $HOSTNAME" | mail -s "ADMIN: lockfile problem" faure@kde.org
  exit 2
fi
lockfile $LOCKFILE
# Don't leave the lockfile if killed
trap "rm -f $LOCKFILE" 1 2 3 15

SRCDIR=$1
filename=$2
WEB=$3

cd $SRCDIR || exit 3
# We assume the regular updating already did a git pull in $SRCDIR
done=0

version=`head -2 $filename | grep -wv xml | sed -e 's/<\!-- *//;s/ *-->//;s/.*\bv\([0-9\.]*\).*/\1/'`
if [ -z "$version" ]; then
  echo "$0: couldn't determine version from $SRCDIR/$filename!" | mail -s "ADMIN: script problem" faure@kde.org
  exit 4
fi

cd $HOME || exit 5
if [ ! -d $WEB ]; then
  echo "$0: couldn't find $WEB!"
  exit 6
fi

if [ ! -d $WEB/$version ]; then
  mkdir $WEB/$version &&
  cd $WEB &&
  svn add $version
fi

DSTDIR=$WEB/$version
cd $DSTDIR || exit 7
svn up $filename >/dev/null
existed=0
if [ -f $filename ]; then
  existed=1
fi

   src=$SRCDIR/$filename
   dst=$DSTDIR/$filename
   #echo $src $dst
   if ! diff -q -IId: $src $dst; then
       cp -f $src $dst
       done=1
   fi

if [ $existed -eq 0 ]; then
  cd $DSTDIR
  svn add $filename
fi

if test "$done" -eq 1; then
  cd $DSTDIR || exit 8
  svn ci -m 'CVS_SILENT autoupdated' $filename
fi

rm -f $LOCKFILE

