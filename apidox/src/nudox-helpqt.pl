#! /usr/bin/env perl

$docdir = $ARGV[0];
$doctag = $ARGV[1];
$srcdir = $ARGV[2];
$module = $ARGV[3];

# Return value not interesting, actually
$needqttags = &lookforqttags();
&generatedoxyfile();
&generatetagsfile();
&completedoxyfile();

###
#
# Search for QT tagfile
#
# As documented:
#	Return 0 = TAG exists and is a file and QTDOCDIR is set
#	Return 1 = TAG doesn't exist
# perl nudox-helpqt.pl "$QTDOCDIR" "$QTDOCTAG" "$MODULE"

sub tagsnotfound() {
	print "! Qt documentation can not be found.\n";
	print "! Use --qtdocdir and --qtdoctag.\n";
	exit( 1 );
}

sub tagsexist() {
	print "# Using Qt tag file '$doctag'\n";
	system("cp \"$doctag\" \"$module\"/qt.tag");
	writetags();
}

sub tagsneeded() {
	print "# Generating Qt tag file from '$docdir'\n";
	system("doxytag -t \"$module\"/qt.tag \"$docdir\"");
	writetags();
}

sub writetags() {
	open TAGFILE, ">$module/tagfiles" or die "! Can not write tagfiles\n";
	print TAGFILE "\"qt.tag=$docdir\"";
	close TAGFILE;
}

# Look for the QT tag file and exit(1) if not found.
sub lookforqttags() {
	if ( -f $doctag ) {
		print "# Checking Qt tags file '$doctag'\n";
		if (!open TAGF, "<$doctag") {
			print "! Can not read given file '$doctag'\n";
			tagsnotfound();
		}

		$first = <TAGF>;
		if (! $first =~ /tagfile/ ) {
			print "! File '$doctag' does not seem to be a tag file\n";
			tagsnotfound();
		}

		if ( $docdir =~ /^$/ ) {
			print "! No documentation directory given\n";
			tagsnotfound();
		}

		if ( not -d $docdir ) {
			print "# Qt documentation directory is not a directory,\n";
			print "# but I'll use it anyway because of the tag file.\n";
		}

		tagsexist();
		return 0;
	}

	if ( -d $docdir and -f "$docdir/index.html" and -f "$docdir/qstring.html" ) {
		print "# Qt documentation will be generated from '$docdir'\n";
		tagsneeded();
		return 0;
	}

	print "! No documentation directory given\n";
	tagsnotfound();
}

sub generatedoxyfile() {
	print "# Generating initial Doxygen file ...\n";
	system("doxygen -g \"$module/Doxyfile.in\" > /dev/null 2>&1");
	open( DOXYFILE, ">>$module/Doxyfile.in") or die "! Can not open Doxyfile.in\n";

	print DOXYFILE "
RECURSIVE = YES
GENERATE_LATEX = NO
GENERATE_HTML = NO # Turned on later
QUIET = YES
";
	close( DOXYFILE );
}


sub generatetagsfile() {
	return if -f "$module/$module.tag" ;
	print "# Generating tags file ...\n";

	system("cp \"$module/Doxyfile.in\" \"$module/Doxyfile.tags\"");
	open( DOXYFILE, ">>$module/Doxyfile.tags" ) or die "! Can not open Doxyfile.tags\n";
	print DOXYFILE "
INPUT = \"$srcdir\"
OUTPUT_DIRECTORY = \"$module\"
GENERATE_TAGFILE = \"$module/$module.tag\"

EXTRACT_ALL = YES

WARNINGS = NO
WARN_IF_UNDOCUMENTED = NO
WARN_IF_DOC_ERROR = NO
WARN_NO_PARAMDOC = NO
";
	close( DOXYFILE );
	system( "doxygen \"$module/Doxyfile.tags\" > /dev/null 2>&1 ; rm -f \"$module/Doxyfile.tags\"" );
}

sub completedoxyfile() {
	open( DOXYFILE, ">>$module/Doxyfile.in" ) or die "! Can not open Doxyfile.in\n";
	print DOXYFILE "
GENERATE_HTML = YES
HTML_OUTPUT = \".\"

EXTRACT_ALL = NO

DISABLE_INDEX = YES

ALPHABETICAL_INDEX = YES
IGNORE_PREFIX = K Q

WARNINGS = YES
WARN_IF_UNDOCUMENTED = YES
WARN_IF_DOC_ERROR = YES
WARN_NO_PARAMDOC = YES
";
}



