#! /bin/bash
VERBOSE=1
VERSION=""

# Read arguments...
test "x--quiet" = "x$1" && { shift ; VERBOSE=0 ; }
test "x--version" = "x$1" && { shift ; VERSION="$1" ; shift ; }
test -z "$VERSION" && { echo "No version given"; exit 1 ; }

# Configuration
# APIPATH: top-level for API stuff
APIPATH=/srv/www/api.kde.org
# OUTPATH: where the final output goes
OUTPATH=$APIPATH/apidocs/apidox-$VERSION
# TEMPPATH: where the temporary output goes
TEMPPATH=/srv/tmp/tempdocs/apidox-$VERSION
# TAGPATH: where tag files (i.e, qtX.Y.tag) reside
TAGPATH=$HOME/quality-kde-org/apidox/data
# BINPATH: where the helper programs (i.e. doxylog2html.pl) reside
BINPATH=/home/api/bin
# SRCPREFIX: where the top-level KDE source code resides
SRCPREFIX=/srv/sources
# LOGPATH: where logs get moved
LOGPATH=/srv/logs/api/$VERSION
# SRCPATH: where the source code for this component resides
SRCPATH=$SRCPREFIX/$VERSION
# DOXYHOME: where the doxygen stuff livs
DOXYHOME=$HOME/kdelibs/doc
DOXYSH=$DOXYHOME/api/doxygen.sh
test -s "$DOXYSH" || { echo "*** NO DOXYGEN.SH" ; exit ; }
DOXDATAPATH=$DOXYHOME/common

# Determine the modules we need to build the documentation of...
MODULES=`ls $SRCPATH`

# If argument given, use it as modules list to run
test -n "$1" && MODULES="$1"

# Prepare to run....
test -d "$OUTPATH" || { echo "No path $OUTPATH -- creating it" ; mkdir -p $OUTPATH || exit 1 ; }
test -d "$TEMPPATH" || { echo "No path $TEMPPATH -- creating it" ; mkdir -p $TEMPPATH || exit 1 ; }
test -d "$LOGPATH" || { echo "No path $LOGPATH -- creating it" ; mkdir -p $LOGPATH || exit 1 ; }
test -d "$SRCPATH" || { echo "No source path $SRCPATH" ; exit 1 ; }
cd "$TEMPPATH" || { echo "Cannot cd into $TEMPPATH" ; exit 1 ; }

if [ $VERBOSE ]; then
  echo "** Fetching kde-accounts from SVN"
fi
svn export svn://anonsvn.kde.org/home/kde/trunk/kde-common/accounts $TEMPPATH/kde-accounts

# Make a nice title
component=`echo $VERSION | sed s/kde-// | sed s/kde// | sed 's/^[a-z]/\U&/'`
title="KDE $component API Reference"

# Log it...
if [ $VERBOSE ]; then
  echo "* Generating APIDOX for $VERSION"
  echo "* Title \"$title\""
fi

# Specify url and path to Qt documentation....
case "$VERSION" in
  kde-3.5)
    export QTDOCDIR=https://doc.qt.io/archives/3.3
    export QTDOCTAG=$TAGPATH/qt3.3.tag
    ;;
  kde-4.14)
    export QTDOCDIR=https://doc.qt.io/archives/qt-4.8
    export QTDOCTAG=$TAGPATH/qt4.8.tag
    ;;
  * )
    export QTDOCDIR=https://doc.qt.io/archives/qt-4.8
    export QTDOCTAG=$TAGPATH/qt4.8.tag
    ;;
esac

# Generate the documentation for each module found
for i in $MODULES; do
    if [ $VERBOSE ]; then
      echo "** Generating APIDOX for $SRCPATH/$i"
    fi

    # Cleanup from previous generations....
    rm -rf $TEMPPATH/$i-*
    rm -f $TEMPPATH/$i.tar.gz $TEMPPATH/$i.log

    # Make sure the sources for this module exist...
    test -d "$SRCPATH/$i" || { echo "$SRCPATH/$i does not exist, skipping" ; continue ; }

    # Build the documentation!
    DOXARGS="$SRCPATH/$i"
    if [ $VERBOSE ]; then
      echo ** Running \
          $DOXYSH --installdir=$TEMPPATH \
	  --doxdatadir=$DOXDATAPATH \
          --title=\""$title"\" \
          --api-searchbox --no-cleanup --preprocess \
          --accountsfile=$TEMPPATH/kde-accounts \
          $DOXARGS
    fi
    # Using doxygen > 1.8.10 broke the mainpage.dox links: we explicitely ask for 1.8.7
    export PATH=$HOME/doxygen-1.8.7/bin:$PATH #pick up our local doxygen build
    sh $DOXYSH \
        --installdir=$TEMPPATH \
        --doxdatadir=$DOXDATAPATH \
        --title="$title" \
        --api-searchbox \
        --no-cleanup \
        --preprocess \
        --accountsfile=~$TEMPPATH/kde-accounts \
        $DOXARGS 2>&1 \
        | grep -v '^Error: Unexpected tag `' \
        | grep -v '^[Ww]arning: ignoring unsupported tag' \
        | grep -v '^[Ww]arning: ignoring unknown tag' \
        | sed s+/tempdocs/+/apidocs/+g > "$i.new"
    mv "$i.new" "$i.log"

    # If we need to, log the number of errors....
    if [ $VERBOSE ]; then
       AMT=`grep ^$SRCPATH $i.log | wc -l`
       expr 0 + "$AMT" > /dev/null 2>&1 || AMT="0"
       echo "** Number of errors: $AMT"
       echo "** Number of applications: " `grep 'Creating apidox' "$i.log" | wc -l`
       echo "** Written to $i.log"
    fi

    # clean-up unneeded files
    ( cd $TEMPPATH; find . -name "*.map" -o -name "*.md5" | xargs rm -f )

    # create one big tar file of all the goodies
    ( cd $TEMPPATH; tar czf $i.tar.gz $i-apidocs )

    # Move the temporary output to the real apidocs location
    rm -rf $OUTPATH/$i-*
    mv -f $TEMPPATH/$i-* $OUTPATH
    rm -f $OUTPATH/$i.tar.gz
    mv -f $TEMPPATH/$i.tar.gz $OUTPATH
    rm -f $LOGPATH/$i.log
    mv -f $TEMPPATH/$i.log $LOGPATH
done

if [ $VERBOSE ]; then
  echo "* APIDOX Generation complete"
fi
