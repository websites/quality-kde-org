#! /usr/bin/env perl

use Template;

$doxdir = $ARGV[0];
$style = $ARGV[1];
$module = $ARGV[2];
$srcdir = $ARGV[3];

-d $module or die "! Target '$module' is not a directory.";
-d $srcdir or die "! Source '$srcdir' is not a directory.";
$doxdir and $module and $srcdir or die "! Insufficient arguments";

$stylefile = "nudox-";
$stylefile = "nudox-$style-" if $style;

$tt = Template->new({
	OUTPUT_PATH => $module,
	RELATIVE => 1,
	ABSOLUTE => 1
	}) || die "! ", Template->error(), "\n";


@subdirs = ();
while(<STDIN>) {
	chomp;
	s/^\.\///;
	-f "$srcdir/$_/Mainpage.dox" or die "Listed directory '$_' has no Mainpage.dox.";
	push @subdirs, $_;
}

%doxysettingsmap = (
	"NAME" => "PROJECT_NAME",
	"VERSION" => "PROJECT_NUMBER",
	"ENABLE" => "# ENABLED",
	"TOOLTIP" => "# TOOLTIP"
	) ;
@doxytoplevelsettings = ( "PROJECT_NAME", "PROJECT_NUMBER" );

sub extract_doxysettings {
	$filename = $_[0];
	%v = ();
	open( MAINPAGE, "<$filename" ) or die "Can not open '$filename'.";
	while ( <MAINPAGE> ) {
		next unless /^\/\/ DOXYGEN_/;
		chomp;
		s/^.. DOXYGEN_//;
		$name = $_;
		$value = $_;
		$name =~ s/[\s=].*//;
		$value =~ s/^[^\s=]*\s*=\s*//;
		$name = $doxysettingsmap{$name} if exists $doxysettingsmap{$name};

		$tvalue = $value;
		while ($value =~ /\\$/) {
			$value = <MAINPAGE>;
			chomp $value;
			die unless $value =~ /^\/\//;
			$value =~ s/^\/\/\s*//;
			$tvalue .= "\n\t$value";
		}

		$v{$name} = $tvalue;
	}
	close( MAINPAGE );
	return %v;
}


%doxy_toplevel = extract_doxysettings("$srcdir/Mainpage.dox");

open( SUBDIRHTML, ">$module/subdirs.html" ) or die "! Can not create subdir HTML\n";
@l = ();
$depth = 0;
for $dir (@subdirs) {
	my @dirs = split( "/", $dir );
	$i = 0;
	while ($l[$i] eq $dirs[$i]) {
		$i++;
	}
	if ($i>$depth) {
		$j=$depth;
		while ($j<$i) {
			print SUBDIRHTML "<ul style=\"padding-left:2em;\" >";
			$j++;
		}
	}
	if ($i<$depth) {
		$j=$i;
		while ($j<$depth) {
			print SUBDIRHTML "</ul>";
			$j++;
		}
	}

	%doxy_local = extract_doxysettings("$srcdir/$dir/Mainpage.dox");

	print SUBDIRHTML "<li><a href=\"[%top%]/";
	print SUBDIRHTML $dir;
	print SUBDIRHTML "/index.html\"";
	# Need HTML escaping & checking that quotes are OK.
	# FIXME
	print SUBDIRHTML " title=", $doxy_local{'# TOOLTIP'} if $doxy_local{'# TOOLTIP'};
	print SUBDIRHTML ">";
	print SUBDIRHTML $dirs[$i];
	print SUBDIRHTML "</a></li>\n";

	@l = @dirs;
	$depth = $i;
}
if (0<$depth) {
	while (0<$depth) {
		print SUBDIRHTML "</ul>";
		$depth--;
	}
}
close( SUBDIRHTML );

open( SUBDIRS, ">$module/subdirs" ) or die "! Can not create subdir list\n";

my $hdr = "header.html";
my $ftr = "footer.html";

for $dir (@subdirs,".") {
	# Skip directories that are still KDE3 structure (used in playground)
	next if -f "$srcdir/$dir/Makefile.am";

	print SUBDIRS "$dir\n";
	mkdir "$module/$dir";

	open( OUTF , ">$module/$dir/Doxyfile.local" ) or die "Can not write Doxygen settings in '$dir'.";

	print OUTF "INPUT = \"$srcdir/$dir/\"\n";
	print OUTF "OUTPUT_DIRECTORY = \"$module/$dir/\"\n";
	print OUTF "RECURSIVE = NO\n" if $dir eq ".";

	%doxy_local = extract_doxysettings("$srcdir/$dir/Mainpage.dox");
	print OUTF "# Local settings\n";
	foreach $k (keys(%doxy_local)) {
		print OUTF $k . " = " . $doxy_local{$k} . "\n";
	}
	print OUTF "# Toplevel settings\n";
	foreach $k (@doxytoplevelsettings) {
		if (exists($doxy_toplevel{$k}) and not exists($doxy_local{$k})) {
			print OUTF $k . " = " . $doxy_toplevel{$k} . "\n";
		}
	}

	print OUTF "# Calculated settings\n";
	@subsubs = ();
	for $d (@subdirs) {
		next unless $d =~ /^$dir\//;
		push @subsubs, "$srcdir/$d/";
	}

	print OUTF "EXCLUDE = ", join( " \\\n", @subsubs ), "\n" if @subsubs;

	if ($style) {
		my $breadcrumb_path = "";
		my @l = ();
		my @up = ( "." );
		for $i (split("/","$module/$dir")) {
			next unless $i;
			push @l,$i;
			push @up,"..";
		}
		pop @up;
		my $toplevel = join( "/", @up );

		for $i (@l) {
			$item=$i;
			$item="<a href=\"" . join("/",@up) . "/index.html\">" . $i . "</a>" if @up;
			$breadcrumb_path .= "<li>$item</li>\n";
			pop @up;
		}
		$breadcrumb_path="" if $dir eq ".";
		$toplevel="." if $dir eq ".";

		my $vars = {
			top => $toplevel,
			site => "http://conference2006.kde.org",
			module => $module,
			breadcrumbs => $breadcrumb_path
		};

		print "# Generating template for '$dir'\n";
		my $flavour = "";
		$flavour = "top-" if $dir eq ".";

		if ($tt->process( "$doxdir/$stylefile$flavour$hdr", $vars, "$dir/$hdr" )) {
			print OUTF "HTML_HEADER=\"$module/$dir/$hdr\"\n";
		} else {
			die "! ", $tt->error(), "\n";
		}

		if ($tt->process( "$doxdir/$stylefile$flavour$ftr", $vars, "$dir/$ftr" )) {
			print OUTF "HTML_FOOTER=\"$module/$dir/$ftr\"\n"
		} else {
			die "! ", $tt->error(), "\n";
		}
	}

	close( OUTF );
}

close( SUBDIRS );

