#! /bin/bash
VERBOSE=1

# Read arguments...
test "x--quiet" = "x$1" && { shift ; VERBOSE=0 ; }

# Configuration
# APIPATH: top-level for API stuff
APIPATH=/srv/www/api.kde.org
# OUTPATH: where the final output goes
OUTPATH=$APIPATH/apidocs/apidox-frameworks
# TEMPPATH: where the temporary output goes
TEMPPATH=/srv/tmp/tempdocs/apidox-kapidox
# TAGPATH: where tag files (i.e, qtX.Y.tag) reside
TAGPATH=$HOME/quality-kde-org/apidox/data
# TOPSRC: where the top-level KDE source code resides that kapidox covers
TOPSRC=/srv/sources/kapidox-system
# LOGPATH: where logs get moved
LOGPATH=/srv/logs/api/kapidox

# Prepare to run....
test -d "$OUTPATH" || { echo "No path $OUTPATH -- creating it" ; mkdir -p $OUTPATH || exit 1 ; }
test -d "$TEMPPATH" || { echo "No path $TEMPPATH -- creating it" ; mkdir -p $TEMPPATH || exit 1 ; }
test -d "$LOGPATH" || { echo "No path $LOGPATH -- creating it" ; mkdir -p $LOGPATH || exit 1 ; }
test -d "$TOPSRC" || { echo "No source path $TOPSRC" ; exit 1 ; }
cd "$TEMPPATH" || { echo "Cannot cd into $TEMPPATH" ; exit 1 ; }

# Log it...
if [ $VERBOSE ]; then
  echo "* Generating APIDOX for $TOPSRC with kapidox"
fi

# Specify url and path to Qt documentation....
QTDOCLINK=https://doc.qt.io/qt-5
QTDOCTAGSDIR=$TAGPATH/5.15
QTHELPGENERATOR=/usr/lib64/qt5/bin/qhelpgenerator
DOXYGEN=$HOME/doxygen-src/bin/doxygen
DOXYSH=/home/api/kapidox/src/kapidox_generate
test -s "$DOXYSH" || { echo "*** NO KAPIDOX" ; exit ; }
DEPDIAGRAMDOTDIR=$HOME/depdiagram-output/

# Cleanup from previous generations....
rm -rf $TEMPPATH/*

# Set the command that does the dox generation
DOXCMD="$DOXYSH \
            --doxygen $DOXYGEN \
            --qtdoc-dir $QTDOCTAGSDIR \
            --qtdoc-link $QTDOCLINK \
            --qtdoc-flatten-links \
            --qhp \
            --qhelpgenerator $QTHELPGENERATOR \
            $TOPSRC"
# currently disabled as newer cmake generates new variants of dot files
# that kapidox fails to digest -> https://invent.kde.org/frameworks/kapidox/-/issues/2
# when re-enabling, also needs re-enabling of generation & feeding of dot files by build.kde.org
# via ci-tooling's generate-dependency-diagram-data.py, also disabled now
#             --depdiagram-dot-dir $DEPDIAGRAMDOTDIR \

# Have kapidox find its specific tools, like py_filter
export PATH=$HOME/bin/kapidox:$PATH

# Build the documentation!
if [ $VERBOSE ]; then
  echo "** Running $DOXCMD"
fi
$DOXCMD 2>&1 \
    | grep -v '^Error: Unexpected tag `' \
    | grep -v '^[Ww]arning: ignoring unsupported tag' \
    | grep -v '^[Ww]arning: ignoring unknown tag' \
    | sed s+/tempdocs/+/apidocs/+g > "../kapidox.log.new"
CMDRESULT=$PIPESTATUS
# If not 0 (= failure)
if [ $CMDRESULT -ne 0 ] ; then
    echo " ** FATAL ERROR during script: not updated. **" >> "../kapidox.log.new"
fi
mv "../kapidox.log.new" "kapidox.log"

# If we need to, log the number of errors....
if [ $VERBOSE ]; then
   AMT=`grep ^$TOPSRC kapidox.log | wc -l`
   expr 0 + "$AMT" > /dev/null 2>&1 || AMT="0"
   echo "** Number of errors: $AMT"
   echo "** Number of applications: " `grep 'Creating apidox' "kapidox.log" | wc -l`
   echo "** Written to kapidox.log"
fi

# clean-up unneeded files
( cd $TEMPPATH; find . -name "*.map" -o -name "*.md5" | xargs rm -f )

# create one big tar file of all the goodies
( cd $TEMPPATH; tar czf kapidox.tar.gz . )

# Move the temporary output to the real apidocs location
rm -f $LOGPATH/kapidox.log
cat $TEMPPATH/kapidox.log | grep -v "INFO " > $LOGPATH/kapidox.log
# Move only if result is 0 (success)
if [ $CMDRESULT -eq 0 ] ; then
    if [ -d $OUTPATH/frameworks5-apidocs ]; then
        rm -rf $OUTPATH/frameworks5-apidocs/*
    else
        mkdir -p $OUTPATH/frameworks5-apidocs
    fi
    mv -f $TEMPPATH/* $OUTPATH/frameworks5-apidocs
else
    echo "** Got an error somewhere, code not moved"
fi

if [ $VERBOSE ]; then
  echo "* APIDOX Generation complete"
fi
