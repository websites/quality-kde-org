<?php
  $page_title = "API Reference Index";
  include 'header.inc';
?>

<p>
The reference guides for the KDE APIs -- for KDE2 all the way to the
current development version -- are collected here.
We assume you are already familiar with the excellent 
<a href="http://doc.qt.io/qt-4.8/" target="_new">Qt4</a> and
<a href="http://doc.qt.io/qt-5/" target="_new">Qt5</a>
documentation.
<a href="http://techbase.kde.org" target="_new">TechBase</a>
is the right place to start looking for general development
information for KDE.
There are only reference guides here.
</p>
<p>
To obtain a gzip compressed tar file containing the documentation, click on the <img src="/img/14x14save.png" alt="[download]"> images, which are immediately adjacent to many of the listed items.
</p>
<p>
To obtain a version of the documentation for use in
<a href="http://doc.qt.digia.com/latest/assistant-manual.html">Digia Qt Assistant</a>, click on the "[qch]" links, which are immediately adjacent to some of the listed items. (In Qt assistant, go into Edit->Preferences->Documentation and [Add] the .qch file.)
</p>
<p>
Man pages are also provided for some modules. Click on the "[man]" links, also immediately adjacent to some of the listed items to download a bzip2 compressed tar file containing the man pages for the corresponding module. (Uncompress and untar these files into a standard MANPATH directory.)
</p>

<table>
<?php require_once 'functions.php' ?>

<tr>

<th style="text-align:center" width="36%">
Frameworks
</th>
<th style="text-align:center" width="36%">
Others
</th>
</tr>
<tr>

<td valign="top">
<ul li style="list-style-type: none;">
<?php
  frameworksqchman('frameworks','frameworks5');
?>
</ul>
</td>
<td valign="top">
<ul>
<li><a href="/other.php">Other KDE Software</a></li>
<li><a href="/history4.php">KDE4 Versions</a></li>
<li><a href="/history.php">KDE3 and older versions</a></li>
</ul>
</td>
</tr>

</table>

<?php include 'footer.inc' ?>

