<?php
  $page_title = "API Reference Index";
  include 'header.inc';
  require_once 'functions.php';
?>

<p>
Reference guides for KDE 4 releases, KDE 3 releases, and older
</p>

<table>
<th style="text-align:center" width="25%">KDE 4.14</th>
<th style="text-align:center" width="25%">KDE 3.5</th>
<th style="text-align:center" width="25%">Even older</th>
</tr>
<tr>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidoxqchman('4.14','kdelibs');
  apidoxqchman('4.14','kdepimlibs');
  apidox('4.14','kdeaccessibility');
  apidox('4.14','kde-baseapps');
  apidox('4.14','kde-runtime');
  apidox('4.14','kdeedu');
  apidox('4.14','kdegames');
  apidox('4.14','kdegraphics');
  apidox('4.14','kdemultimedia');
  apidox('4.14','kdenetwork');
  apidox('4.14','kdepim');
  apidox('4.14','kdeplasma-addons');
  apidox('4.14','kdesdk');
  apidox('4.14','kdeutils');
  apidox('4.14','kdewebdev');
  apidox4('pykde-4.7','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('3.5','kdelibs');
  apidox('3.5','kdebase');
  apidox('3.5','kdeedu');
  apidox('3.5','kdegames');
  apidox('3.5','kdegraphics');
  apidox('3.5','kdemultimedia');
  apidox('3.5','kdenetwork');
  apidox('3.5','kdepim');
?>
</ul>
</td>
<td valign="top">
<ul>
<li><a href="3.1-api/classref">KDE 3.1 kdelibs</a></li>
<li><a href="3.0-api/classref">KDE 3.0 kdelibs</a></li>
<li><a href="2.2-api/classref">KDE 2.2 kdelibs</a></li>
<li><a href="2.1-api/classref">KDE 2.1 kdelibs</a></li>
<li><a href="2.0-api/classref">KDE 2.0 kdelibs</a></li>
</ul>
</td>
</tr>

</table>


<?php include 'footer.inc' ?>
