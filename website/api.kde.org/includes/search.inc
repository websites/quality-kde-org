<div class="menutitle"><div>
  <h2 id="cp-menu-search"><?php i18n("Search")?></h2>
</div></div>

<div style="text-align: left;">

<?php
  if (array_key_exists('miss',$_GET) && $_GET["miss"] == 1) {
    echo("<small style='color:#f00'>No results found!</small>");
  }
?>

<form action="/mapsearcher.php" method="get">
<input type="text" name="class" value="<?php if (array_key_exists('class',$_GET)) { echo htmlentities($_GET["class"]); } else { echo "search term"; } ?>" style="width:100%;" onClick="this.value='';"/><br/>
<select name="module">
  <option>ALL</option>
  <option>kdelibs</option>
  <option>kdepimlibs</option>
  <option>kdebase-apps</option>
  <option>kdebase-runtime</option>
  <option>kdebase-workspace</option>
  <option>kdeedu</option>
  <option>kdegames</option>
  <option>kdegraphics</option>
  <option>kdemultimedia</option>
  <option>kdenetwork</option>
  <option>kdepim</option>
  <option>kdepim-runtime</option>
  <option>kdeplasma-addons</option>
  <option>kdesdk</option>
  <option>kdevelop</option>
  <option>kdeutils</option>
  <option>kdewebdev</option>
  <option>kdesupport</option>
</select>
<select name="version">
  <option>ALL</option>
  <option>4.x</option>
  <option>4.14</option>
  <option>3.5</option>
  <option>frameworks</option>
  <option>extragear</option>
  <option>kdesupport</option>
</select><br/>
<input type="submit" name="go" value="Go" />
</form>

<!--
<hr>
<font color="red">Even More Advanced Search (Actually-In-Testing)</font>
<form action="/metellius.classmapper.php" method="get">
<input type="text" name="class" value="<?php if (array_key_exists('class',$_GET)) { echo $_GET["class"]; } else { echo "[class, method or regexp]"; } ?>" style="width:100%;" onClick="this.value='';"/><br/>
<select name="module">
  <option>ALL</option>
  <option>kdelibs</option>
  <option>kdepimlibs</option>
  <option>kdebase-apps</option>
  <option>kdebase-runtime</option>
  <option>kdebase-workspace</option>
  <option>kdeedu</option>
  <option>kdegames</option>
  <option>kdegraphics</option>
  <option>kdemultimedia</option>
  <option>kdenetwork</option>
  <option>kdepim</option>
  <option>kdepim-runtime</option>
  <option>kdesdk</option>
  <option>kdevelop</option>
  <option>kdeutils</option>
  <option>kdewebdev</option>
  <option>kdesupport</option>
</select>
<select name="version">
  <option>4.x</option>
  <option>4.5</option>
  <option>3.5</option>
  <option>kdesupport</option>
  <option>ALL</option>
</select><br/>
<input type="submit" name="go" value="Go" />
</form>
-->

</div>
