<?php
$this->setName( "KDE API Reference" );

$section =& $this->appendSection("Navigation");

$section->appendLink("Main Page","/index.php",false);
$section->appendLink("Old KDE Versions","/history.php",false);

$section =& $this->appendSection("Related");

$section->appendLink("API Doc Tutorial","https://community.kde.org/Guidelines_and_HOWTOs/API_Documentation",false);
$section->appendLink("KDE TechBase","https://techbase.kde.org/",false);
$section->appendLink("KDE CMake Modules","/cmake/modules.html",false);
$section->appendLink("Extra CMake Modules", "/ecm/",false);
