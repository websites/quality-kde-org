<?php
  $page_title = "API Reference Index";
  include 'header.inc';

  function apidox($directory, $displayName) {
    print '<li><a href="appscomplete-api/' . $directory . '-apidocs/" title="View online API documentation for complete code of ' . $displayName . '">';
    print $displayName . "</a></li>\n\n";
  }
?>

<p>
Reference guides for complete code of some products (based on legacy documentation system)
</p>

<table>
<tr>
<th style="text-align:center" width="36%">
Software
</th>
</tr>

<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('calligra', 'Calligra Suite');
  apidox('kdevelop', 'KDevelop');
  apidox('kexi', 'Kexi');
  apidox('calligraplan', 'Calligra Plan');
  apidox('kphotoalbum', 'KPhotoAlbum');
  apidox('krita', 'Krita');
  apidox('krusader', 'Krusader');
  apidox('kstars', 'KStars');
  apidox('skrooge', 'Skrooge');
  # HERE lists further
  # apidox('directory', 'Name of App');
?>
</ul>
</td>

</table>


<?php include 'footer.inc' ?>
