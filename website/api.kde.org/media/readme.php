<?php
$page_title = "KDE.org Web Site Tutorial";
$site_external = true;
include "header.inc";
?>

<p><b>
A new tutorial is being written at the moment,
look a <a href="http://developernew.kde.org/Projects/kde.org">this page</a> to get a overview
about the current state. It shows how to setup a staging vhost for the /media (Capacity) framework
and explains the internals of it's usage.
</b>
</p>

<p>
<ul>
<li><a href="http://developernew.kde.org/Projects/kde.org/Staging_Setup">Setup HOWTO your Apache for testing the *.kde.org sites.</a></li>
<li><a href="http://developernew.kde.org/Projects/kde.org/Capacity_HOWTO">New HOWTO for the /media-Framework (Capacity)</a></li>
</ul>
</p>

<?php
 include "footer.inc";
?>
