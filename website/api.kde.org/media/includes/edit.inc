<?php
include_once ("diff.inc");

if (isset ($_POST ['text0'])) {
    $newversion = "";
    $count = 0;
    while (isset ($_POST ['text'.$count])) {
        if (ini_get ("magic_quotes_gpc") > 0)
            $newversion .= str_replace ("\r", "", stripslashes ($_POST ['text'.$count]));
        else
            $newversion .= str_replace ("\r", "", $_POST ['text'.$count]);

        $newversion .= rawurldecode ($_POST ['code'.$count]);
        $count++;
    }
}
else
    $newversion = file_get_contents ($_SERVER['SCRIPT_FILENAME']);

$blocks = separate_php_code ($newversion);

if ($blocks === false || count ($blocks) < 2 || trim ($blocks [0]["text"]) || ! trim ($blocks [count ($blocks)-1]["code"])) {
    echo "Error separating out the PHP tags! Please edit the page off-line.";
}
else {
    if (isset ($_POST ['text0'])) {
        foreach ($blocks as $key=>$block) {
            if (trim ($block ["text"])) {
                echo $block ["text"];
                echo '<hr/>';
            }
        }

        echo '<form action="/media/download.php" method="post">';
        echo '<input type="hidden" name="filename" value="'.basename ($_SERVER['SCRIPT_FILENAME']).'"/>';
        echo '<input type="hidden" name="download" value="'.rawurlencode ($newversion).'"/>';
        echo '<input type="submit" value="Download the altered page (as PHP file)"/>';
        echo '</form>';
        echo '<hr/>';

        echo '<h2>Diff of the changes</h2>';
        $diff = PHPDiff (file_get_contents ($_SERVER['SCRIPT_FILENAME']), $newversion);
        if (trim ($diff)) {
            $diff='--- '.$_SERVER['SCRIPT_FILENAME']." (original) \n"
                .'+++ '.$_SERVER['SCRIPT_FILENAME']." (new version) \n"
                .$diff;

            echo '<pre>'.htmlspecialchars ($diff).'</pre>';

            echo '<form action="/media/download.php" method="post">';
            echo '<input type="hidden" name="filename" value="'.basename ($_SERVER['SCRIPT_FILENAME']).'.diff"/>';
            echo '<input type="hidden" name="download" value="'.rawurlencode ($diff).'"/>';
            echo '<input type="submit" value="Download diff file"/> ';
            echo '<a href="mailto:kde-www@kde.org?subject='
                . rawurlencode ('Patch for ' . $_SERVER['SCRIPT_FILENAME'])
                . '&amp;body='
                . rawurlencode ("\n"."\n"."\n"."\n".$diff)
                . '">Email diff to the web team</a>';
            echo '</form>';
        }
        else
            echo "No changes";

        echo '<hr/>';
    }

    echo '<p>PHP code cannot be edited here and will be skipped in the preview. If you need to change it, then edit the file off-line and set up your own local mirror for testing.</p>';
    echo '<form action="" method="post"><input type="hidden" name="edit" value="1"/>';
    foreach ($blocks as $key=>$block) {
        if (trim ($block ["text"])) {
            echo '<textarea name="text'.$key.'" style="width:100%; height:500px;" cols="50" rows="5">';
            echo htmlspecialchars ($block ["text"]);
            echo '</textarea>';
            echo '<hr/>';
        }
        else {
            echo '<input type="hidden" name="text'.$key.'" value="'.htmlspecialchars ($block ["text"]).'"/>';
        }
       echo '<input type="hidden" name="code'.$key.'" value="'.rawurlencode ($block ["code"]).'"/>';
    }
    echo '<input type="submit" value="Preview"/>';
    echo '</form>';
}

function separate_php_code (const $text) {
    $blocks = array ();

    $textblock = "";
    $position = 0;
    while ($position < strlen ($text)) {
        if (substr ($text, $position, 2) === "<?") {
            $codeblock = parse_php_code ($text, $position);
            if ($codeblock === false)
                return false;

            while ($position < strlen ($text) && trim ($text [$position]) == "") {
                $codeblock .= $text [$position];
                $position++;
            }

            $trimmedtext = rtrim ($textblock);
            $codeblock = substr ($textblock, strlen ($trimmedtext)) . $codeblock;
            $blocks [] = array ("text"=>$trimmedtext, "code"=>$codeblock);
            $textblock = "";
        }
        else {
            $textblock .= $text [$position];
            $position++;
        }
    }

    if (trim ($textblock))
        $blocks [] = array ("text"=>$textblock, "code"=>"");

    return $blocks;
}

function parse_php_code (const $text, &$position) {
    if (substr ($text, $position, 5) !== "<?php")
        return false;

    $position += 5;

    $codeblock = "<?php";
    while (substr ($text, $position, 2) != "?>") {
        if (substr ($text, $position, 2) == "/*") {
            $newposition = strpos ($text, "*/", $position+2);
            if ($newposition === false)
                return false;
            $codeblock .= substr ($text, $position, $newposition-$position+2);
            $position = $newposition+2;
        }
        elseif (substr ($text, $position, 2) == "//") {
            $newposition = strpos ($text, "\n", $position+2);
            if ($newposition === false)
                return false;
            $codeblock .= substr ($text, $position, $newposition-$position+1);
            $position = $newposition+1;
        }
        elseif ($text [$position] == "#") {
            $newposition = strpos ($text, "\n", $position+1);
            if ($newposition === false)
                return false;
            $codeblock .= substr ($text, $position, $newposition-$position+1);
            $position = $newposition+1;
        }
        elseif ($text [$position] == "'" || $text [$position] == '"') {
            $newposition = $position+1;
            while ($newposition < strlen ($text) && $text [$newposition] != $text [$position]) {
                if ($text [$newposition] == "\\")
                    $newposition++;
                $newposition++;
            }
            $codeblock .= substr ($text, $position, $newposition-$position+1);
            $position = $newposition+1;
        }
        else {
            $codeblock .= $text [$position];
            $position++;
        }

        if ($position >= strlen ($text))
            return false;
    }
    $codeblock .= "?>";
    $position += 2;

    return $codeblock;
}

?>
