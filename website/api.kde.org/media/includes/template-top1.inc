<?php // This file contains content between the top of the html body and the heading
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">

<!-- table row holding the header -->
<tr>
  <td>  
    <form action="/media/switch.php" method="post">
    <div id="nav_header_top" align="right">
      <a href="/"><img id="nav_header_logo" alt="Home" align="left" <?php print $site_logo_left ?> border="0" /></a>
    <?php if ($site_logo_right) { ?>
      <span class="doNotDisplay">::</span>
      <img id="nav_header_logo_right" alt="" align="right" <?php print $site_logo_right ?> border="0" />
    <?php } ?>
    
      <div id="nav_header_title" align="left"><?php print $site_title; ?></div>
    
    <?php if ($site_location) { ?>
      <span id="nav_header_location">
        <span class="doNotDisplay"><label for="select_location">Language: </label></span>
        <select size="1" name="location" id="select_location" title="<?php i18n("Choose one of the local KDE sites or mirror")?>">
        <option value="http://www.kde.org/" selected="selected"><?php i18n("Choose your location")?></option>
        <option value="">---------</option>
        <option value="/international/"><?php i18n("KDE in Your Country")?></option>
        <option value="">---------</option>
    <!--
    IMPORTANT: Please remember to also change www/international/index.php if you add or remove sites here!
    //-->
          <optgroup label="<?php i18n("Americas")?>">
          <option value="http://br.kde.org/"><?php i18n("Brasil")?></option>
          <option value="http://www.kde.cl/"><?php i18n("Chile")?></option>
          </optgroup>
          <optgroup label="<?php i18n("Asia")?>">
          <option value="http://www.kdecn.org/"><?php i18n("China")?></option>
          <option value="http://www.farsikde.org/"><?php i18n("Iran")?></option>
          <option value="/il/"><?php i18n("Israel")?></option>
          <option value="http://www.kde.gr.jp/"><?php i18n("Japan")?></option>
    <!--      <option value="http://www.kde.or.kr/"><?php i18n("Korea")?></option>-->
          <option value="http://kde.linux.org.tw/"><?php i18n("Taiwan")?></option>
          <option value="http://www.kde.org.tr/"><?php i18n("Turkey")?></option>
          </optgroup>
          <optgroup label="<?php i18n("Europe")?>">
          <option value="http://kde.tilix.org/"><?php i18n("Bulgaria")?></option>
          <option value="http://czechia.kde.org/"><?php i18n("Czech Republic")?></option>
          <option value="/fr/"><?php i18n("France")?></option>
          <option value="http://de.kde.org/"><?php i18n("Germany")?></option>
          <option value="http://www.kde.org.uk/"><?php i18n("Great Britain")?></option>
          <option value="http://www.is.kde.org/"><?php i18n("Iceland")?></option>
          <option value="http://www.kde.ie/"><?php i18n("Ireland")?></option>
          <option value="http://www.kde-it.org/"><?php i18n("Italy")?></option>
          <option value="http://www.kde.nl/"><?php i18n("Netherlands")?></option>
          <option value="http://www.kde.pl/"><?php i18n("Poland")?></option>
          <option value="http://www.ro.kde.org/"><?php i18n("Romania")?></option>
          <option value="http://www.kde.ru/"><?php i18n("Russia")?></option>
          <option value="http://es.kde.org/"><?php i18n("Spain")?></option>
          <option value="http://www.kde.org.tr/"><?php i18n("Turkey")?></option>
          <option value="http://www.kde.org.ua/"><?php i18n("Ukraine")?></option>
          <option value="http://www.kde.org.yu/"><?php i18n("Yugoslavia")?></option>
          </optgroup>
          <option value="">---------</option>
          <option value="/mirrors/web.php"><?php i18n("Regional KDE Mirrors")?></option>
          <option value="">---------</option>
    <!--
    IMPORTANT: Please remember to also change www/mirrors/web.php if you add or remove mirrors here!
    //-->
        <optgroup label="<?php i18n("Americas")?>">
          <option value="http://kde.pandmservices.com/"><?php i18n("United States")?> 1</option>
          <option value="http://kde.oregonstate.edu/"><?php i18n("United States")?> 2</option>
          <option value="http://kde.intissite.com/"><?php i18n("United States")?> 3</option>
          <option value="http://kde.feratech.com/"><?php i18n("United States")?> 4</option>
          <option value="http://www.unixformula.com/kde/"><?php i18n("United States")?> 5</option>
          <option value="http://kde.eng.lsu.edu/"><?php i18n("United States")?> 6</option>
          <option value="http://kde.hostingzero.com/"><?php i18n("United States")?> 7</option>
          </optgroup>
          <optgroup label="<?php i18n("Asia")?>">
    <!--      <option value="http://kde.lrc.ath.cx/"><?php i18n("Israel")?></option> -->
          <option value="http://mirrors.mybsd.org.my/kde/"><?php i18n("Malaysia")?></option>
          <option value="http://kde.thaiweb.net/"><?php i18n("Thailand")?></option>
          </optgroup>
          <optgroup label="<?php i18n("Australia &amp; Oceans")?>">
          <option value="http://kde.planetmirror.com/"><?php i18n("Australia")?></option>
          </optgroup>
          <optgroup label="<?php i18n("Europe")?>">
          <option value="http://www.at.kde.org/"><?php i18n("Austria")?></option>
          <option value="http://kde.paralax.org/"><?php i18n("Bulgaria")?></option>
          <option value="http://kde.linux-mirror.org/"><?php i18n("Germany")?></option>
          <option value="http://kde.ace-hellas.gr/"><?php i18n("Greece")?></option>
          <option value="http://www.lt.kde.org/"><?php i18n("Lithuania")?></option>
          <option value="http://kde.mirror.transip.nl/"><?php i18n("Netherlands")?></option>
          <option value="http://www.kde.ps.pl/"><?php i18n("Poland")?></option>
          <option value="http://kde.nux.ipb.pt/"><?php i18n("Portugal")?></option>
          <option value="http://kde.rinet.ru/"><?php i18n("Russia")?> 1</option>
          <option value="http://kde-mirror.offshoredrive.com/"><?php i18n("Russia")?> 2</option>
          <option value="http://www.kde.si/"><?php i18n("Slovenia")?></option>
          <option value="http://www.se.kde.org/"><?php i18n("Sweden")?></option>
    <!--      <option value="http://kde.fredan.org/"><?php i18n("Sweden")?> 2</option> -->
          <option value="http://kde.mirror.fr/"><?php i18n("Switzerland")?> 1</option>
          <option value="http://kde.prokmu.com/"><?php i18n("Switzerland")?> 2</option>
          </optgroup>
        </select>
    
        <input type="submit" value="<?php i18n("Go")?>" />
      </span>
    <?php } ?>
    
    </div>
    
    <div id="nav_header_bottom" align="right">
      <span id="nav_header_bottom_right">
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/family/" accesskey="3" title="<?php i18n("A complete structural overview of the KDE.org web pages")?>"><?php i18n("Sitemap")?></a> ::
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/documentation/" accesskey="6" title="<?php i18n("Having problems? Read the documentation")?>"><?php i18n("Help")?></a> ::
          <a href="<?php if ($site_external) print "http://www.kde.org" ?>/contact/" title="<?php i18n("Contact information for all areas of KDE")?>"><?php i18n("Contact Us")?></a>
      </span>
      <span id="nav_header_bottom_left">
        <?php echo i18n ('Location').': '; $kde_menu->showLocation(); ?>
      </span>
    </div>
    </form>
  </td>
</tr>

<!-- table row holding the menu + main content -->
<tr>
  <td>
    <table id="main" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <?php if ($site_menus > 0) { ?>
        <td valign="top" class="menuheader" height="0"></td>
      <?php } ?>
    
      <td id="contentcolumn" valign="top" <?php if ($site_menus > 0) print 'rowspan="2"'; ?> >
        <div id="content"><div style="width:100%;">
