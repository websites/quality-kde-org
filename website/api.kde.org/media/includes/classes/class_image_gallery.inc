<?php

/**
 * Written by Chris Howells <howells@kde.org>
 * Based on code by Christoph Cullmann <cullmann@kde.org>
 * Based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 */

class ImageGallery
{
	var $items = array();
	var $item;
	var $newrow;
	var $summary;
	var $section;
	var $tr_open; /* This is needed to prevent empty <tr></tr> tags which cause validation errors */

	function ImageGallery ($summary)
	{
		$this->summary = $summary;
	}

	function addImage($src_url, $dest_url, $width_pixels, $height_pixels, $alt_text, $caption_text = "", $description_text = "")
	{
		$item = new Item($src_url, $dest_url, $width_pixels, $height_pixels, $alt_text, $caption_text, $description_text);
		array_push($this->items, $item);
	}

	function startNewRow()
	{
		$newrow = new NewRow;
		array_push($this->items, $newrow);
	}

	function addSection($section_name)
	{
		$section = new GalSection($section_name);
		array_push($this->items, $section);
	}

	function show()
	{
		if (count($this->items))
		{
			print "<table summary=\"$this->summary\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">\n";
			$tr_open = false;

			for ($i=0; $i < count($this->items); $i++)
			{
				$this->items[$i]->show($tr_open);
			}

			if ($tr_open)
				print "</tr>\n";
			print "</table>\n";
		}
	}
}

class NewRow
{
	function NewRow()
	{
	}

	function show(&$tr_open)
	{
		if ($tr_open)
			print "</tr>\n";
		$tr_open = false;
	}
}

class GalSection
{
	var $section;

	function GalSection($section)
	{
		$this->section = $section;
	}

	function show(&$tr_open)
	{
		if ($tr_open)
			print "</tr>\n";
		print "<tr><td>\n";
		print "<h3>$this->section</h3>\n";
		print "</td></tr>\n";
		$tr_open = false;
	}
}

class Item
{
	var $image;
	var $src_url;
	var $dest_url;
	var $width_pixels;
	var $height_pixels;
	var $alt_text;
	var $caption_text;
	var $description_text;

	function Item($src_url, $dest_url, $width_pixels, $height_pixels, $alt_text, $caption_text, $description_text)
	{
		$this->src_url = $src_url;
		$this->dest_url = $dest_url;
		$this->width_pixels = $width_pixels;
		$this->height_pixels = $height_pixels;
		$this->alt_text = $alt_text;
		$this->caption_text = $caption_text;
		$this->description_text = $description_text;
	}

	function show(&$tr_open)
	{
		if (!$tr_open)
			print "<tr>\n";
		$image = "<td valign=\"middle\" style=\"text-align:center\">\n";

		if ($this->dest_url != "")
		{
			if ($this->caption_text != "")
			{
				$image = $image . "<b>$this->caption_text</b>\n";
				$image = $image . "<br />\n";
			}
			if ($this->src_url != "")
			{
				$image = $image . "<a href=\"$this->dest_url\"><img src=\"$this->src_url\" alt=\"$this->alt_text\"  width=\"$this->width_pixels\" height=\"$this->height_pixels\" border=\"0\" /></a>\n";
				$image = $image . "<br />\n";
			}
			if ($this->description_text != "")
			{
				$image = $image . "$this->description_text\n";
				$image = $image . "<br />\n";
			}
		}
		else
		{
			if ($this->caption_text != "")
			{
				$image = $image . "<b>$this->caption_text</b>\n";
				$image = $image . "<br />\n";
			}
			if ($this->src_url != "")
			{
				$image = $image . "<img src=\"$this->src_url\" alt=\"$this->alt_text\" width=\"$this->width_pixels\" height=\"$this->height_pixels\" />\n";
				$image = $image . "<br />\n";
			}
			if ($this->description_text != "")
			{
				$image = $image . "$this->description_text\n";
				$image = $image . "<br />\n";
			}
		}

		$image = $image . "&nbsp;\n";
		$image = $image . "</td>\n";
		print $image;
		$tr_open = true;
	}
}

