<?php

class SitemapItem {
	var $depth,$flags,$link,$page,$flink,$isopen,$parent;

	function SitemapItem($dp,$fl,$li,$pg)
	{
		global $site_root;
		$this->depth=$dp;
		$this->flags=$fl;
		$this->link=$li;
		$this->page=$pg;
		$this->isopen=0;
		$this->parent="";
		if ($li{0} == ":") {
			$this->flink = "http://" . ltrim($li,":");
		} else {
			if (($li{strlen($li)-1} != "/") and (!strpos($li,"#")))
				$this->flink = $site_root . "/" . $li . ".php";
			else
				$this->flink = $site_root . $li;
		}

	}
}

class SitemapMenu {
	var $heretag;
	var $maplines;
	var $loaded;
	var $sitemap;
	var $parents;

	function SitemapMenu($ht = "",$file = "" )
	{
		global $site_root;
		$this->maplines = array();
		$this->loaded = false;
		$this->parents = new URLName;
		if ($ht) {
			$this->heretag = explode(";",$ht);
		} else {
			$this->heretag = array();
		}

		if (!empty($file))
			$this->sitemap = $file;
		else
			$this->sitemap = $site_root . "site.content";
	}

	function push($link)
	{
		array_push($this->heretag,$link);
	}

	function load_file()
	{
		global $site_root;
		if ($this->loaded) return;
		$this->loaded = true;
		$content_entry = $_SERVER['REQUEST_URI'];
		$content_entry = str_replace("index.php","",$content_entry);
		$content_entry = str_replace(".php","",$content_entry);
		if ($content_entry[0]=="/")
			$content_entry = substr($content_entry,1);
		# echo "<!-- Looking for $content_entry -->\n";
		$content_parents = array();

		$map = fopen($this->sitemap, "r");
		if (!$map) {
			echo "<!-- " . $this->sitemap . " not found -->\n";
			return;
		}
		$lineno = 0;
		$this_page = 0;
		$this_parents = "";
		while (!feof($map)) {
			$line = fgets($map);
			if (preg_match( "/^<\s*(-?)\s*([\w\/:~\.-][\w\/:~\.#-]*)\s+(.*)$/", $line, $matches)) {
				if ($matches[2] == '/') $matches[2]='';
				$this->parents->push($site_root . $matches[2],$matches[3]);
				continue;
			}
			if (!preg_match( "/^\s*(>*)\s*(-?)\s*([\w\/:~\.-][\w\/:~\.#-]*)\s+(.*)$/", $line, $matches))
				continue;
			 $item = new SitemapItem(strlen($matches[1]),$matches[2],$matches[3],$matches[4]);
			$content_parents[$item->depth] = $item->link;
			$parents = array_slice($content_parents,0,$item->depth + 1);
			$item->parent = join(";",$parents);
			if ($item->link == $content_entry) {
				$this->heretag = $parents;
				$this_page = $lineno;
				$this_parents = $item->parent;
			}
			$this->maplines[$lineno] = $item;
			$lineno++;
		}
		fclose($map);

		# echo "<!-- Loaded $lineno entries -->\n";
		# echo "<!-- Main entry is $this_parents -->\n";
		for ($i=0; $i<count($this->maplines); $i++)
		{
			$entry = $this->maplines[$i]->parent;
			if ($this->maplines[$i]->depth == $this->maplines[$this_page]->depth) {
				# At the smae depth, need to compare parents,
				# not the complete reference line.
				$lastpos = strrpos($entry,";");
				if ($lastpos) {
					$entry = substr($entry,0,$lastpos-1);
				}
			}
			if  (strpos($this_parents,$entry) === 0) {
				$name = $this->maplines[$i]->page;
				# echo "<!-- $name should be open -->\n";
			}
		}
	}

	function show()
	{
		global $site_root;
		$this->load_file();

		$depth = -1;
		$openness = array();

		for ($i = 0 ; $i<count($this->maplines); $i++) {
			# This is an actual sitemap line.
			$newdepth = $this->maplines[$i]->depth;
			$flags = $this->maplines[$i]->flags;
			$link = $this->maplines[$i]->link;
			$page = $this->maplines[$i]->page;

			# Check that depth increases gradually
			if ($newdepth > $depth+1) {
				echo "<!-- Bad depth $newdepth with title $page -->\n";
				return;
			}

			# Handle top-level entries specially.
			if ($newdepth == 0) {
				while ($depth > 0) {
					if ($openness[$depth-1])
						echo "</ul>";
					$depth--;
				}
				if ($depth>=0) {
					echo "</div>";
				}
				echo "<h2>$page</h2>\n";
				echo "<div class=\"nav_list\">\n";
				$depth = $newdepth;
				$openness = array(true);
				continue;
			}

			$display = 0;
			# Enter a new level (newdepth = depth+1)
			if ($newdepth > $depth) {
				if ($openness[$depth]) {
					echo "<ul>";
					if ($newdepth < count($this->heretag))
						$otag = $this->heretag[$newdepth];
					else
						$otag = "";
					# echo "<!-- Opened '$link' ; Looking for '$otag' -->\n";
					array_push($openness,$link == $otag);
					# if ($openness[$newdepth]) {
					# 	echo "<!-- Entered printing depth $newdepth page $page -->\n";
					# } else {
					# 	echo "<!-- Entered non-printing depth $newdepth page $page -->\n";
					# }
					$display = 1;
				} else {
					# echo "<!-- Entered suppressed depth $newdepth page $page -->\n";
					array_push($openness,0);
					$display = 0;
				}
				$depth = $newdepth;
			} else if ($newdepth == $depth) {
				#echo "<!-- Got item depth $depth page $page -->\n";
				$display = $openness[$depth-1];
				if ($newdepth < count($this->heretag))
					$openness[$depth] = ( $link == $this->heretag[$newdepth] );
				else
					$openness[$depth] = false;

				#if ($openness[$depth]) {
				#	echo "<!-- Now printing at depth $depth -->\n";
				#}
			} else { # $newdepth < $depth
				while ($newdepth < $depth) {
					if ($openness[$depth-1])
						echo "</ul>\n";
					$depth--;
				}
				$display = $openness[$depth-1];
				$openness[$depth] = ( $link == $this->heretag[$newdepth] );
				#if ($openness[$depth]) {
				#	echo "<!-- Dropped to printing depth $depth page $page -->\n";
				#} else {
				#	echo "<!-- Dropped to non-printing depth $depth page $page -->\n";
				#}
			}


			#$odepth = count($openness);
			#echo "<!-- $depth $newdepth $odepth $page -->\n";

			if ($depth == 1)
				$display = 1;

			if ($display) {
				$display = 1;
				if ($link == $this->heretag[count($this->heretag)-1]) {
					$display = 2;
				}
				$link = $this->maplines[$i]->flink;
				if ($display == 2) {
					echo "<li class=\"here\"><a href=\"$link\">$page</a></li>\n";
				} else if ($flags == "-") {
					echo "<li>$page</li>\n";
				} else {

					echo "<li><a href=\"$link\">$page</a></li>\n";
				}
			}

		}

		# Close out remaining subtrees.
		while ($depth > 0) {
			echo "</ul>";
			$depth--;
		}
		if ($depth>=0) {
			echo "</div></div>";
		}
	}

	function currentMenu() {
		$this->show();
	}

	function activeSection() {
	}

	function showlocation()
	{
		$this->load_file();
		$depth=0;
		print 'Location: ';
		$this->parents->show("/ ");
		for ($i=0; ($i<count($this->maplines)) && ($depth<count($this->heretag)); $i++) {
			if ($this->maplines[$i]->link == $this->heretag[$depth]) {
				$page = $this->maplines[$i]->page;
				$link = $this->maplines[$i]->flink;
				if ($this->maplines[$i]->flags == "-")
					echo "/ $page ";
				else
					echo "/ <a href=\"$link\">$page</a> ";
				$depth++;
			}
		}
	}
}

class StylesheetGroup
{
	var $items = array();
	var $name;
	function StylesheetGroup($name,$group)
	{
		$this->name=$name;
		$this->items=explode(";",$group);
	}
	function show($alt)
	{
		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			for ($i=0; $i < $numitems; $i++)
			{
				echo '<link rel="';
				if ($alt) echo "alternate stylesheet";
				else echo "stylesheet";
				echo '" media="screen" type="text/css" title="';
				echo $this->name;
				echo '" href="';
				echo $this->items[$i];
				echo "\" />\n";
			}
		}
	}
}

class StylesheetList
{
	var $items= array();
	function push($name,$group)
	{
		array_push($this->items,new StylesheetGroup($name,$group));
	}
	function show()
	{
		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			for ($i=0; $i < $numitems; $i++)
			{
				$this->items[$i]->show($i>0);
			}
		}
	}
}


class RDFList
{
	var $items = array();
	function push($source,$num)
	{
		$item = new RDFItem($source, $num);
		array_push($this->items, $item);
	}
	function count()
	{
		return count($this->items);
	}
	function load()
	{
		if (count($this->items))
		{
			for ($i=0; $i<count($this->items); $i++)
			{
				$item = $this->items[$i];
				echo "<!-- $i -->\n";
				$item->load();
			}
		}
	}
}

class RDFItem
{
	var $source;
	var $num;
	function RDFItem($source,$num)
	{
		$this->source = $source;
		$this->num = $num;
	}
	function load()
	{
		echo "<div class=\"newsshort\">\n";
		kde_general_news($this->source,$this->num,1);
		echo "</div>\n";
	}
}

class URLName
{
	var $items = array();

	function URLName()
	{
	}

	function push($url, $name)
	{
		$item = new Pair($url, $name);
		array_push($this->items, $item);
	}

	function count()
	{
		return count($this->items);
	}
	function show($sep)
	{
		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			for ($i=0; $i < $numitems; $i++)
			{
				$item =& $this->items[$i];
				print '<a href="' . $item->one . '" >' . $item->two . "</a>\n";
				if ($i < $numitems - 1)
					echo " $sep ";
			}
		}
	}
	function showList()
	{
		if (count($this->items))
		{
			$numitems = count($this->items);
			for ($i=0; $i<$numitems; $i++)
			{
				echo "<li>";
				$item = $this->items[$i];
				echo "<a href=\"$item->one\">$item->two</a>\n";
				echo "</li>\n";
			}
		}
	}
}

class Pair
{
	var $one;
	var $two;

	function Pair($url, $name)
	{
		$this->one = $url;
		$this->two = $name;
	}

}

?>
