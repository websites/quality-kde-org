<?php

/**
 * Menu code for www.kde.org, this is the www/media version
 * Written by Christoph Cullmann <cullmann@kde.org>
 * Altered by Olaf Schmidt <ojschmidt@kde.org> 
 * Based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 */

class BaseMenu
{
  var $menu_root;
  var $menu_baseurl;
  var $items = array();
  var $current_relativeurl;
  var $name;

  function BaseMenu ($menu_root, $menu_baseurl, $current_relativeurl)
  {
    global $name;

    if (! (substr ($menu_baseurl, strlen($menu_baseurl)-1, 1) == "/"))
      $menu_root = $menu_root."/";

    if (! (substr ($menu_baseurl, strlen($menu_baseurl)-1, 1) == "/"))
      $menu_baseurl = $menu_baseurl."/";

    if (! (substr ($menu_root, strlen($menu_root)-1, 1) == "/"))
      $menu_root = $menu_root."/";

    $current_relativeurl = str_replace ("index.php", "", $current_relativeurl);
    $current_relativeurl = str_replace ("//", "/", $current_relativeurl);

    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;
    $this->name = "";

    if (file_exists ($this->menu_root."menu.inc") && is_readable ($this->menu_root."menu.inc"))
      include ($this->menu_root."menu.inc");
  }

  function setName ($name)
  {
    $this->name = $name;
  }

  function &appendSection ($name)
  {
    $section = new MenuSection ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl, $name);
    $this->items[] =& $section;

    return $section;
  }

  function show ()
  {
    print '<a name="cp-menu" />';

    if(count($this->items))
    {
      for($i=0; $i < count($this->items); $i++)
      {
       $this->items[$i]->show ();
      }
    }
  }

  /**
   * Return the current active section of the menu
   * default to the first section in the menu if no other marked active
   * this is the case for the index.php for example !
   */
  function activeSection ()
  {
    for($i=0; $i < count($this->items); $i++)
    {
      if ($this->items[$i]->active)
        return $i;
    }

    return 0;
  }

  function activeSectionName ()
  {
    return $this->items[$this->activeSection()]->name;
  }

  function showLocation ()
  {
    if ($this->name)
      print '<a href="'.($this->menu_baseurl).'" accesskey="1">'.$this->name.'</a>';

    if(count($this->items))
    {
      for($i=0; $i < count($this->items); $i++)
      {
       $this->items[$i]->showLocation ();
      }
    }
  }
}

class MenuSection
{
  var $menu_root;
  var $menu_baseurl;
  var $name;
  var $id;
  var $items = array();
  var $current_relativeurl;

  function MenuSection ($menu_root, $menu_baseurl, $current_relativeurl, $name, $id = "")
  {
    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;
    $this->name = $name;
    if ($id == "")
      $this->id = strtolower(preg_replace("([^0-9a-zA-Z])", "", $name));
    else
      $this->id = $id;
  }

  function appendLink ($name, $link, $relative_link = true, $is_dir = false)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $link, $relative_link, false);
    array_push($this->items,$item);

    return $item;
  }

  function appendDir ($name, $dir)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $dir, true, true);
    array_push($this->items,$item);

    return $item;
  }

  function show ()
  { global $template_menulist1, $template_menulist2, $template_menulist3;

    print $template_menulist1."\n";
    print '<h2 id="cp-menu-'.$this->id.'">'.$this->name."</h2>\n";
    print '<a href="#cp-skip-'.$this->id.'" class="cp-doNotDisplay">'.str_replace ('%1', $this->name, i18n_var ('Skip menu "%1"'))."</a>\n";
    print $template_menulist2."\n";

    print "<ul>\n";

    for($i=0; $i < count($this->items); $i++)
      $this->items[$i]->show ();

    print "</ul>\n";
    print $template_menulist3."\n";
    print '<a name="cp-skip-'.$this->id."\"/>\n";
  }

  function showLocation ()
  {
    for($i=0; $i < count($this->items); $i++)
    {
      $this->items[$i]->showLocation ();
    }
  }
}

class Menu {
  var $menu_root;
  var $menu_baseurl;
  var $items;
  var $name;
  var $link;
  var $relative_link;
  var $current_relativeurl;
  var $active;
  var $item_active = false;

  function Menu($menu_root, $menu_baseurl, $current_relativeurl, $name, $link, $relative_link, $is_dir)
  {
    if ($is_dir && (substr ($link, -1) != "/"))
      $link = $link."/";
    $link = str_replace ("index.php", "", $link);

    $this->name = $name;
    $this->link = $link;
    $this->relative_link = $relative_link;
    $this->items = array();
    $this->active = strcmp ($link, $current_relativeurl) == 0;
    $this->menu_root = $menu_root;
    $this->menu_baseurl = $menu_baseurl;
    $this->current_relativeurl = $current_relativeurl;

    if ($is_dir)
    {
      $this->menu_root .= $link;
      $this->menu_baseurl .= $link;
      $this->link = "";

      if (strncmp ($link, $current_relativeurl, strlen ($link)) == 0)
      {
        $this->current_relativeurl = substr ($current_relativeurl, strlen ($link));
        $this->item_active = true;

        if (file_exists ($this->menu_root."menu.inc") && is_readable ($this->menu_root."menu.inc"))
          include ($this->menu_root."menu.inc");
      }
      else
        $this->current_relativeurl = "";
    }
  }

  function itemActive()
  {
    return $this->active || $this->item_active;
  } 
   
  function appendLink ($name, $link, $relative_link = true)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $link, $relative_link, false);
    array_push($this->items,$item);

    if ($item->itemActive())
      $this->item_active = true;

    return $item;
  }

  function appendDir ($name, $dir)
  {
    $item = new Menu ($this->menu_root, $this->menu_baseurl, $this->current_relativeurl,
                      $name, $dir, true, true);
    array_push($this->items,$item);

    if ($item->itemActive())
      $this->item_active = true;

    return $item;
  }

  function show ()
  { global $template_menuitem1, $template_menuitem2;
    global $template_menusublist1, $template_menusublist2, $template_menusublist3;

    if ($this->active) 
      print "<li class=\"here\">";
    else 
      print "<li>";
 
    if ($this->itemActive() && (count($this->items) > 0))
      print $template_menusublist1."\n";
    else
      print $template_menuitem1."\n";

    if (! empty ($this->link) || ! empty ($this->menu_baseurl)) {
        print '<a href="';
        if ($this->relative_link)
            print $this->menu_baseurl;
        print $this->link.'">'.$this->name."</a>";
    }
    else
        print $this->name;

    if ($this->itemActive() && (count($this->items) > 0))
    {
      print $template_menusublist2."\n";
      print "<ul>\n";

      for($i=0; $i < count($this->items); $i++)
        $this->items[$i]->show ();

      print "</ul>\n";
      print $template_menusublist3."\n";
    }
    else
      print $template_menuitem2."\n";

    print "</li>\n";
  }

  function showLocation ()
  {
    if ($this->active || $this->item_active)
    {
      if ($this->menu_baseurl.$this->link != "/")
        if ($this->active)
          print ' / '.$this->name;
        else
          print ' / <a href="'.($this->menu_baseurl.$this->link).'">'.$this->name.'</a>';

      if (count($this->items) > 0)
      {
        for($i=0; $i < count($this->items); $i++)
        {
          $this->items[$i]->showLocation ();
        }
      }
    }
  }
}

?>
