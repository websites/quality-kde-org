<?php // This file contains content between the menu and the end of html body

  // figure out the contact
  if (isset($name) && isset($mail))
    $contact = i18n_var("Maintained by") . " <a href=\"mailto:$mail\">$name</a><br />\n";
  else
    $contact = i18n_var("Report problems with this website to <a href=\"https://go.kde.org/systickets\">our bug tracking system</a>.<br>");
?>

<?php
  // Handle the search box, if wanted.
  if (!isset($site_search) || ($site_search === true)) {
?>
             <div class="menutitle"><div>
               <h2 id="cp-menu-search"><?php i18n("Search:")?></h2>
             </div></div>

             <div></div>
             <form method="get" name="Searchform" action="/media/search.php"><p style="text-align:center">
               <span class="cp-doNotDisplay"><label for="Search" accesskey="4"><?php i18n("Search:")?></label></span>
               <input style="width:80%" type="text" size="10" name="q" id="Search" value="" /><br />
               <input style="width:80%" type="submit" value=" <?php i18n("Search")?> " name="Button" id="searchButton" />
             </p></form>
<?php
  // Handle the search box, if wanted.
  } else if ($site_search == "custom") {
    include_once("$site_root/includes/search.inc");
  }
?>

          </div>
        </div>
        <div class="clearer"></div>
      </div>
      <div class="clearer"></div>
    </div>
    <div id="end_body"></div>

    <div id="footer"><div id="footer_text">
        <?php print $contact; ?>
        KDE<sup>&#174;</sup> and <a href="/media/images/kde_gear_black.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
        <a href="https://kde.org/community/whatiskde/impressum">Legal</a>
    </div></div>
  </div>

<!--
WARNING: DO NOT SEND MAIL TO THE FOLLOWING EMAIL ADDRESS! YOU WILL
BE BLOCKED INSTANTLY AND PERMANENTLY!
<?php
  $trapmail = "aaaatrap-";
  $t = pack('N', time());
  for($i=0;$i<=3;$i++) {
      $trapmail.=sprintf("%02x",ord(substr($t,$i,1)));
  }
  $ip=$_SERVER["REMOTE_ADDR"];
  sscanf($ip,"%d.%d.%d.%d",$ip1,$ip2,$ip3,$ip4);
  $trapmail.=sprintf("%02x%02x%02x%02x",$ip1,$ip2,$ip3,$ip4);

  echo "<a href=\"mailto:$trapmail@kde.org\">Block me</a>\n";
?>
WARNING END
-->

<?php
  if (isset ($_GET ['javascript'])) {
?>

<script type="text/javascript">
function fitFooter() {
  try {
    document.getElementById ('body').style.minHeight = (window.innerHeight - document.getElementsByTagName ('body')[0].offsetHeight + document.getElementById ('body').offsetHeight) + "px";
    window.setTimeout ("fitFooter()", 200);
  } catch (e) {
  }
};
window.setTimeout ("fitFooter()", 100);
</script>

<?php
}
?>
