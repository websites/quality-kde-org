<?php

/**
 * Prefer to switch the location of the page if we have 'location' set
 */
if (isset($_REQUEST['location']))
{
  if ($_REQUEST['location'] != "")
    header ("Location: ".$_REQUEST['location']);
  else if (isset ($_SERVER['HTTP_REFERER']))
    header ("Location: ".$_SERVER['HTTP_REFERER']);
  else
    header ("Location: http://www.kde.org");

  exit ();
}

if (isset ($_POST['site_style']))
  setcookie ('sitestyle', $_POST['site_style'], time()+31536000, '/', '.kde.org');
else if (isset ($_GET['site_style']))
  setcookie ('sitestyle', $_GET['site_style'], time()+31536000, '/', '.kde.org');

if (isset ($_SERVER['HTTP_REFERER']))
  header ("Location: ".$_SERVER['HTTP_REFERER']);
else
  header ("Location: http://www.kde.org/media/settings.php");

?>

