#!/usr/bin/env python

# Basic script to convert pot files to php4 style

import sys


# Conversion table from ISO accents to HTML typos
IsoHtmlDict = { "�":"&Agrave;","�":"&Aacute;","�":"&Acirc;",
	"�":"&Atilde;","�":"&Auml;","�":"&Aring;",
	"�":"&AElig;","�":"&Ccedil;","�":"&Egrave;",
	"�":"&Eacute;","�":"&Ecirc;","�":"&Euml;",
	"�":"&Igrave;","�":"&Iacute;","�":"&Icirc;",
	"�":"&Iuml;","�":"&ETH;","�":"&Ntilde;",
	"�":"&Ograve;","�":"&Oacute;","�":"&Ocirc;",
	"�":"&Otilde;","�":"&Ouml;","�":"&Oslash;",
	"�":"&Ugrave;","�":"&Uacute;","�":"&Ucirc;",
	"�":"&Uuml;","�":"&Yacute;","�":"&THORN;",
	"�":"&szlig;","�":"&agrave;","�":"&aacute;",
	"�":"&acirc;","�":"&atilde;","�":"&auml;",
	"�":"&aring;","�":"&aelig;","�":"&ccedil;",
	"�":"&egrave;","�":"&eacute;","�":"&ecirc;",
	"�":"&euml;","�":"&igrave;","�":"&iacute;",
	"�":"&icirc;","�":"&iuml;","�":"&eth;",
	"�":"&ntilde;","�":"&ograve;","�":"&oacute;",
	"�":"&ocirc;","�":"&otilde;","�":"&ouml;",
	"�":"&oslash;","�":"&ugrave;","�":"&uacute;",
	"�":"&ucirc;","�":"&uuml;","�":"&yacute;",
	"�":"&thorn;","�":"&yuml;" }

# Funtion to convert accents to html ( using conv table )
def iso2html( word ):
	val = ""
	for letter in word:
		if IsoHtmlDict.has_key( letter ):
			val += IsoHtmlDict[ letter ]
		else:
			val += letter
	
	return val


# Main function
def main():
	potfile="media.pot"
	v = len( sys.argv )
	if v == 3:
		potfile = sys.argv[1]
		datafile = sys.argv[2]
	# read media file
	file = open( potfile );
	print datafile
	data = open( datafile, "w" )
	data.write( "<?php\n" )
	while 1:
		line = file.readline()
		if not line:
			break
		if line.find( "msgid" ) != -1:
			text = '\t$text[' + line.replace( 'msgid ', '' ).replace( '\n', '' ) + ']='
			line = file.readline()
			parse = iso2html( line.replace( 'msgstr ', '' ).replace( '\n', '' ) )
			text += parse + ';\n'
			data.write( text )
	data.write( "?>" )
	file.close()
	data.close()


if __name__ == "__main__":
	main()

