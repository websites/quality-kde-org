#!/bin/bash

# Written by Chris Howells <howells@kde.org>
# License under the GNU GPL

# The place to put the .pot files so that translators can pick them up
OUTPUTPOTDIR=$2
# The location of the web site so that the scrip can run over it looking for mesages
WEBSITEDIR=$1
# Directories under $WEBSITEDIR that we want the script to ignore and not bother looking for messages in
EXCLUDEDIRS="apps areas dot es extragear fr il img international kdegames kdeslides koffice calligra"
# Arguments to be use for xgettext
XGETTEXTARGS="-ki18n -ki18n_var --no-location"
# List of web sites to look at
SITES="
.
areas/women
"

cd $WEBSITEDIR
svn up

for subdirectory in $SITES
do
	echo "switching to $WEBSITEDIR/$subdirectory"
	cd "$WEBSITEDIR/$subdirectory"

	if test ! $subdirectory = "."; then
		#echo "$subdirectory is not top level"
		subsubdirectory=`echo "$subdirectory" | sed s/"\/"/""/g` # remove slashes
		#echo "subsubdirectory is $subsubdirectory"
		finaloutputdir="$OUTPUTPOTDIR/$subsubdirectory"
		if test ! -d "$finaloutputdir"; then
			#echo "making the dir $finaloutputdir"
			mkdir -p "$finaloutputdir"
			echo "adding the subdirectory to cvs"
			pushd $OUTPUTPOTDIR
			svn add "$subsubdirectory"
			popd
		fi
	else
		#echo "is top level"
		finaloutputdir=$OUTPUTPOTDIR
	fi

	for item in *
	do
		if test -d "$item"; then # if it's a directory
			#echo "looking in directory $item"
			WANTED=wanted
			for excludedir in $EXCLUDEDIRS
			do
				if test $excludedir = $item; then
					WANTED=notwanted
				fi
			done
			echo "looking $WEBSITEDIR/$subdirectory/$item"	
			if test $WANTED = wanted; then
				LIST=`find $WEBSITEDIR/$subdirectory/$item -name \*.php -o -name \*.inc`

				POTFILE=$finaloutputdir/$item.pot
				POTFILETEMP=$POTFILE.temp
				if test ! -f $POTFILE; then
					touch $POTFILE
				        /usr/bin/xgettext $XGETTEXTARGS $LIST -o $POTFILETEMP &>/dev/null
					if test ! -s $POTFILETEMP; then
						rm -f $POTFILE $POTFILETEMP
						continue
					fi
					#echo "touch'ing and cvs add'ing $POTFILE"
					echo "changing to $finaloutputdir and adding $item.pot"
					pushd $finaloutputdir
					echo "changing directory"
					pwd
					svn add $item.pot
					popd
				else # check whether the date is the only thing to have changed
					touch $POTFILETEMP
					echo "looking for strings"
					/usr/bin/xgettext $XGETTEXTARGS $LIST -o $POTFILETEMP &>/dev/null
					if test -s $POTFILETEMP && test ! -z "`diff -I POT-Creation-Date $POTFILE $POTFILETEMP`"; then
						echo "moving temp to final"
						mv $POTFILETEMP $POTFILE
					else
						echo "deleting temp pot file"
						rm -f $POTFILETEMP
					fi
				fi
			fi
		fi
	done
done

cd $OUTPUTPOTDIR && svn commit -m "CVS_SILENT made messages"

# Now do the actual job of making arrays from the translated .po files

#for subdir in `cat $OUTPUTPOTDIR/../../subdirs`
#do
#	# first of all create the required language output directories
#	if test -d $OUTPUTPOTDIR/../../$subdir/www.kde.org; then
#		if test ! -d $WEBSITEDIR/media/includes/i18n/$subdir; then
#			pushd $WEBSITEDIR/media/includes/i18n
#			mkdir $subdir
#			cvs add $subdir
#			popd
#		fi
#
#		# then create the subdirs under each language
#		for location in $SITES
#		do
#			if test ! $location = "."; then	
#				if test ! -d $WEBSITEDIR/media/includes/i18n/$subdir/$location; then
#					pushd $WEBSITEDIR/media/includes/i18n/$subdir
#					mkdir -p $location
#					cvs add $location
#					popd
#				fi
#			fi
#		done
#
#		# now create the actual php array from the pot file
#		for location in $SITES
#		do
#			# go to the directory containing the translated po files
#			cd $OUTPUTPOTDIR/../../$subdir/www.kde.org/$location
#			for item in *
#			do
#				if test -f $item; then
#				/usr/bin/env python conv_media.py $item $WEBSITEDIR/media/includes/i18n/
#				#cvs add 
#				fi
#			done
#		done
#	fi
#done


