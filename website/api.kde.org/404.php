<?php

include("classes/class_handler404.inc");

$handler = new Handler404();

$handler->add("/tutorial", "https://community.kde.org/Guidelines_and_HOWTOs/API_Documentation");
$handler->add("/tutorial/index.php", "https://community.kde.org/Guidelines_and_HOWTOs/API_Documentation");
$handler->add("/tutorial/guidelines.php", "https://community.kde.org/Guidelines_and_HOWTOs/API_Documentation");

// do it ;)
$handler->execute();

?>
