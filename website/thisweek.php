<?php
$title='This week in KDE';
$subtitle='... in pictures';

include 'crumbs.inc';
crumb('Home','index.php');
crumb('This Week');

include 'header.inc';
?>

<table width="100%" cellpadding="8">

<tr>
<td align="center">
  <img src="/img/commits-weekdaily.png" align="center" alt="Commits per day"/>
</td>
<td align="center">
  <img src="/img/commits-hourly.png" align="center" alt="Commits per hour"/>
</td>
</tr>

<tr>
<td>
  Commits over the past seven days across all of KDE,
  per day (measured at UTC).
</td>
<td>
  Commits over the past seven days across all of KDE,
  per hour daily (UTC).
</td>
</tr>



<tr>
<td align="center">
  <img src="/img/commit-who.png" align="center" alt="Top 20 committers"/>
</td>
<td align="center">
  <img src="/img/commit-component.png" align="center" alt="Top 20 components"/>
</td>
</tr>
<tr>
<td>
  Top 20 commiters measured by the number of commits;
  number of files is also indicated.
</td>
<td>
  Top 20 components measured by the number of files
  modified this week in all commits.
</td>
</tr>

</table>

<?php include 'footer.inc'; ?>