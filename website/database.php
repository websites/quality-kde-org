<?php
include 'crumbs.inc' ;
crumb('About','about.php');
crumb('Administrative');

include 'header.inc';
include 'db.inc';
?>

<p>This is the administrative page for the EBN. It describes
things like the tools used, database schemas, etc.
</p>

<h1>Database Schemas</h1>

<ul>
<li>
components - a table mapping component names to ids.
Columns id (integer), a name and a verbose name (which is suitable for being
shown on the web page). A component is a
source package larger than a module. For instance, KDE 3.5 is
a component (containing modules like kdelibs, kdebase).
We couldn't think of a better name.
<table class="boxed" width="100%">
<tr><th>id</th><th>Name</th><th>Verbose name</th></tr>
<?php
generic_query("
	SELECT id, name, verbose_name
	FROM components",
	"<tr><td>%id</td><td>%name</td><td>%verbose_name</td></tr>");
?>
</table>
</li>
<li>
tools - a table containing information on all the different
tools that run on the source. For our purposes, a tool
is bound to a specific component, so the tool 'apidox checking
for KDE 3.5' is a different tool from 'apidox checking for KDE 4.0'.
This is reflected in the name and component columns of the table.
<p>
Columns are
id (integer),
name (character, giving the tool a very short tag. It's a good
idea to name all the tools that perform a specific task like apidox
checking the same; they will be distinguished by component number),
component (integer, identifying a component from the components table),
description (80 characters, a short description of the tool),
desc_long (a longer description of the tool, unused as yet),
path (character, a URL relative to ebn.kde.org
where the results of the tool are stored),
and generation (integer).
</p>

<p>Generation is the only field that is updated in this table.
It identifies which generation was run <i>most recently</i>
for this particular tool. As such,
this field is redundant because it can be calculated from the
generations table below.
Tools should update the generation column when a run completes
successfully.
</p>

<table class="boxed" width="100%">
<tr><th>id</th><th>Name</th><th>Component</th><th>Description</th><th>Gen.</th></tr>
<?php
generic_query("
	SELECT tools.id, tools.name AS tname,
		components.name AS cname,
		tools.description, tools.generation
	FROM tools JOIN components ON tools.component = components.id
	ORDER BY tools.id",
	"<tr><td>%id</td><td>%tname</td><td>%cname</td><td>%description</td><td class='numresult'>%generation</td></tr>"); ?>
</table>

</li>

<li>
generation - a <i>sequence</i> for generation numbers, so we
can distinguish runs of the various tools.
Currently at generation
<?php
generic_query("
	SELECT last_value FROM generation;",
	"%last_value");
?>.
</li>
<li>
generations - a table mapping generation numbers to
tools and timestamps. Columns
generation (a generation number from the corresponding sequence),
timestamp (a time string),
tool (a tool number) and
SVN revision (a number).
The SVN revision is optional, but can be useful to
indicate when things are not updating as they should.
The mapping for the current generation is:
<table class="boxed" width="100%">
<tr><th>Gen.</th><th>Tool</th><th>Timestamp</th><th>Revision</th><th class='numresult'>Result</th></tr>
<?php
generic_query("
	SELECT generations.generation, generations.revision,
		tools.name, components.name AS cname, timestamp, count
	FROM generations,tools
	WHERE tools.id = generations.tool AND
		components.id = tools.component AND
		generations.generation>(SELECT last_value FROM generation)-4
	ORDER BY generation",
    "
<tr><td><b>%generation</b></td>
    <td>%name-%cname</td>
    <td>%timestamp</td>
    <td>%revision</td>
    <td class='numresult'>%count</td>
</tr>");
?>
</table>
</li>
<li>results_apidox - the results from apidox runs.
Columns are generation (to track which run the data is from),
component (integer index in the components table),
module (SVN module name),
application (subdirectory in module),
issues (defect count),
checksum and report (path to log file, for instance).
Current (non-zero) results for KDE 3.5 are:

<table class="boxed" width="100%">
<tr><th>Module</th><th class="numresult">Errors</th></tr>
<?php
generic_query(
	'SELECT module,sum(issues) as issues FROM results_apidox
	WHERE generation=(SELECT generation FROM tools WHERE id=1)
		AND issues > 0
	GROUP BY module',
	"<tr><td>%module</td><td class='numresult'>%issues</td></tr>")
?>
</table>
</li>

<li>results_sanitizer - the results from the user documentation
sanitizer runs.
Since the generation is not used for the sanitizer results
(there's only the most very recent results stored for each tool),
there is no generation column.
Columns are
checksum (an md5 hash to identify when things have changed),
component (an index into the components table),
module (SVN module name),
application (subdirectory in module),
issues (defect count),
report (path to HTML report for this entry).
Current results for KDE extragear are:

<table class="boxed" width="100%">
<tr><th>Module</th><th>Application</th><th class="numresult">Errors</th></tr>
<?php
generic_query("
	SELECT module,application,issues
	FROM results_sanitizer
	WHERE component = 6",
	"<tr><td>%module</td><td>%application</td><td class='numresult'>%issues</td></tr>")
?>
</table>

</ul>


<?php include 'footer.inc' ?>
