<?php
include 'crumbs.inc' ;
crumb('About');

include 'header.inc';
?>

<h1>About the EBN</h1>

<p>
The English Breakfast Network is a collection of
machines that do automated KDE source artifact quality checking.
Basically, that means they have a SVN checkout of the entire KDE
codebase (including documentation and whatnot)
and they run checking tools on that.
</p>

<p>
The EBN is operated by Allen Winter.
<a href="http://ev.kde.org/">KDE e.V.</a> owns the hardware.
</p>

<!--
<ul>
<li>Allen Winter handles the APIDOX generation and statistics.</li>
<li>Frerich Raabe maintains the user documentation sanitizer.</li>
<li>Allen Winter maintains the code checking tool.</li>
</ul>
-->

<h1>Credits</h1>

<p>The logo at upper-left is crystalsvg/48x48/apps/kteatime.png,
while the list bullets are all slick/16x16/apps/kteatime.png.
Hurray for Open Source artists.
Finally, http://www.amgmedia.com/freephotos/
is just what I needed for some more teapots and bits.
</p>


<h1>Databases</h1>

<p>
The database used here is PostgreSQL. For an explanation of
which tables there are and what they do (really only of
interest to EBN contributors who need to wrestle with SQL),
take a look at the <a href="database.php">administrative page</a>.
</p>

<?php include 'footer.inc' ?>
