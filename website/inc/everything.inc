<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
# Copyright (C) 2007 by Allen Winter <winter@kde.org>
#
# This file does 'everything' for a regular web page view.
# Given that $i_am is set to a key in the $everything_table
# read in the desired results, generate graphs and spit out a
# nice webpage.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

include_once 'everything_table.inc';

if (isset($i_am) and array_key_exists($i_am,$everything_table)) {

	$table_name = $everything_table[$i_am]["table_name"];
	$mypath = $everything_table[$i_am]["mypath"];
	$myname = $everything_table[$i_am]["myname"];
	$toolname = $everything_table[$i_am]["toolname"];
	$reportsdir = $everything_table[$i_am]["reportsdir"];
	if ($everything_table[$i_am]["haverss"]) {
		$rssfeed = 'http://'.$_SERVER['HTTP_HOST'].'/'.$mypath.'/rssfeed.php?'.htmlspecialchars($_SERVER['QUERY_STRING']);
	}

	include 'db.inc';

	if (!isset($db) or (!$db)) {
		include 'header.inc';
		print '<p>The database is off-line. The EBN is unable to function.</p>';
		include 'footer.inc';
		return;
	}

	include 'getresults.inc';

	$title = "$myname $title";
	include 'header.inc';
	include 'about.inc';

	if (isset($results_hook)) $results_hook->exec();

	print "<h2>".htmlspecialchars($title)."</h2>\n";
	print "<p>";
	include 'historygraph.inc';

	print "There are ".$totalIssues." issues remaining";
	if ( $haveComponent ) {
		print " as of ".str_replace(" ", "&nbsp;", $when);
	}
	print ". ";

	if ( isset($svn_revision) and ($svn_revision > 0) ) {
		print "Results are for SVN revision " . $svn_revision . ".\n";
	}

	print "You can click on the column headers to sort the data in the table as you like.";
	print "</p>\n\n";

	include 'resultstable.inc';


	include 'footer.inc';
} else {
	include 'header.inc';
	print "<p>Who am i? What? Where? Hunh?</p>\n";
	include 'footer.inc';
}

