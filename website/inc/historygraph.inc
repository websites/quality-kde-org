<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
#
# Produce a history graph showing results counts for
# recent generations.
#
# $toolId must be set and indicates the tool ID to use
# from the generations table -- getresults.inc does that.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.



if (isset($toolId) and is_numeric($toolId)) {
	print '<img style="float: right; margin: 0px 0px 1ex 1em; 
		width: 200px;
		height: 100px; padding: 2px;" 
		class="boxed" 
		src="/img/graph.php?f=' . $toolId;
	if ( $haveModule ) {
		print '&t=' . $table_name;
		print '&m=' . $queryVars["module"];
	}
	print '"
		alt="Graph of results" 
		title="Graph of results, with maximum and minimum error counts indicated for recent runs." />';
}

