<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
# Copyright (C) 2007 by Allen Winter <winter@kde.org>
#
# This file does 'everything' for an RSS feed.
# Given that $i_am is set to a key in the $everything_table
# read in the desired results and do the RSS thing.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

include_once 'everything_table.inc';

if (isset($i_am) and array_key_exists($i_am,$everything_table) and
	$everything_table[$i_am]["haverss"]) {
	parse_str($_SERVER['QUERY_STRING'], $queryVars);
	$table_name = $everything_table[$i_am]["table_name"];
	$mypath = $everything_table[$i_am]["mypath"];
	$myname = $everything_table[$i_am]["myname"];
	$toolname = $everything_table[$i_am]["toolname"];
	$reportsdir = $everything_table[$i_am]["reportsdir"];
	$rssfeed = 'http://'.$_SERVER['HTTP_HOST'].'/'.$mypath.'/rssfeed.php?'.htmlspecialchars($_SERVER['QUERY_STRING']);

	include 'db.inc';
	include 'getresults.inc';

	$title = "$myname $title";

	print '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n";
	print '<!DOCTYPE rss PUBLIC "-//Netscape Communications//DTD RSS 0.91//EN"'."\n";
	print '                     "http://my.netscape.com/publish/formats/rss-0.91.dtd">'."\n";
	print '<rss version="0.91">'."\n";
	print "  <channel>\n";
	print "    <title>".htmlspecialchars($title)."</title>\n";
	print "    <language>en</language>\n";
	print "    <link>http://".$_SERVER['HTTP_HOST']."/$mypath/index.php?".htmlspecialchars($_SERVER['QUERY_STRING'])."</link>\n";
	print "    <description>$myname statistics</description>\n";
	print "    <image>\n";
	print "      <title>English Breakfast Network</title>\n";
	print "      <url>http://www.englishbreakfastnetwork.org/img/logo.png</url>\n";
	print "      <link>http://www.englishbreakfastnetwork.org</link>\n";
	print "    </image>\n";
	foreach ($results as $result) {
		print "    <item>\n";
		print "      <title>".htmlspecialchars($result["subject"])." - " .$result['issues'] . " issues.</title>\n";
		print "      <link>http://www.englishbreakfastnetwork.org/$mypath/".htmlspecialchars($result["link"])."</link>\n";
		print "    </item>\n";
	}
	print "  </channel>\n";
	print "</rss>\n";
} else {
	print "Who am i? What? Where? Hunh?\n";
}
