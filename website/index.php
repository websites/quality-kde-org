<?php
include 'inc/header.inc';
include 'inc/db.inc';
?>
<p>
The English Breakfast Network (EBN) had been for more than a decade
dedicated to
the contemplation of tea,
KDE API Documentation Validation
User Documentation Validation,
Source Code Checking,
omphaloskepsis, and star-gazing.
</p>

<p>
EBN helped to improve the quality of KDE software. Countless commits referencing "EBN" or "krazy"
have made it a part of the history of that code.
</p>

<p>
These days a lot of the functionality is done by CI (not the contemplation of tea or even coffee, though). Thus EBN is shut down now.
</p>

<p>
Some checks of Krazy are still without counterparts in clazy or other tests run by CI. If you are interested in extending KDE CI to also run those remaining krazy checks, please get in contact with KDE systadmins.
</p>

<?php
include 'inc/footer.inc';
?>
