#include "const_comparison_ops.h"

#include "rpp-ast.h"
#include "rpp-parser.h"
#include "rpp-tokens.h"

using namespace rpp;

bool CheckConstComparisonOps::visit( InitDeclaratorAST *, SemanticEnvironment * )
{
    m_hadComparisonOp = false;
    return true;
}

void CheckConstComparisonOps::endVisit( InitDeclaratorAST *n, SemanticEnvironment * )
{
    if ( m_hadComparisonOp && n->declarator && !n->declarator->fun_cv ) {
        addResult( n->start_token, "Comparison operator should be const." );
    }
}

bool CheckConstComparisonOps::visit( FunctionDefinitionAST *n, SemanticEnvironment *env )
{
    /* For some reason InitDeclaratorAST gets visited after OperatorAST. So
     * lets hook into FunctionDefinitionAST (which gets visited before
     * OperatorAST) and visit the init declarator directly.
     */
    if ( n->init_declarator ) {
        n->init_declarator->accept( this, env );
    }
    return false;
}

bool CheckConstComparisonOps::visit( OperatorAST *n, SemanticEnvironment * )
{
    // Only interested if not a cast operator
    if ( n->op ) {
        switch ( parser()->tokenStream()->kind( n->op ) ) {
            case Token_eq:
            case Token_not_eq:
            case '>':
            case Token_geq:
            case '<':
            case Token_leq:
                m_hadComparisonOp = true;
                break;
        }
    }

    return false;
}

extern "C" rpp::CheckVisitor *create_instance()
{
    return new rpp::CheckConstComparisonOps;
}

