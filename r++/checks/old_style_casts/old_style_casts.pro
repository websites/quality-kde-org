TEMPLATE = lib
INCLUDEPATH += $$PWD $$PWD/../../lib $$PWD/../../shared
DESTDIR = $$PWD/../../bin
TARGET = r++check_old_style_casts
HEADERS += old_style_casts.h
SOURCES += old_style_casts.cpp
CONFIG += plugin
LIBS += -L$$PWD/../../bin -lr++
