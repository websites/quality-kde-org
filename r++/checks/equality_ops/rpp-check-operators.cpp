
#include "rpp-check-operators.h"
#include "rpp-ast.h"
#include "rpp-tokens.h"
#include "rpp-lexer.h"
#include "rpp-parser.h"

#include <QtCore/QtDebug>

namespace rpp {

class VisitInitDeclarator: protected ASTVisitor
{
  CheckOperators::Info &_M_info;
  const Parser *_M_parser;
  
public:
    VisitInitDeclarator(CheckOperators::Info &info, const Parser *parser):
    _M_info(info), _M_parser(parser) {}
    
  void operator () (InitDeclaratorAST *ast) {
    Q_ASSERT(ast != 0);
    Q_ASSERT(ast->declarator != 0);

    ast->accept(this, /*env=*/ 0);
  }

protected:
  virtual bool visit (OperatorAST *ast, SemanticEnvironment *)
  {
    const TokenStream *token_stream = _M_parser->tokenStream();
    
    switch (token_stream->kind(ast->op)) {          
      case Token_eq:
        _M_info.has_operator_eq = true;
        break;
        
      case Token_not_eq:
        _M_info.has_operator_not_eq = true;
        break;
        
      default:
        break;
    } // switch
        
    return false;
  }
};

  
bool CheckOperators::visit (ClassSpecifierAST *ast, SemanticEnvironment *)
{
  _M_info.push_back(Info(ast));
  return true;
}

void CheckOperators::endVisit (ClassSpecifierAST *ast, SemanticEnvironment *)
{
  const Info &info = _M_info.back();
  
  if (info.has_operator_eq != info.has_operator_not_eq)
    {
      const TokenStream *token_stream = parser()->tokenStream();
      const std::size_t nameId = ast->name->unqualified_name->id;
      QString cn = token_stream->token( nameId ).extra.symbol->as_string();
      if (info.has_operator_eq)
        addResult (ast->start_token, "Class " + cn + " has operator== but no operator!=" );
      else
        addResult (ast->start_token, "Class " + cn + " has operator!= but no operator==" );
    }
  
  _M_info.pop_back();
}

bool CheckOperators::visit (FunctionDefinitionAST *ast, SemanticEnvironment *env)
{
  if (ast->init_declarator)
    ast->init_declarator->accept(this, env);
  
  return false;
}

bool CheckOperators::visit (InitDeclaratorAST *ast, SemanticEnvironment *)
{
  if (ast->declarator && ! _M_info.isEmpty())
    {
      VisitInitDeclarator visitInitDeclarator(_M_info.back(), parser ());
    
      visitInitDeclarator (ast);
    }

  return true;
}

} // namespace rpp

extern "C" rpp::CheckVisitor *create_instance()
{
  return new rpp::CheckOperators;
}

