/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "rpp-ast.h"

namespace rpp {

void AccessSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (specs, visitor, context);
    }
  visitor->endVisit (this, context);
}


void AsmDefinitionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (cv, visitor, context);
    }
  visitor->endVisit (this, context);
}


void BaseClauseAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (base_specifiers, visitor, context);
    }
  visitor->endVisit (this, context);
}


void BaseSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (virt, visitor, context);
      acceptToken (access_specifier, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void BinaryExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
      acceptNode (left_expression, visitor, context);
      acceptNode (right_expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void CastExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_id, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ClassMemberAccessAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ClassSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (win_decl_specifiers, visitor, context);
      acceptToken (class_key, visitor, context);
      acceptNode (name, visitor, context);
      acceptNode (base_clause, visitor, context);
      acceptNodes (member_specs, visitor, context);
    }
  visitor->endVisit (this, context);
}


void CompoundStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (statements, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ConditionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNode (declarator, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ConditionalExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (condition, visitor, context);
      acceptNode (left_expression, visitor, context);
      acceptNode (right_expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void CppCastExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
      acceptNode (type_id, visitor, context);
      acceptNode (expression, visitor, context);
      acceptNodes (sub_expressions, visitor, context);
    }
  visitor->endVisit (this, context);
}


void CtorInitializerAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (colon, visitor, context);
      acceptNodes (member_initializers, visitor, context);
    }
  visitor->endVisit (this, context);
}


void DeclarationStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (declaration, visitor, context);
    }
  visitor->endVisit (this, context);
}


void DeclaratorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (ptr_ops, visitor, context);
      acceptNode (sub_declarator, visitor, context);
      acceptNode (id, visitor, context);
      acceptNode (bit_expression, visitor, context);
      acceptNodes (array_dimensions, visitor, context);
      acceptNode (parameter_declaration_clause, visitor, context);
      acceptTokens (fun_cv, visitor, context);
      acceptNode (exception_spec, visitor, context);
    }
  visitor->endVisit (this, context);
}


void DeleteExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (scope_token, visitor, context);
      acceptToken (delete_token, visitor, context);
      acceptToken (lbracket_token, visitor, context);
      acceptToken (rbracket_token, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void DoStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (statement, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ElaboratedTypeSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (type, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void EnumSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (name, visitor, context);
      acceptNodes (enumerators, visitor, context);
    }
  visitor->endVisit (this, context);
}


void EnumeratorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (id, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ExceptionSpecificationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (ellipsis, visitor, context);
      acceptNodes (type_ids, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ExpressionOrDeclarationStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (expression, visitor, context);
      acceptNode (declaration, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ExpressionStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void FunctionCallAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (arguments, visitor, context);
    }
  visitor->endVisit (this, context);
}


void FunctionDefinitionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (win_decl_specifiers, visitor, context);
      acceptTokens (storage_specifiers, visitor, context);
      acceptTokens (function_specifiers, visitor, context);
      acceptNode (type_specifier, visitor, context);
      acceptNode (init_declarator, visitor, context);
      acceptNode (function_body, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ForStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (init_statement, visitor, context);
      acceptNode (condition, visitor, context);
      acceptNode (expression, visitor, context);
      acceptNode (statement, visitor, context);
    }
  visitor->endVisit (this, context);
}


void IfStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (condition, visitor, context);
      acceptNode (statement, visitor, context);
      acceptNode (else_statement, visitor, context);
    }
  visitor->endVisit (this, context);
}


void IncrDecrExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
    }
  visitor->endVisit (this, context);
}


void InitDeclaratorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (declarator, visitor, context);
      acceptNode (initializer, visitor, context);
    }
  visitor->endVisit (this, context);
}


void InitializerAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (initializer_clause, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void InitializerClauseAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void LabeledStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (label, visitor, context);
    }
  visitor->endVisit (this, context);
}


void LinkageBodyAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (declarations, visitor, context);
    }
  visitor->endVisit (this, context);
}


void LinkageSpecificationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (extern_type, visitor, context);
      acceptNode (linkage_body, visitor, context);
      acceptNode (declaration, visitor, context);
    }
  visitor->endVisit (this, context);
}


void MemInitializerAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (initializer_id, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NameAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (qualified_names, visitor, context);
      acceptNode (unqualified_name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NamespaceAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (namespace_name, visitor, context);
      acceptNode (linkage_body, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NamespaceAliasDefinitionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (namespace_name, visitor, context);
      acceptNode (alias_name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NewDeclaratorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (ptr_op, visitor, context);
      acceptNode (sub_declarator, visitor, context);
      acceptNodes (expressions, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NewExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (scope_token, visitor, context);
      acceptToken (new_token, visitor, context);
      acceptNode (expression, visitor, context);
      acceptNode (type_id, visitor, context);
      acceptNode (new_type_id, visitor, context);
      acceptNode (new_initializer, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NewInitializerAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void NewTypeIdAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNode (new_initializer, visitor, context);
      acceptNode (new_declarator, visitor, context);
    }
  visitor->endVisit (this, context);
}


void OperatorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
      acceptToken (open, visitor, context);
      acceptToken (close, visitor, context);
    }
  visitor->endVisit (this, context);
}


void OperatorFunctionIdAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (op, visitor, context);
      acceptNode (type_specifier, visitor, context);
      acceptNodes (ptr_ops, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ParameterDeclarationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNode (declarator, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ParameterDeclarationClauseAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (parameter_declarations, visitor, context);
      acceptToken (ellipsis, visitor, context);
    }
  visitor->endVisit (this, context);
}


void PostfixExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNode (expression, visitor, context);
      acceptNodes (sub_expressions, visitor, context);
    }
  visitor->endVisit (this, context);
}


void PrimaryExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (literal, visitor, context);
      acceptToken (token, visitor, context);
      acceptNode (expression_statement, visitor, context);
      acceptNode (sub_expression, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void PtrOperatorAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (cv, visitor, context);
      acceptToken (op, visitor, context);
      acceptNode (mem_ptr, visitor, context);
    }
  visitor->endVisit (this, context);
}


void PtrToMemberAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      // ### visit me
    }
  visitor->endVisit (this, context);
}


void ReturnStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void SimpleDeclarationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (storage_specifiers, visitor, context);
      acceptTokens (function_specifiers, visitor, context);
      acceptNode (type_specifier, visitor, context);
      acceptNodes (init_declarators, visitor, context);
      acceptNode (win_decl_specifiers, visitor, context);
    }
  visitor->endVisit (this, context);
}


void SimpleTypeSpecifierAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (integrals, visitor, context);
      acceptToken (type_of, visitor, context);
      acceptNode (type_id, visitor, context);
      acceptNode (expression, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void SizeofExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (sizeof_token, visitor, context);
      acceptNode (type_id, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void StringLiteralAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptTokens (literals, visitor, context);
    }
  visitor->endVisit (this, context);
}


void SubscriptExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (subscript, visitor, context);
    }
  visitor->endVisit (this, context);
}


void SwitchStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (condition, visitor, context);
      acceptNode (statement, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TemplateArgumentAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_id, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TemplateDeclarationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (exported, visitor, context);
      acceptNodes (template_parameters, visitor, context);
      acceptNode (declaration, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TemplateParameterAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_parameter, visitor, context);
      acceptNode (parameter_declaration, visitor, context);
    }
  visitor->endVisit (this, context);
}


void ThrowExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (throw_token, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TranslationUnitAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNodes (declarations, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TryBlockStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      // ### visit me
    }
  visitor->endVisit (this, context);
}


void TypeIdAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNode (declarator, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TypeIdentificationAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (typename_token, visitor, context);
      acceptNode (name, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TypeParameterAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (type, visitor, context);
      acceptNode (name, visitor, context);
      acceptNode (type_id, visitor, context);
      acceptNodes (template_parameters, visitor, context);
      acceptNode (template_name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void TypedefAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (type_specifier, visitor, context);
      acceptNodes (init_declarators, visitor, context);
    }
  visitor->endVisit (this, context);
}


void UnaryExpressionAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (op, visitor, context);
      acceptNode (expression, visitor, context);
    }
  visitor->endVisit (this, context);
}


void UnqualifiedNameAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (tilde, visitor, context);
      acceptToken (id, visitor, context);
      acceptNode (operator_id, visitor, context);
      acceptNodes (template_arguments, visitor, context);
    }
  visitor->endVisit (this, context);
}


void UsingAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (type_name, visitor, context);
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void UsingDirectiveAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (name, visitor, context);
    }
  visitor->endVisit (this, context);
}


void WhileStatementAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptNode (condition, visitor, context);
      acceptNode (statement, visitor, context);
    }
  visitor->endVisit (this, context);
}


void WinDeclSpecAST::accept0 (ASTVisitor *visitor, SemanticEnvironment *context)
{
  if (visitor->visit (this, context))
    {
      acceptToken (specifier, visitor, context);
      acceptToken (modifier, visitor, context);
    }
  visitor->endVisit (this, context);
}

} // namespace rpp

// kate: space-indent on; indent-width 2; replace-tabs on;
