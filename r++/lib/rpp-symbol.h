/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef SYMBOL_H
#define SYMBOL_H

#include <QtCore/QString>
#include <QtCore/QSet>

namespace rpp {

class NameSymbol
{
public:
  inline QString as_string() const { return QString::fromUtf8 (_M_data, int (_M_size)); } // ### deprecated

  inline bool operator == (const NameSymbol &other) const
  {
    return _M_size == other._M_size
        && qstrncmp (_M_data, other._M_data, int (_M_size)) == 0;
  }

  inline const char *begin() const { return _M_data; }
  inline const char *end() const { return _M_data + _M_size; }

  inline const char at(std::size_t index) const { return _M_data [index]; }

  inline std::size_t size() const { return _M_size; }
  inline const char *data() const { return _M_data; }

protected:
  inline NameSymbol() {}

  inline NameSymbol(const char *data, std::size_t size)
    : _M_data(data), _M_size(size) {}

private:
  const char *_M_data;
  std::size_t _M_size;

private:
  void operator = (const NameSymbol &);

  friend class NameTable;
};

inline uint qHash(const NameSymbol &name)
{
  uint hash_value = 0;

  for (std::size_t i = 0; i < name.size (); ++i)
    hash_value = (hash_value << 5) - hash_value + name.at (i);

  return hash_value;
}

class NameTable
{
public:
  NameTable() {}

  const NameSymbol *findOrInsert (const char *str, std::size_t len)
  {
    NameSymbol tmp (str, len);
    return &*_M_storage.insert (tmp);
  }

  inline std::size_t count () const { return _M_storage.size(); }
  inline std::size_t size () const { return _M_storage.size(); }

private:
  QSet<NameSymbol> _M_storage;

private:
  NameTable(const NameTable &other);
  void operator = (const NameTable &other);
};

} // namespace rpp


#endif // SYMBOL_H

// kate: space-indent on; indent-width 2; replace-tabs on;
