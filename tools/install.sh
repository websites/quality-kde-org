#!/bin/sh
#
# Install general EBN tools.
#
# Usage:
#    install [--prefix=/dir]
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


# change TOP to whatever you like for your top-level installation directory
if ( test `hostname | egrep -c www` -gt 0 ) then
  EBN=1
  TOP=/mnt/ebn
else
  EBN=0
  TOP=/usr/local/EBN
fi

# check command-line argument
case "$1" in
--prefix=*)
  TOP=`echo "$1" | sed -e 's+--prefix=++'`
  ;;
*)
  ;;
esac

mkdir -p $TOP/bin $TOP/lib $TOP/share/apidox
for i in bin lib share/apidox
do
	test -d $TOP/$i || { echo "Expected $TOP/$i to be a directory." ; exit 1 ; }
	test -w $TOP/$i || { echo "Cannot write to directory $TOP/$i." ; exit 1 ; }
done


BINDIR=$TOP/bin
DATADIR=$TOP/share/apidox
PERM=775

progs="svn-rsync update-checkouts \
       update-apidox update-qch update-man \
       update-mapper genclassmapper.sh genprojectmapper.sh gennamespacemapper.sh \
       update-krazy update-i18n \
       update-graphs date.py svnpy.py svnpy.pyc weekdaily.py who2.py neep.pil neep.pbm \
       update-sanitizer \
       update-cmakedoc \
       cleanup-db \
       update-all"
for p in $progs
do
  cp $p $BINDIR
  chmod $PERM $BINDIR/$p
  echo "$p installed"
done
