#! /bin/sh
VERBOSE=false
VERSION=""

# Read arguments...
test "x--verbose" = "x$1" && { shift ; VERBOSE=true ; }
test "x--version" = "x$1" && { shift ; VERSION="$1" ; shift ; }
test -z "$VERSION" && { echo "No version given"; exit 1 ; }

# Configuration
# APIPATH: top-level for API stuff
APIPATH=/srv/www/api.kde.org
# OUTPATH: where the final output goes
OUTPATH=/srv/www/englishbreakfastnetwork.org/apidocs/apidox-$VERSION
# TEMPPATH: where the temporary output goes
TEMPPATH=/srv/tmp/tempdocs/apidox-$VERSION
# TAGPATH: where tag files (i.e, qtX.Y.tag) reside
TAGPATH=$HOME/quality-kde-org/apidox/data
# BINPATH: where the helper programs (i.e. doxylog2html.pl) reside
BINPATH=/home/api/bin
# SRCPREFIX: where the top-level KDE source code resides
SRCPREFIX=/srv/sources
# LOGPATH: where logs get moved
LOGPATH=/srv/logs/api/$VERSION
# SRCPATH: where the source code for this component resides
SRCPATH=$SRCPREFIX/$VERSION
# LIBSPATH: where the source code for kdelibs resides (so we can find doxygen.sh)
LIBSPATH=$HOME/
# DOXDATAPATH: where the doxygen.sh data lives
DOXDATAPATH=$LIBSPATH/kdelibs/doc/common
# Database access variables
PSQL="psql" #replace with echo when/if the database is in a bad way
PSQL_FLAGS="-t -h localhost -U kde ebn"

GITREV() {
  savepath=`pwd`
  cd $1 2>/dev/null
  if ( test $? -ne 0 )
  then
    REV=0
  else
    REV=`git rev-parse --short HEAD 2>/dev/null`
    if ( test -z "$REV" )
    then
      REV=0
    fi
  fi
  cd $savepath
}

# Determine the modules we need to build the documentation of...
case "$VERSION" in
  kde-3.5|kde-4.* )
    SRCPATH=$SRCPREFIX/$VERSION
    MODULES=`ls $SRCPATH`
    DOXDATAPATH=$SRCPREFIX/$VERSION/kdelibs/doc/common
    ;;
  frameworks )
    SRCPATH=$SRCPREFIX/$VERSION
    MODULES=frameworks5
    ;;
  * )
    SRCPATH=$SRCPREFIX/$VERSION
    MODULES=`ls $SRCPATH`
    ;;
esac

# If argument given, use it as modules list to run
test -n "$1" && MODULES="$1"

# Prepare to run....
test -d "$OUTPATH" || { echo "No path $OUTPATH -- creating it" ; mkdir -p $OUTPATH || exit 1 ; }
test -d "$TEMPPATH" || { echo "No path $TEMPPATH -- creating it" ; mkdir -p $TEMPPATH || exit 1 ; }
test -d "$LOGPATH" || { echo "No path $LOGPATH -- creating it" ; mkdir -p $LOGPATH || exit 1 ; }
test -d "$SRCPATH" || { echo "No source path $SRCPATH" ; exit 1 ; }
cd "$TEMPPATH" || { echo "Cannot cd into $TEMPPATH" ; exit 1 ; }

$VERBOSE && echo "* Parsing logs for $MODULES"

timestamp=""

# Find my flavor (dox for various versions have different flavors)
if [ "$PSQL" != "echo" ]; then
  component=`echo "SELECT id FROM components WHERE name = '$VERSION';" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'`
expr 0 + "$component" > /dev/null 2>&1 || { echo "Could not get component."; exit 1 ; }
  flavor=`echo "SELECT id FROM tools WHERE name='dox' AND component = $component ;" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'`
expr 0 + "$flavor" > /dev/null 2>&1 || { echo "Could not get flavor." ; exit 1 ; }
else
  component="kde"
  flavor="kde"
fi

# If argument (module list) given, don't overwrite log data
if [ "$PSQL" != "echo" ]; then
  if test -z "$1" ; then
    timestamp=`date "+%B %d %Y %T"`
    # Bump generation now
    generation=`echo "SELECT * FROM nextval('generation') ;" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'` ;
  else
    generation=`echo "SELECT generation FROM tools WHERE id = $flavor" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'` ;
    generation=`expr 1 + "$generation"`
  fi
else
  generation="1"
fi

expr 0 + "$generation" > /dev/null 2>&1 || { echo "Bad generation." ; exit 1 ; }

$VERBOSE && { echo "* Component $component"; echo "* Tool      $flavor" ; echo "* Generation $generation" ; }

case "$VERSION" in
  kde-3.5)
    export QTDOCDIR=http://doc.qt.digia.com/3.3
    export QTDOCTAG=$TAGPATH/qt3.3.tag
    ;;
  frameworks* )
    export QTDOCDIR=http://qt-project.org/doc/qt-5.1
    export QTDOCTAG=$TAGPATH/qt5.1.tag
    ;;
  * )
    export QTDOCDIR=https://doc.qt.io/archives/qt-4.8
    export QTDOCTAG=$TAGPATH/qt4.8.tag
    ;;
esac

for i in $MODULES; do
        LOGFILE=$LOGPATH/$i.log
        test -d "$SRCPATH/$i" || { echo "Skipping $SRCPATH/$i" ; continue ; }

        # Just count the total number of errors
        AMT=`grep ^$SRCPATH $LOGFILE | wc -l`
        expr 0 + "$AMT" > /dev/null 2>&1 || AMT="0"
        $VERBOSE && { echo "* Number of errors: $AMT" ; \
                echo "* Number of applications: " `grep 'Creating apidox' "$i.log" | wc -l` ; }

        # An impenetrable forest of awkisms. Scan the logfile,
        # breaking it into sections using the *** Creating
        # header; count total errors under a top-level application
        # heading (eg. dcop or kio) and print the results as
        # lines with
        #     appname count
        #
        # ... then pipe that to a while loop that reads the results
        # and adds records to the results table for each appname.
        : > /tmp/doxygen.sh.$$
        awk 'BEGIN {t=-1;app=""; ot="'$i'"; }
             /\*\*\* Creating apidox/ {
                napp=$5; slash=index(napp,"/");
                if (slash>0) napp=substr($5,1,slash-1);
                if (napp!=app) {
                        if (t>=0) print app,t;
                        close( ot "-" app ".log" );
                        app=napp;
                        t=0;
                        print $0 > ( ot "-" app ".log" ) ;
                }
             }
             /^\/.*[Ww]arning/ { if (t>=0) t=t+1; }
             t>=0 { print $0 >> ( ot "-" app ".log" ); }
             END { if (t>=0) print app,t;}' $LOGFILE | \
        while true ; do
        read APP COUNT ;
                test -z "$APP" && break ;
                echo "$APP $COUNT" >> /tmp/doxygen.sh.$$
                GITREV $SRCPATH/$i/$APP
                $BINPATH/doxylog2html.pl --explain --export=ebn --cms="$VERSION/$i/$APP" --rev=$REV $TEMPPATH/$i-$APP.log | sed s+/tempdocs/+/apidocs/+g > $OUTPATH/$i-$APP.html;
                mv -f $TEMPPATH/$i-$APP.log $OUTPATH;
                echo "INSERT INTO results_apidox VALUES ( $generation, '$i', '$APP', $COUNT, $component, '', 'apidox-$VERSION/$i-$APP.html' ); ";
        done | \
        $PSQL $PSQL_FLAGS > /dev/null
done

if [ "$PSQL" != "echo" ]; then
  if test -z "$1" ; then
  (
    REV=0 #lacking anything better at the moment
    echo "INSERT INTO generations VALUES ( $generation, '$timestamp', (SELECT SUM(issues) FROM results_apidox WHERE generation = $generation), $flavor, $REV ) ;"
    echo "UPDATE tools SET generation = $generation WHERE id = $flavor ;"
  ) | $PSQL $PSQL_FLAGS | sed 's+^\s*++' > /dev/null
  fi
fi

rm -f /tmp/doxygen.sh.$$
