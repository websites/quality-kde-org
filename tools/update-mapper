#! /bin/sh
# Wrapper to build all the search maps.

# Top-level path where the web pages reside
APIPATH=/srv/www/api.kde.org
# Name of the directory where the final search maps will be stored
MAPDIRNAME=searchmaps
# Full path to where the final search maps will be stored (recreated each run)
MAPDIR=$APIPATH/$MAPDIRNAME
# Full path to where the temporary search maps will be stored (created, then removed)
TMAPDIR=/srv/tmp/mapper/$MAPDIRNAME.$$
# Full path to the sources
SOURCES=/srv/sources

# Top-level path to where the search map generators reside
BINPATH=/home/api/bin/mapper/
# Full path to the program that generates the class map
GEN_CLASSMAP=$BINPATH/genclassmapper.sh
# Full path to the program that generates the namespace map
GEN_NAMESPACEMAP=$BINPATH/gennamespacemapper.sh
# Full path to the program that generates the project map
GEN_PROJECTMAP=$BINPATH/genprojectmapper.sh

# KDE SC components to generate the search maps
components="4.x 4.14 3.5"

# KDE SC modules to generate the search maps
modules=`ls $SOURCES/kde-4.x`
# Extragear modules to generate the search maps
emodules=`ls $SOURCES/extragear`

DOIT()
{
    out=$TMAPDIR/classmap-$1-$2.inc
    rm -f $out
    bash $GEN_CLASSMAP $1 $2 > $out

    out=$TMAPDIR/namespacemap-$1-$2.inc
    rm -f $out
    bash $GEN_NAMESPACEMAP $1 $2 > $out

    out=$TMAPDIR/projectmap-$1-$2.inc
    rm -f $out
    bash $GEN_PROJECTMAP $1 $2 > $out
}

MERGE_SEARCHES()
{
    out=$TMAPDIR/map-$1-$2.inc
    rm -f $out

    echo '<?php' > $out
    echo "\$map = array(" >> $out

    cat $TMAPDIR/classmap-$1-$2.inc | grep =\> | grep -v \"index\" >> $out
    cat $TMAPDIR/namespacemap-$1-$2.inc | grep =\> | grep -v \"index\" >> $out
    cat $TMAPDIR/projectmap-$1-$2.inc | grep =\> | grep -v \"index\" >> $out

    echo ");" >> $out
    echo "?>" >> $out
}

COMBINE_MODULES_FOR_COMPONENT()
{
    out=$TMAPDIR/map-$1-ALL.inc
    rm -f $out
    echo "<?php" >> $out
    echo "\$map = array(" >> $out
    if [ "$1" = "extragear" ]; then
      for m in $emodules; do
        cat $TMAPDIR/map-$1-$m.inc | grep =\> | grep -v \"index\" >> $out
      done
    else
      for m in $modules; do
        cat $TMAPDIR/map-$1-$m.inc | grep =\> | grep -v \"index\" >> $out
      done
    fi
    echo ");" >> $out
    echo "?>" >> $out
}

COMBINE_COMPONENTS_FOR_MODULE()
{
    out=$TMAPDIR/map-ALL-$1.inc
    rm -f $out
    echo "<?php" >> $out
    echo "\$map = array(" >> $out
    for c in $components; do
      cat $TMAPDIR/map-$c-$1.inc | grep =\> | grep -v \"index\" >> $out
    done
    echo ");" >> $out
    echo "?>" >> $out
}

if [ ! -d $TMAPDIR ]; then
    mkdir -p $TMAPDIR || exit
fi
cd $APIPATH

# Create a map files for each (component,module) and for each type of search
for c in $components; do
  for m in $modules; do
    DOIT $c $m
  done
done

for m in $emodules; do
  DOIT extragear $m
done
DOIT kdesupport kdesupport

# Merge all searches for each (component,module)
for c in $components; do
  for m in $modules; do
    MERGE_SEARCHES $c $m
  done
done
for m in $emodules; do
  MERGE_SEARCHES extragear $m
done
MERGE_SEARCHES kdesupport kdesupport

# Create an ALL map file for each component
for c in $components; do
  COMBINE_MODULES_FOR_COMPONENT $c
done
COMBINE_MODULES_FOR_COMPONENT extragear
cp -f $TMAPDIR/map-kdesupport-kdesupport.inc $TMAPDIR/map-kdesupport-ALL.inc

# create an ALL map file for each module
for m in $modules; do
  COMBINE_COMPONENTS_FOR_MODULE $m
done
cp -f $TMAPDIR/map-kdesupport-kdesupport.inc $TMAPDIR/map-ALL-kdesupport.inc

# create one ALL map for everything
OUT=$TMAPDIR/map-ALL-ALL.inc
rm -f $OUT
echo "<?php" >> $OUT
echo "\$map = array(" >> $OUT
for c in $components; do
  cat $TMAPDIR/map-$c-ALL.inc | grep =\> | grep -v \"index\" >> $OUT
done
cat $TMAPDIR/map-extragear-ALL.inc | grep =\> | grep -v \"index\" >> $OUT
cat $TMAPDIR/map-kdesupport-ALL.inc | grep =\> | grep -v \"index\" >> $OUT
echo ");" >> $OUT
echo "?>" >> $OUT

# Finally, move the temp into the final
rm -rf $MAPDIR
mv -f $TMAPDIR $MAPDIR
