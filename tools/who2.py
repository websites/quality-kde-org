#! /usr/bin/env python

# Who - a program to tell you who committed in a given
# path / repository in a given period of revisions, with
# the number of changed files and changed lines, too.

# License is GPL v 2.

import sys
import os
import getopt
from svn import fs, core, repos
import Image, ImageDraw, ImageFont
import svnpy


# Produce a list of committers for the repo from rev. First to Last
def who( repospath, First, Last, Except ):
  # Strip trailing / since it breaks stuff
  if repospath[-1] == "/":
    repospath = repospath[:-1]

  # Get the repo and the Last revision in it.
  Repo = repos.open( repospath )
  FS = repos.fs( Repo )
  if Last is None:
    Last = fs.youngest_rev( FS )

  if Last < First:
    raise ArgumentError,"First must be before Last (%d)" % Last

  # Information collects in the Authors hashes.
  # Authors is keyed by the name of the author; it indicates
  #   the number of commits by that author.
  Authors = {}
  Components = {}
  SkipComponents = [ '', 'branches', 'trunk', 'KDE', 'stable', 'l10n', 'work', '3.5', 'playground', 'extragear' ]

  # Now walk all the SVN commits.
  p = svnpy.Progress( First );
  for i in xrange(First, Last+1):
    Author = fs.revision_prop( FS, i, core.SVN_PROP_REVISION_AUTHOR) or ''
    p.next()
    if Author in Except:
      continue

    root = fs.revision_root( FS, i )
    changed_files = fs.svn_fs_paths_changed( root )

    if not Authors.has_key(Author):
      Authors[Author] = (0,0)
    commits = Authors[Author][0] + 1
    files = Authors[Author][1] + len(changed_files)
    Authors[Author] = (commits,files)

    for f in changed_files.keys():
      l = f.split("/")
      c = 0
      while l[c] in SkipComponents:
        c += 1
      if not Components.has_key(l[c]):
        Components[l[c]] = 0
      Components[l[c]] += 1

  return ( Authors, Components )


def postprocess(Authors,Limit=None,Except=['']):
  for i in Except:
    if Authors.has_key(i):
      del Authors[i]

  l = zip(Authors.values(),Authors.keys())
  l.sort()
  l.reverse()

  if not Limit is None:
    l = l[:Limit]
 
  return l

def graph(l,filename):
  # Calculate the "grid" size
  font = ImageFont.load("neep.pil")
  BlockX,BlockY = font.getsize("t")

  # Experimentally verified.
  MaxUsernameLength = 16

  # Create the image and fill it with something soothing
  ImageWidth = (len(l)+26) * BlockX
  ImageHeight = (len(l)+1) * BlockY
  im = Image.new( "RGB", ( ImageWidth, ImageHeight ) )
  draw = ImageDraw.Draw(im)
  draw.rectangle( [ (0,0),(ImageWidth,ImageHeight) ], fill="#FFFFFF", outline="#FFFFFF")

  c=0

  # Since commits is the first entry, l[0][0][0] is the maximum number
  # of commits, but to get the rest we need to do some list comprehension.
  MostCommits = l[0][0][0]
  filelist = [ x[0][1] for x in l ]
  MostFiles = max(filelist)

  pale="#FFE1AD"
  darker="#C8AF8C"

  # Here, 16*BlockX is for the account name and 6* for the numbers
  AvailableWidth = ImageWidth - 16*BlockX - 6*BlockX - 8
  for i in l:
    Label = i[1]
    TextX,TextY = font.getsize( Label )

    # Account label
    draw.text( ( 16*BlockX - 4 - TextX , c*BlockY ), 
      Label, font=font, fill="black" )
    data = i[0]

    # Number of commits
    X = 16*BlockX + (data[0] * AvailableWidth / MostCommits)
    draw.rectangle( [ (16*BlockX,c*BlockY+1), (X,c*BlockY+BlockY-1) ], fill=pale )
    # Draw number of files 
    X = 16*BlockX + (data[1] * AvailableWidth / MostFiles)
    draw.rectangle( [ (16*BlockX,c*BlockY+BlockY/2-1), (X,c*BlockY+BlockY/2+1) ], fill=darker )

    # Draw labels overtop
    draw.text( ( 18*BlockX + AvailableWidth, c*BlockY+1 ), "%4d" % data[0], font=font, fill=darker )
    draw.text( ( X + 4, c*BlockY+1 ), str(data[1]), font=font, fill="black" )
    c = c + 1
  del draw
  im.save( filename, "PNG" )


def graphComponents(l,filename):
  # Calculate the "grid" size
  font = ImageFont.load("neep.pil")
  BlockX,BlockY = font.getsize("t")

  # Experimentally verified.
  MaxUsernameLength = 16

  # Create the image and fill it with something soothing
  ImageWidth = (len(l)+26) * BlockX
  ImageHeight = (len(l)+1) * BlockY
  im = Image.new( "RGB", ( ImageWidth, ImageHeight ) )
  draw = ImageDraw.Draw(im)
  draw.rectangle( [ (0,0),(ImageWidth,ImageHeight) ], fill="#FFFFFF", outline="#FFFFFF")

  c=0

  # Since commits is the first entry, l[0][0][0] is the maximum number
  # of commits, but to get the rest we need to do some list comprehension.
  MostCommits = l[0][0]

  darker="#C8AF8C"
  pale="#FFE1AD"

  # Here, 16*BlockX is for the account name and 6* for the numbers
  AvailableWidth = ImageWidth - 16*BlockX - 6*BlockX - 8
  for i in l:
    Label = i[1]
    TextX,TextY = font.getsize( Label )
    draw.text( ( 16*BlockX - 4 - TextX , c*BlockY ), 
      Label, font=font, fill="black" )
    data = i[0]
    X = 16*BlockX + (data * AvailableWidth / MostCommits)
    draw.rectangle( [ (16*BlockX,c*BlockY+1), (X,c*BlockY+BlockY-1) ], fill=pale )
    draw.text( ( X + 4, c*BlockY+1 ), str(data), font=font, fill="black" )
    c = c + 1
  del draw
  im.save( filename, "PNG" )




if __name__ == '__main__':
  Usage = """
Usage: %s [OPTIONS] REPOS-PATH
  --first=N        First revision to read (default 1)
  --last=N         Last revision to read (default HEAD)
  --outdir=dir     Directory for output graph (default none)
  --limit=N        Limit output to top N committers (default all)
  --exclude=S[,S]  List of accounts to exclude (default '')

""" % sys.argv[0]

  if len(sys.argv) < 2:
    sys.stderr.write(Usage)
    sys.exit(1)

  l = []
  try:
    l = getopt.getopt(sys.argv[1:],'',['first=','last=','outdir=','limit=','exclude='])
  except getopt.GetoptError:
    sys.stderr.write(Usage)
    sys.exit(1)

  if len(l[1]) != 1:
    sys.stderr.write(Usage)
    sys.exit(1)

  First = 1
  Last = None;
  Except = ['']
  for i in l[0]:
    if i[0] == '--first':
      First = int(i[1])
    if i[0] == '--last':
      Last = int(i[1])
    if i[0] == '--exclude':
      Except = i[1].split(',')
  ( Authors, Components ) = who( l[1][0], First, Last, Except )

  Limit = None
  for i in l[0]:
    if i[0] == '--limit':
      Limit = int(i[1])
  CommitList = postprocess( Authors, Limit, Except )
  ComponentList = postprocess( Components, Limit, Except )
  del Authors
  del Components


  Dirname = None
  for i in l[0]:
    if i[0] == '--outdir':
      Dirname = i[1]
  if not Dirname is None:
    graph(CommitList,Dirname + "/commit-who.png")
    graphComponents(ComponentList,Dirname + "/commit-component.png")

