#!/bin/sh
# map a search maps for classes

# $1 is the component and $2 is the module

if ( test ! -d $1-api/$2-apidocs )
then
  exit 1
fi

echo "<?php"
echo "\$map = array("
for htmlfile in `find $1-api/$2-apidocs/ -type f -name "class[A-Z]*.html" | grep -v "\-members.html$"`; do
	if ( test `grep -c -e -members $htmlfile` -gt 0 ) then
          classname=`echo $htmlfile | sed -e "s,.*/class\\(.*\\).html,\1," -e "s,.*_1_1,,g" | tr "[A-Z]" "[a-z]"`
          echo "  \"$htmlfile\" => \"$classname\","
	fi
done
echo "  \"index\" => \"$1-api/\""
echo ");"
echo "?>"
