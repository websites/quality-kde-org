# This file defines the tools which are run over a given chunk of markup. Feel
# free to add new stuff here; just keep in mind that the order in which you
# specify the tools is also the order in which the tools are executed.
#

toolDefinitions = [
# First, try checkXML
(
"Checking for well-formed XML",
"tools/check-xml-syntax.sh %s",
"Any XML document which you plan to get past an XSLT processor needs to be "
"valid XML, and the KDE DocBook files are no exception to this. There is no "
"chance to turn your DocBook file into HTML or any other format if it's not "
"valid."
),

# Now, have a look at the DOCTYPE declaration
(
"Checking for proper DOCTYPE declaration",
"tools/check-doctype.py %s",
"Not using the most up to date DOCTYPE declaration for your markup "
"means not being able to use the entities which might be suggested "
"below, when checking for applicable entities."
),

## Next comes check-empty-elements.py
# Commented out, not sure whether this is applicable anymore
#(
#"Testing for empty elements",
#"tools/check-empty-elements.py %s",
#"Elements without any content make it impossible to translate the DocBook file "
#"into other languages; in particular, the xml2po program which is used by the "
#"KDE translation framework will break. You need to remove those elements, or "
#"fill them with some dummy content like &quot;FILL ME WITH CONTENT&quot; or "
#"such."
#),

# Now check-multiple-root-elements.py
(
"Scanning for multiple root elements",
"tools/check-multiple-root-elements.py %s",
"Just like elements without any content, multiple root elements make it "
"impossible to translate the DocBook file into other languages. You should "
"split a file with multiple root elements up into a bunch of files with one "
"root element each."
),

# Now check-translator-markers.sh
(
"Looking for proper translator markers",
"tools/check-translator-markers.sh %s",
"People who spent time on translating your document should be given credit "
"appropriately. To do this, you need to specify two special comments in your "
"markup:<br/><br/>"
"<span class=\"markup\">&lt;!-- TRANS:ROLES_OF_TRANSLATORS --&gt;</span><br/>"
"This special comment must appear exactly once in your markup; you should "
"put it at the end of your <span class=\"markup\">&lt;authorgroup&gt;</span> "
"element.<br/><br/>"
"<span class=\"markup\">&lt;!-- TRANS:CREDIT_FOR_TRANSLATORS --&gt;</span><br/>"
"There must only be one DocBook file in the directory which has this "
"marker. It needs to be placed at the end of your document, right before the "
"&amp;underFDL; entity."
),

# check-preferred-forms.py
(
"Verifying that preferred words of phrases were used",
"tools/check-preferred-forms.py tools/preferred_forms %s",
"For the sake of consistency (and proper spelling), there is a centralized "
"list of common phrases and how they should be spelt in KDE manuals. This "
"covers cases such as \"file system\" vs. \"filesystem\" or \"Spell-check\" "
"vs. \"Spellcheck\". It's a matter of professional appearance, and you should "
"really try to make your spelling consistent with that of the other KDE "
"documents."
),

# insert_entities
(
"Testing for applicable entities",
"tools/run_insert_entities.py %s",
"There are a lot of entities available to authors of KDE manuals which make "
"it easy to keep the spelling of applications and abbreviations consistent. "
"Using them makes sure that application names are capitalized and marked up "
"correctly, licensed terms get proper 'trademark' and 'copyright' signs, "
"keys which are labelled differently in different countries (such as the "
"'Ctrl' key which is labelled 'Strg' on german keyboards) get automatically "
"translated and more. To make a long story short - you don't lose anything "
"when using entities as much as possible, but you gain a lot."
),

# check-invalid-img-refs.py
(
"Checking for invalid image references",
"tools/check-invalid-img-refs.py %s",
"A few tools used in KDE's documentation toolchain won't work if you reference "
"images from your markup which live in subdirectories. In particular, "
"generating PDF documentation out of your markup will fail. You can work "
"around this problem by shipping all images which you reference from your "
"document in the same directory as the DocBook file."
)
]

