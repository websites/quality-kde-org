/**
 * options.h - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#ifndef OPTIONS_H
#define OPTIONS_H

struct Options
{
	static bool KDEMode;
	static unsigned short VerbosityLevel;
};

#endif // OPTIONS_H

