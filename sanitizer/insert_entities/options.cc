/**
 * options.cc - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "options.h"

bool Options::KDEMode = false;
unsigned short Options::VerbosityLevel = 0;

