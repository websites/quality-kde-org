/**
 * entitytest.cc - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "entity.h"

#include <iostream>

using namespace std;

void check( const string &decl, const string &name, const string &value, const string &strippedValue )
{
	Entity e = Entity::fromDeclaration( decl );

	if ( e.name() != name ) {
		cerr << "ERROR! Failed to parse " << decl << endl;
		cerr << "Expected name: " << name << endl;
		cerr << "Got name     : " << e.name() << endl;
		exit( 1 );
	}

	if ( e.value() != value ) {
		cerr << "ERROR! Failed to parse " << decl << endl;
		cerr << "Expected value: " << value << endl;
		cerr << "Got value     : " << e.value() << endl;
		exit( 1 );
	}

	if ( e.strippedValue() != strippedValue ) {
		cerr << "ERROR! Failed to parse " << decl << endl;
		cerr << "Expected stripped value: " << strippedValue << endl;
		cerr << "Got stripped value     : " << e.strippedValue() << endl;
		exit( 1 );
	}
}

int main()
{
	check( "<!ENTITY Frerich.Raabe \"<personname>Frerich Raabe</personname>\">",
			"&Frerich.Raabe;",
			"<personname>Frerich Raabe</personname>",
			"Frerich Raabe" );
	check( "		  <!ENTITY 	        	Frerich.Raabe 	  			 \"<personname>Frerich Raabe</personname>\" 	 		  > 	 	",
			"&Frerich.Raabe;",
			"<personname>Frerich Raabe</personname>",
			"Frerich Raabe" );
	check( "<!ENTITY blah \"<foo><bar>blah blah</bar></foo>\">",
			"&blah;",
			"<foo><bar>blah blah</bar></foo>",
			"blah blah" );

	cout << "All OK!" << endl;
}

