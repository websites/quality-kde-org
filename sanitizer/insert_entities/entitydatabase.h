/**
 * entitydatabase.h - (C) 2003, 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#ifndef ENTITYDATABASE_H
#define ENTITYDATABASE_H

#include "entity.h"

#include <vector>

class EntityDatabase
{
	public:
		EntityDatabase();

		void merge( const std::string &filename );
		void merge( const std::string &data, const std::string &source );

		const std::vector<Entity> &entities() const { return m_entities; }

	private:
		std::vector<Entity> m_entities;
};

#endif // ENTITYDATABASE_H
