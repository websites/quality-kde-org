/**
 * util.cc - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "util.h"

#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

using namespace std;

vector<string> split( const string &s, string::value_type sep )
{
	vector<string> elements;
	string::size_type p = 0;
	string::size_type q = s.find( sep, p );
	while ( q != string::npos ) {
		elements.push_back( s.substr( p, q - p ) );
		p = s.find_first_not_of( sep, q );
		q = s.find( sep, p );
	}
	elements.push_back( s.substr( p ) );
	return elements;
}

string stripWhitespace( const string &s )
{
	string::size_type p = s.find_first_not_of( " \n\t\r" );
	if ( p == string::npos ) {
		return string();
	}
	string::size_type q = s.find_last_not_of( " \n\t\r" );
	if ( q == string::npos ) {
		return s.substr( p );
	}
	return s.substr( p, q - p + 1 );
}

string simplifyWhitespace( const string &s_ )
{
	string s = stripWhitespace( s_ );
	string::size_type p = s.find_first_of( " \n\t\r" );
	while ( p != string::npos ) {
		string::size_type q = s.find_first_not_of( " \n\t\r", p );
		s.replace( p, q - p, " " );
		p = s.find_first_of( " \n\t\r", p + 1 );
	}
	return s;
}

vector<string> filesInDirectory( const string &dirName_ )
{
	vector<string> files;
	string dirName = dirName_;
	if ( dirName[ dirName.size() - 1 ] != '/' ) {
		dirName.push_back( '/' );
	}

	DIR *dir = ::opendir( dirName.c_str() );
	if ( dir == NULL ) {
		cerr << "Skipping inaccessible directory " << dirName << endl;
		return files;
	}

	for ( struct dirent *ent = ::readdir( dir ); ent != NULL; ent = ::readdir( dir ) ) {
 		if ( ent->d_type == DT_REG ) {
			files.push_back( dirName + ent->d_name );
		}
	}

	::closedir( dir );

	return files;
}

bool fileIsDirectory( const string &f )
{
	struct stat sb;
	::stat( f.c_str(), &sb );
	return sb.st_mode & S_IFDIR != 0;
}


