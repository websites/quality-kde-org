/**
 * entitydatabase.cc - (C) 2003, 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "entitydatabase.h"
#include "options.h"

#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

EntityDatabase::EntityDatabase()
{
}

void EntityDatabase::merge( const string &filename )
{
	ifstream f( filename.c_str() );
	if ( !f ) {
		cerr << "Skipping inaccessible entity file " << filename << endl;
		return;
	}

	string data;
	{
		ostringstream stream;
		stream << f.rdbuf();
		data = stream.str();
	}

	merge( data, filename );
}

void EntityDatabase::merge( const string &data, const string &source )
{
	vector<Entity>::size_type oldSize = m_entities.size();
	string::size_type p = Entity::findDeclarationStart( data );
	while ( p != string::npos ) {
		string::size_type q = Entity::findDeclarationEnd( data, p + 1 );
		if ( q == string::npos ) {
			cerr << "Unclosed entity declaration in " << source << endl;
			break;
		}

		const string decl = data.substr( p, q - p + 1 );
		p = Entity::findDeclarationStart( data, q );
		if ( !Entity::isValidDeclaration( decl ) ) {
			continue;
		}

		const Entity e = Entity::fromDeclaration( decl );

		/* For some reason the documentation.index entity expands to an
		 * empty string. The naked value can be empty for e.g.
		 * "<email><!-- FIXME --></email>"
		 * Both cases are uninteresting, so we don't add them to the list.
		 */
		if ( e.value().empty() || e.strippedValue().empty() ) {
			continue;
		}

		if ( Options::KDEMode ) {
			/* <PhilRod> found a bug( let )
			 * <PhilRod> it replaces the application name with '&kappname;'
			 * <lauri> oh, nasty indeed
			 * ...same goes for "&package;", it seems.
			 */
			if ( e.name() == "&kappname;" || e.name() == "&package;" ) {
				continue;
			}
		}

		m_entities.push_back( e );

		if ( Options::VerbosityLevel >= 3 ) {
			cout << "Loaded entity '" << e.name() << "' -> '" << e.value() << "' (" << e.strippedValue() << ")" << endl;
		}
	}
	if ( Options::VerbosityLevel >= 2 ) {
		cout << "Loaded " << m_entities.size() - oldSize << " entities from " << source << " (" << data.size() << " bytes)" << endl;
	}
}

