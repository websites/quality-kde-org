/**
 * chunkstringtest.cc - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "chunkstring.h"

#include <iostream>

using namespace std;

string escapeNewlines( const string &s_ )
{
	string s( s_ );
	string::size_type p = s.find( '\n' );
	while ( p != string::npos ) {
		s.replace( p, 1, "\\n" );
		p = s.find( '\n', p + 2 );
	}
	return s;
}

void check( const string &haystack, const string &needle, const string &replacement, const string &expected )
{
	ChunkString chunkString( haystack, '\n' );
	chunkString.replaceAll( needle, replacement );

	if ( chunkString.chunkString() != expected ) {
		cout << "ERROR! " << endl;
		cout << "Haystack...: " << escapeNewlines( haystack ) << endl;
		cout << "Needle.....: " << escapeNewlines( needle ) << endl;
		cout << "Replacement: " << escapeNewlines( replacement ) << endl;
		cout << endl;
		cout << "Expected...: " << escapeNewlines( expected ) << endl;
		cout << "Got........: " << escapeNewlines( chunkString.chunkString() ) << endl;
		exit( 1 );
	}
}

int main( int argc, char **argv )
{
	check( "Frerich Raabe",
			"Frerich Raabe",
			"&Frerich.Raabe;",
			"&Frerich.Raabe;" );
	check( "He is Frerich Raabe, indeed.",
			"Frerich Raabe",
			"&Frerich.Raabe;",
			"He is &Frerich.Raabe;, indeed." );
	check( "This is Linus Torvalds,\nthe Linus Torvalds who started Linux.",
			"Linus Torvalds",
			"&Linus;",
			"This is &Linus;,\nthe &Linus; who started Linux." );
	check( "He is Linus Torvalds,\noh yes Linus Torvalds,\nwicked Linus Torvalds",
			"Linus Torvalds",
			"&Linus;",
			"He is &Linus;,\noh yes &Linus;,\nwicked &Linus;" );
	check( "He is Linus Torvalds,\noh yes Linus\nTorvalds, wicked Linus Torvalds",
			"Linus Torvalds",
			"&Linus;",
			"He is &Linus;,\noh yes &Linus;\n, wicked &Linus;" );
	check( "His name is Frerich\nRaabe",
			"Frerich Raabe",
			"&Frerich.Raabe;",
			"His name is &Frerich.Raabe;\n" );
	check( "This thing was written by Frerich\nRaabe with help from various people.",
			"Frerich Raabe",
			"&Frerich.Raabe;",
			"This thing was written by &Frerich.Raabe;\n with help from various people." );
	check( "He is called Tom\nJones - the same\nTom Jones as\nMike Myers.",
			"Tom Jones",
			"&Tom.Jones;",
			"He is called &Tom.Jones;\n - the same\n&Tom.Jones; as\nMike Myers." );
	check( "Foo bar Blah Blah baz\nblubb Blah\nBlah baz bar\nfoo xyzzy Blah\nBlah",
			"Blah Blah",
			"&Blah;",
			"Foo bar &Blah; baz\nblubb &Blah;\n baz bar\nfoo xyzzy &Blah;\n" );
	check( "My names are Frerich\nJuergen\nEberhard\nRaabe and nothing\nelse.",
			"Frerich Juergen Eberhard Raabe",
			"&Frerich.J.E.Raabe;",
			"My names are &Frerich.J.E.Raabe;\n\n\n and nothing\nelse." );

	cout << "All OK!" << endl;
}

