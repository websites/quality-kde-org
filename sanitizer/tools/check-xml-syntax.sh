#!/bin/sh
myname=`basename $0`
tmpfile=`mktemp -t ${myname}.XXXXXXXXXX` || exit 1

CHECKXML=/usr/bin/checkXML5

if [ ! -x $CHECKXML ]; then
        echo "<p>Failed to run <tt>checkXML</tt> program."
        exit 1
fi

$CHECKXML $1 > $tmpfile 2>&1
returnCode=$?

echo "<ul>"
exec 3<&0
exec 0<$tmpfile
while read line; do
        if echo $line | grep '^index\.docbook' > /dev/null 2>&1; then
                echo "<li>"
                echo $line | sed -e 's,^index\.docbook:\([0-9]*\):[^:]*,In line <div span="lineno">\1</div>,'
                read line
                echo -n "<p class=\"dbexcerpt\">"
                echo -n $line | sed -e 's,&,&amp;,g' | sed -e 's,<,&lt;,g' | sed -e 's,>,&gt;,g'
                echo "</p>"
                read line
                echo "</li>"
        fi
done
echo "</ul>"

rm -f $tmpfile

exit $returnCode
