#!/bin/sh
if [ -z "$1" ]; then
	echo "Usage: $0 <filename>"
	exit 1
fi

hits=0
rolesOfTranslatorsCount=`grep "TRANS:ROLES_OF_TRANSLATORS" "$1" | wc -l | sed -e 's,^ *,,'`

if [ $rolesOfTranslatorsCount -lt 1 ]; then
	echo "<span class=\"markup\">TRANS:ROLES_OF_TRANSLATORS</span> not present. It is required, please add it."
	echo "It should be placed right before <span class=\"markup\">&lt;/authorgroup&gt;</span>."
	hits=`expr $hits + 1`
else
	if [ $rolesOfTranslatorsCount -gt 1 ]; then
		echo "<span class=\"markup\">TRANS:ROLES_OF_TRANSLATORS</span> found $rolesOfTranslatorsCount times. It should appear only once per file."
		hits=`expr $hits + 1`
	else
		nextLine=`grep -A5 "TRANS:ROLES_OF_TRANSLATORS" "$1" | grep -v '^$' | head -n 2 | tail -n 1`
		echo "$nextLine" | grep "</authorgroup>" > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			echo "Misplaced <span class=\"markup\">TRANS:ROLES_OF_TRANSLATORS</span>. <span class=\"markup\">TRANS:ROLES_OF_TRANSLATORS</span> must come immediately before <span class=\"markup\">&lt;/authorgroup&gt;</span>!"
			hits=`expr $hits + 1`
		fi
	fi
fi

if [ $hits -gt 0 ]; then
	echo "<br/>"
fi

if ! grep "&underFDL;" "$1" > /dev/null 2>&1; then
	echo "The markup does not seem to use the"
	echo "<span class=\"markup\">&amp;underFDL;</span> entity."
	echo "This is required for all KDE documentation files in order"
	echo "to include the <a href=\"http://www.gnu.org/copyleft/fdl.html\">Free Documentation License</a> text."
	exit `expr $hits + 1`
fi

creditsOfTranslatorsCount=`grep "TRANS:CREDIT_FOR_TRANSLATORS" "$1" | wc -l | sed -e 's,^ *,,'`

if [ $creditsOfTranslatorsCount -lt 1 ]; then
	echo "The markup doesn't seem to contain a <span class=\"markup\">TRANS:CREDIT_FOR_TRANSLATORS</span> marker."
	echo "This is required, please add it."
	hits=`expr $hits + 1`
else
	if [ $creditsOfTranslatorsCount -gt 1 ]; then
		echo "There is more than one <span class=\"markup\">TRANS:CREDIT_FOR_TRANSLATORS</span> marker in the markup."
		echo "There should be only one in all DocBook files of a project."
		hits=`expr $hits + 1`
	else
		nextLine=`grep -A5 "TRANS:CREDIT_FOR_TRANSLATORS" "$1" | grep -v '^$' | head -n 2 | tail -n 1`
		echo "$nextLine" | grep "&underFDL;" > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			echo "Misplaced <span class=\"markup\">TRANS:CREDIT_FOR_TRANSLATORS</span> placeholder."
			echo "<span class=\"markup\">TRANS:CREDIT_FOR_TRANSLATORS</span> has to come immediately before <span class=\"markup\">&amp;underFDL;</span>."
			hits=`expr $hits + 1`
		fi
	fi
fi

exit $hits

