#!/usr/bin/env python
import re
import sys

systemEntityDeclRx = re.compile("<!ENTITY\s+([^ ]+)\s+SYSTEM\s+\"([^\"]+)\">")

entityMappings = []

if len(sys.argv) == 1:
	print "usage: dbexpand.py [--mapback <lineno>] <file>"
	sys.exit(1)

mapBack = -1
inputFile = sys.argv[1]
if inputFile == "--mapback":
	inputFile = sys.argv[2]
	mapBack = int(sys.argv[3])

try:
	contents = open(inputFile, 'r').read()
	for m in systemEntityDeclRx.finditer(contents):
		entityMappings.append((m.group(1), m.group(2)))

except IOError, e:
	print e

if mapBack == -1:
	for (entity, file) in entityMappings:
		injection = open(file, 'r').read()
		contents = contents.replace("&%s;" % entity, injection)
	print contents
else:
	curLine = 0
	curFile = inputFile
	for (entity, file) in entityMappings:
		numLines = open(file, 'r').read().count("\n")
		if curLine + numLines > mapBack:
			print "%s:%i" % (curFile, mapBack - curLine)
			sys.exit()
		curLine += numLines
		curFile = file

