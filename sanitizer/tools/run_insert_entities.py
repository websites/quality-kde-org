#!/usr/bin/python
import os
import subprocess
import sys
import tempfile

tmpFile = tempfile.NamedTemporaryFile()
cmd = "%s/insert_entities -k -e /srv/sources/frameworks/frameworks/kdoctools/src/customization/entities %s" % (os.path.dirname(sys.argv[0]), sys.argv[1])

p = subprocess.Popen(cmd.split(' '), 0, None, None, tmpFile)
errors = p.wait()
if errors == 0:
        sys.exit(0)

cmd = "diff -U0 %s %s" % (sys.argv[1], tmpFile.name)
p = subprocess.Popen(cmd.split(' '), 0, None, None, subprocess.PIPE)
if p.wait() == 2:
        sys.exit(0)

print "<p><b>Note</b>:It is not guaranteed that the document is still valid after applying these suggestions!</p>"
print "<ul>"

output = p.stdout.readlines()
for i in range(len(output)):
        line = output[i].strip()
        if line[0] != '@':
                continue

        lineNo = line[4:line.find(' ', 4) + 1]
        comma = lineNo.find(',')
        if comma:
                lineNo = lineNo[:comma]

        print "<li>"
        print "Entity could be used in line <span class=\"lineno\">%s</span>:" % lineNo
        print "<pre class=\"dbexcerpt\">"
        i += 1
        while i < len(output):
                line = output[i].strip()
                if line[0] not in ('+', '-'):
                        break
                print line.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')
                i += 1
        print "</pre>"
        print "</li>"

print "</ul>"
print sys.exit(errors)
