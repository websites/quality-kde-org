#!/usr/bin/python
import cgitb; cgitb.enable()
import subprocess
import string
import sys
import urllib

# cgi.escape or html.escape would work, too, but it's
# tricky to depend on them without requiring specific
# python versions. This approach comes from https://wiki.python.org/moin/EscapingHtml
html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
}

def html_escape(text):
        """Produce entities within text."""
        return "".join(html_escape_table.get(c,c) for c in text)

# For some reason, FancyURLOpener makes urlopen not throw an exception,
# contradicting the documented behaviour.
#
class ThrowingURLOpener(urllib.FancyURLopener):
        http_error_default = urllib.URLopener.http_error_default

class Tool:
        def __init__(self, msg, cmd, explanation = "Lord knows"):
                self.msg = msg
                self.cmd = cmd
                self.explanation = explanation

        def check(self, fileName):
                cmd = string.replace(self.cmd, "%s", fileName)
                print "<!-- %s -->" % cmd
                p = subprocess.Popen(cmd.split(' '), 0, None, None, subprocess.PIPE)
                status = p.wait()
                if (status == 0):
                        print "<b>okay!</b></span>"
                else:
                        if status == 1:
                                print "<b>OOPS! %i issue found!</b></span><br/>" % status
                        else:
                                print "<b>OOPS! %i issues found!</b></span><br/>" % status
                        print p.stdout.read()
                        print "<p class=\"explanation\">" + self.explanation + "</p>"
                return status

def loadTools():
        tools = []
        import tooldefs
        for t in tooldefs.toolDefinitions:
                tools.append(Tool(t[0], t[1], t[2]))
        return tools

def showError(msg):
        print "<b>Error</b>: %s" % msg

def checkFile(file, srcStr = None, title = "Running Checks"):
        print "<h1>%s</h1>" % title
        if srcStr:
                print "<p>...%s</p>" % srcStr
        print "<ol>"
        errors = 0
        for tool in tools:
                print "<li><span class=\"toolmsg\">%s..." % tool.msg
                errors = errors + tool.check(file)
                print "</li>"
        print "</ol>"
        return errors

# I wish I could just use "print open('header.inc', 'r').read();" here but
# a CGI is supposed to return plain HTML. header.inc contains PHP stuff.
def printHeader(title = None, component = None, module = None, app = None):
        print '<html>'
        print '<head>'
        if title:
                print '<title>', title, '</title>'
        print '<link rel="stylesheet" type="text/css" title="Normal" href="/style.css" />'
        print '</head>'
        print '<body>'
        print '<div id="title">'
        print '<div class="logo">&nbsp;</div>'
        print '<div class="header">'
        print '  <h1><a href="/">English Breakfast Network</a></h1>'
        print '  <p><a href="/">Almost, but not quite, entirely unlike tea.</a></p>'
        print '</div>'
        print '</div>'
        print '<div id="content">'
        print '<div class="inside">'

        from sys import stdout;
        print '<p class="breadcrumbs">'
        print '<a href="/index.php">Home</a>&nbsp;&gt;&nbsp;'
        stdout.write( '<a href="/sanitizer/index.php">DocBook Sanitizer</a>' )
        if component:
                print '&nbsp;&gt;&nbsp;'
                stdout.write( '<a href="/sanitizer/index.php?component=%s">%s</a>' % ( component, component ) )
                if module:
                        print '&nbsp;&gt;&nbsp;'
                        stdout.write( '<a href="/sanitizer/index.php?component=%s&module=%s">%s</a>' % ( component, module, module ) )
                        if app:
                                print '&nbsp;&gt;&nbsp;'
                                stdout.write( app )
        print '\n</p>'

        if component and module and app:
                print '<p class="crosslinks">'
                print 'Other %s/%s reports:&nbsp;[<a href="/apidocs/apidox-%s/%s-%s.html">API Documentation</a>]&nbsp;[<a href="/krazy/reports/%s/%s/%s/index.html">Krazy Code Checker</a>]' % ( module, app, component, module, app, component, module, app )
                print '</p>'

def handleInput():
        import cgi;
        form = cgi.FieldStorage()

        if (form.has_key("uri")):
                import urllib, os
                uri = form["uri"].value
                if not(uri.startswith("http:") or uri.startswith("https:")):
                        showError( "URI <b>%s</b> is invalid." % html_escape(uri) )
                        return None

                try:
                        urllib._urlopener = ThrowingURLOpener()
                        (filename, headers) = urllib.urlretrieve(uri)
                except IOError, e:
                        # XXX Make this error output nicer.
                        showError( "Failed to retrieve <b>%s</b>: %s." % (uri, e))
                else:
                        return checkFile(filename, "on <a href=\"%s\">%s</a>" % (uri, uri))

        elif (form.has_key("uploaded_file")):
                import tempfile
                f = tempfile.NamedTemporaryFile()
                f.write(form["uploaded_file"].value)
                f.flush()
                return checkFile(f.name, "on uploaded file")

        elif (form.has_key("markup")):
                import tempfile
                f = tempfile.NamedTemporaryFile()
                f.write(form["markup"].value)
                f.flush()
                return checkFile(f.name)
        else:
                print "Euhm?"

def printFooter():
        print open("/srv/www/englishbreakfastnetwork.org/inc/footer.inc", 'r').read()

errors = 0
tools = loadTools()
if len(sys.argv) > 1:
        if len(sys.argv) == 7:
                printHeader(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
                import datetime
                errors = checkFile(sys.argv[1], "as of %s (Git commit %s)" % (datetime.datetime.now().strftime('%B %d %Y %T'), sys.argv[6]), sys.argv[2])
        else:
                printHeader()
                errors = checkFile(sys.argv[1])
else:
        print "Content-Type: text/html"
        print ""
        printHeader()
        errors = handleInput()
printFooter()
sys.exit(errors)
