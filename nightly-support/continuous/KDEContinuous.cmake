# It uses the file KDECTestNightly.cmake, which is in KDE svn in kdesdk/cmake/modules/. 
# You need to have this file on some location on your system and then point the environment variable
# KDE_CTEST_NIGHTLY_DIR to the directory containing this file when running this script.
#
# Alex <neundorf AT kde.org>
# Torgny <nyblom AT kde.org>

# transform ctest script arguments of the form script.ctest,var1=value1,var2=value2
# to variables with the respective names set to the respective values
string(REPLACE "," ";" scriptArgs "${CTEST_SCRIPT_ARG}")
foreach(currentVar ${scriptArgs})
   if ("${currentVar}" MATCHES "^([^=]+)=(.+)$" )
      message("Setting '${CMAKE_MATCH_1}' = '${CMAKE_MATCH_2}'")
      set("${CMAKE_MATCH_1}" "${CMAKE_MATCH_2}")
   endif ("${currentVar}" MATCHES "^([^=]+)=(.+)$" )
endforeach(currentVar ${scriptArgs})

if(NOT DEFINED KDE_CTEST_MODULE)
   message(FATAL_MESSAGE "No module defined!")
endif()
if(NOT DEFINED KDE_CTEST_REF)
   message("No ref set, defaulting to 'master'")
   set(KDE_CTEST_REF master)
endif()
if(NOT DEFINED KDE_CTEST_NIGHTLY_DIR)
   message("Path to where the KDECTestNightly.cmake file can be found not defined!")
endif()
if(NOT DEFINED KDE_CTEST_CONTINUOUS_LOOP)
   set(KDE_CTEST_CONTINUOUS_LOOP true)
endif(NOT DEFINED KDE_CTEST_CONTINUOUS_LOOP)

message("Running continiuous for ${KDE_CTEST_MODULE}")
set(KDE_CTEST_VCS git)
set(KDE_CTEST_VCS_REPOSITORY git://anongit.kde.org/${KDE_CTEST_MODULE})
set(KDE_CTEST_AVOID_SPACES TRUE)
#if(NOT DEFINED KDE_CTEST_DASHBOARD_DIR)
#   set(KDE_CTEST_DASHBOARD_DIR "/var/code/kde/Dashboards/${KDE_CTEST_MODULE}/${KDE_CTEST_REF}" )
#endif()
set(KDE_CTEST_PARALLEL_LEVEL 2)
set(DO_INSTALL true)
set(KDE4_BUILD_TESTS true)
if(NOT DEFINED KDE_CTEST_INSTALL_DIR)
   set(CMAKE_PREFIX_PATH ${KDE_CTEST_DASHBOARD_DIR}/../../inst/${KDE_CTEST_REF})
else()
   set(CMAKE_PREFIX_PATH ${KDE_CTEST_INSTALL_DIR})
endif()
set(CMAKE_INSTALL_PREFIX ${CMAKE_PREFIX_PATH})
set(KDEPIM_MOBILE_UI true)

set(OPTIONS -DKDE4_BUILD_TESTS=${KDE4_BUILD_TESTS} -DKDEPIM_MOBILE_UI=${KDEPIM_MOBILE_UI} -DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH} -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX})
# for now hardcode the generator to "Unix Makefiles"
#set(CTEST_CMAKE_GENERATOR "Unix Makefiles" )
#set(CTEST_USE_LAUNCHERS 1)

# generic support code, provides the kde_ctest_setup() macro, which sets up everything required:
get_filename_component(_currentDir "${CMAKE_CURRENT_LIST_FILE}" PATH)
include( "${KDE_CTEST_NIGHTLY_DIR}/KDECTestNightly.cmake")

# if CMAKE_INSTALL_PREFIX and BUILD_experimental were defined on the command line, put them
# in the initial cache, so cmake gets them
set(CMAKE_BUILD_TYPE DEBUG)

include( "${_currentDir}/internalKDEContinuous.cmake" )

kde_ctest_continuous_loop( ${KDE_CTEST_CONTINUOUS_LOOP} )

