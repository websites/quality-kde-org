# Alex <neundorf AT kde.org>
# Torgny <nyblom AT kde.org>
function(kde_ctest_continuous_loop doLoop)
  if(DO_LOOP)
    set(firstLoop TRUE)
  else()
    set(firstLoop FALSE)
  endif()

  if(NOT DEFINED RUNTIME)
    set(RUNTIME 43200) #run for 12 hours
  endif()

  while (${CTEST_ELAPSED_TIME} LESS ${RUNTIME})
    message("Running next loop...")
    set (START_TIME ${CTEST_ELAPSED_TIME})
    kde_ctest_setup()
    ctest_empty_binary_directory("${CTEST_BINARY_DIRECTORY}")
    kde_ctest_write_initial_cache("${CTEST_BINARY_DIRECTORY}" CMAKE_BUILD_TYPE)
    ctest_start(Continuous)
    ctest_update(SOURCE "${CTEST_SOURCE_DIRECTORY}" RETURN_VALUE updatedFiles)

    if ("${updatedFiles}" GREATER 0  OR  firstLoop)

      # read additional settings, like maximum number warnings, warning exceptions, etc.
      include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")
      include("${CTEST_SOURCE_DIRECTORY}/CTestCustom.cmake" OPTIONAL)

      # configure, build, test, submit
      ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" OPTIONS "${OPTIONS}" RETURN_VALUE ret)

      if(${ret} EQUAL 0)
        include("${CTEST_BINARY_DIRECTORY}/CTestCustom.cmake" OPTIONAL)

        ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE ret)

        if(${ret} EQUAL 0)
          # now run all tests:
          if(DO_TEST)
            ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" )
          endif(DO_TEST)
          if(DO_INSTALL)
             execute_process(COMMAND make -j2 install WORKING_DIRECTORY "${CTEST_BINARY_DIRECTORY}")
          endif(DO_INSTALL)
        endif(${ret} EQUAL 0)
      endif(${ret} EQUAL 0)
      # submit the build and test results to cdash:
      ctest_submit()
      set(firstLoop FALSE)
    endif ("${updatedFiles}" GREATER 0  OR  firstLoop)
    if(NOT doLoop)
      break()
    endif()
    message("Loop done, waiting for next run")
    ctest_sleep( 600 ) # wait for 10 minutes
  endwhile()

endfunction(kde_ctest_continuous_loop doLoop)
