# This is a script for running a Nightly build of kdelibs.
# It is ready for testing.
# To adapt it for other projects, basically only the KDE_CTEST_VCS_REPOSITORY variable
# has to be changed.
#
# At the bottom of this file you can find  (commented out) a simple shell script which 
# I use to drive the Nightly builds on my machine. You have to adapt this to the 
# conditions on your system, then you can run it e.g. via cron.
#
# Alex <neundorf AT kde.org>

# The VCS of KDE is "svn", also specify the repository
set(KDE_CTEST_VCS svn)
set(KDE_CTEST_VCS_REPOSITORY svn://anonsvn.kde.org/home/kde/trunk/KDE/kdelibs)

get_filename_component(_currentDir "${CMAKE_CURRENT_LIST_FILE}" PATH)
# generic support code, provides the kde_ctest_setup() macro, which sets up everything required:
include( "${_currentDir}/../KDECTestNightly.cmake")

# set up binary dir, source dir, etc.
kde_ctest_setup()

# now actually do the Nightly
ctest_empty_binary_directory("${CTEST_BINARY_DIRECTORY}")
ctest_start(Nightly)
ctest_update(SOURCE "${CTEST_SOURCE_DIRECTORY}" )

# read some settings
include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")
include("${CTEST_SOURCE_DIRECTORY}/CTestCustom.cmake" OPTIONAL)

# build the tests
set(KDE4_BUILD_TESTS TRUE)

# if CMAKE_INSTALL_PREFIX and BUILD_experimental were defined on the command line, put them
# in the initial cache, so cmake gets them
kde_ctest_write_initial_cache("${CTEST_BINARY_DIRECTORY}" CMAKE_INSTALL_PREFIX 
                                                          BUILD_experimental 
                                                          KDE4_BUILD_TESTS
                                                          ${KDE_CTEST_CACHE_VARIABLES} )

# configure, build, test, submit
ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE ret)

if(${ret} EQUAL 0)

   ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE ret)
   if(${ret} EQUAL 0)
      ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" )

      # optionally install afterwards, so additional nightly builds can use this current install 
      # (e.g. kdepimlibs could use this kdelibs install)
      if(DO_INSTALL)
         kde_ctest_install("${CTEST_BINARY_DIRECTORY}" )
      endif(DO_INSTALL)

   endif(${ret} EQUAL 0)

endif(${ret} EQUAL 0)

ctest_submit()

############################################################################################
#
# ------------8<-----------------8<---------------------8<---------------------8<-----------
# #!/bin/sh
#
# # Set which ctest will be used, where the results should be installed to, and which suffix the 
# # build name on my.cdash.org should get:
# CTEST=/opt/cmake-2.6.2-Linux-i386/bin/ctest
# INSTALL_ROOT=/home/alex/Dashboards/installs/2.6.2
#
# export CMAKE_PREFIX_PATH=/opt/qt-4.5/qt/:$INSTALL_ROOT/automoc4:/opt/shared-mime-info:/opt/kdesupport/
#
# $CTEST -S KDELibsNightly.cmake,KDE_CTEST_BUILD_SUFFIX=gcc-4.2.3,CMAKE_INSTALL_PREFIX=$INSTALL_ROOT/kdelibs,DO_INSTALL=TRUE
#
# ------------8<-----------------8<---------------------8<---------------------8<-----------
