# This is a script for running a Nightly build of kdepim.
# To adapt it for other projects, basically only the KDE_CTEST_VCS_REPOSITORY variable
# has to be changed.
# Alex <neundorf AT kde.org>

# The VCS of KDE is "svn", also specify the repository
set(KDE_CTEST_VCS git)
set(KDE_CTEST_VCS_REPOSITORY git://anongit.kde.org/kdepim-runtime)

get_filename_component(_currentDir "${CMAKE_CURRENT_LIST_FILE}" PATH)
# generic support code, provides the kde_ctest_setup() macro, which sets up everything required:
include( "${_currentDir}/../KDECTestNightly.cmake")

# set up binary dir, source dir, etc.
kde_ctest_setup()

# now actually do the Nightly
ctest_empty_binary_directory("${CTEST_BINARY_DIRECTORY}")
ctest_start(Nightly)
ctest_update(SOURCE "${CTEST_SOURCE_DIRECTORY}" )

# read some settings
include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")
include("${CTEST_SOURCE_DIRECTORY}/CTestCustom.cmake" OPTIONAL)

# if CMAKE_INSTALL_PREFIX was defined on the command line, put it
# in the initial cache, so cmake gets them
kde_ctest_write_initial_cache("${CTEST_BINARY_DIRECTORY}" CMAKE_INSTALL_PREFIX ${KDE_CTEST_CACHE_VARIABLES} )

# configure, build, test, submit
ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE ret)

if(${ret} EQUAL 0)

   ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE ret)
   if(${ret} EQUAL 0)
      ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" )

      # optionally install afterwards, so additional nightly builds can use this current install 
      # (e.g. kdepimlibs could use this kdelibs install)
      if(DO_INSTALL)
         kde_ctest_install("${CTEST_BINARY_DIRECTORY}" )
      endif(DO_INSTALL)

   endif(${ret} EQUAL 0)

endif(${ret} EQUAL 0)

ctest_submit()
